# Bitbucket Server developer documentation

This repository contains the developer documentation for Bitbucket Server. Documentation is written 
in Markdown and published to the Atlassian npm repository.

## What's inside

Inside this directory you will find the content and navigation structure for your docs. These will
be published at:

https://developer.atlassian.com/server/bitbucket

```
README.md
node_modules/
package.json
.gitignore
.spelling
content/
/server/bitbucket/
    apis/
        index.md
        sample.md
    products/
        index.md
        sample.md
    images/
        screenshot.jpg
    getting-started.md
    index.md
data/
    /server/bitbucket.json
```

As you can see, the configuration and folder structures are simple, and you only generate the files 
that you need to document your product or service.

Once the installation is done, you can run some commands inside this folder:

## Preview your documentation set locally

You can instantly preview changes to your documentation set as you make them using the `npm start` command.
See the [viewing docs locally guide](https://developer.atlassian.com/dac/viewing-docs-locally/) for full details.

1. Open terminal and run `npm start`
2. Wait for preview to start
3. Open browser and navigate to `http://localhost:8080/server/bitbucket/`

## Run spellcheck 

You can run the following commands inside the project folder to check for spelling errors:

* `npm test`: Spellchecks Markdown files
* `npm run-script spellcheck`: Interactively fix or ignore spelling errors

Edit the `.spelling` file in the root of this repository to add words to the dictionary. 
The dictionary is unique to your repository. Note that you may need to turn on the ability to see 
hidden files and folders to see the `.spelling` file.

## Release your documentation set

The initial release of your documentation set will require help from the DAC team.
After that, further changes can be released by publishing your documentation set to npm.

See the [releasing your documentation guide](http://developer.atlassian.com/dac/publish/) for full details.

## Markdown frontmatter

Certain YAML frontmatter is required in order for the navigation and other page elements, such as the 
page title and last published date, to work properly. See the link below for more information:

    http://developer.atlassian.com/dac/markdown-metadata
