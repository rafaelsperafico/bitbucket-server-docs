---
title: Adding your events to the audit log
platform: server
product: bitbucketserver
category: devguide
subcategory: learning
date: "2017-12-14"
---
# Adding your events to the audit log

The [auditing feature](https://confluence.atlassian.com/bitbucketserver/audit-events-in-bitbucket-server-776640423.html) in Bitbucket Server
records audit events in a log file and also displays the most important events on tabs under the project and repository settings views. 
As a plugin developer you can also add your events to the audit log and UI; this page will show you how.

## How to add your events to the audit feature

Adding events to the audit system is relatively easy, all you need to do is correctly annotate a POJO and then [publish
it using the event system.](https://developer.atlassian.com/static/javadoc/events/2.1.1/reference/com/atlassian/event/api/EventPublisher.html)

Let's look at the [Audited](https://developer.atlassian.com/static/javadoc/bitbucket-server/latest/api/reference/com/atlassian/bitbucket/event/annotation/Audited.html) annotation you will need to add:

```java
@Audited(converter=MyConverter.class, channels={Channels.REPOSITORY_UI}, priority = Priority.HIGH)
```

We have a few different attributes to set:

- **converter**: a class implementing [AuditEntryConverter](https://developer.atlassian.com/static/javadoc/bitbucket-server/latest/api/reference/com/atlassian/bitbucket/audit/AuditEntryConverter.html)
which is called to extract the details of the event to be recorded
- **channels**: where this event should appear in the Bitbucket Server audit UI (optional)
- **priority**: used to decide whether to filter an event from the audit log (HIGH priority events are less likely to be
 filtered than LOW priority events)

An example of the converter code is:

```java
public class MyConverter implements AuditEntryConverter<MyEvent> {
    @Nonnull
    @Override
    public AuditEntry convert(@Nonnull MyEvent event,
                              @Nonnull AuditEntry.Builder builder) {
          return builder.
              action(event.getClass()).
              timestamp(new Date()).
              details(getDetails(event)).
              user(event.getUser()).
              repository(event.getRepository()).build();
    }
}
```

Some of the key features of the code above:

- Adding a repository or project: If you are specifying that the event should be displayed in the project or
  repository UI you must tell the auditing system which project or repository by adding them in the builder.

- Event details: the events added to the audit system by Bitbucket Server use a JSON format in their details strings for 
  consistency although it is not required.

- Data: all the required information must be available in the event for use in the converter.

## Choosing a priority

The priority of your event should reflect the event's importance to users. HIGH priority events will be logged by default
 so they should be of interest to all Bitbucket Server administrators. Generally speaking events occurring due to normal 
 development workflow are considered LOW or MEDIUM priority events. Events which alter the server's configuration or allow 
 users greater privileges are considered HIGH priority.

## Add it to the UI?

Adding an event to the project or repository audit tab incurs a cost to the system (storing the data in the database).
 Events which are added to the UI should not be fired frequently. Events in the UI should be those which an administrator
 would want quick access to. Most events are intended for general auditing and so are just available to system
 administrators via the log file.

## Gotchas

- **Firing too often.** You need to be mindful of the overhead firing events can have on the Bitbucket Server instance,
  especially events which are marked to be stored in the UI which will be added to the database.
- **Subclassing Bitbucket Server events.** Although it is possible to subclass Bitbucket Server events it is discouraged as
  it may have unintended effects. The event system will pass a subclass to a handler which is expecting the parent class. 
  If you `@Audited` annotate a subclassed Bitbucket Server event it is likely to be recorded in the audit log twice.
