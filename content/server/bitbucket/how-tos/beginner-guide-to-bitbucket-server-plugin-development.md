---
title:  "Beginner guide to Bitbucket Server plugin development"
platform: server
product: bitbucketserver
category: devguide
subcategory: intro
date: "2018-01-05"
---
#   Beginner guide to Bitbucket Server plugin development

Welcome to plugin development for Bitbucket Server. You will build a plugin with the same framework used in Jira and 
Confluence Server. If you have written Atlassian plugins before then you can use that knowledge to start from our 
[experienced plugin development guide](./experienced-guide-to-plugin-development). Otherwise, the tutorials below provide 
a good introduction.

Note, these guides are for developing plugins to run on Server instances you host yourself. If you want to create an 
app for a cloud instance hosted by Atlassian then checkout our [cloud plugin development tutorials](https://developer.atlassian.com/cloud/bitbucket/getting-started/).

##  Tutorials

Below are tutorials for different types of behavior in Bitbucket Server. Read through their summaries and choose one that 
represents something you are interested in or know about. Please note, all tutorials require you to [install the Atlassian 
Plugin SDK](https://developer.atlassian.com/server/framework/atlassian-sdk/set-up-the-atlassian-plugin-sdk-and-build-a-project/).

After completing any of these tutorials you will have learned to do the following:  

- Generate a Bitbucket Server plugin using the Atlassian Plugin SDK
- Use various Bitbucket Server services

| Name | Description | Learnings |
| --- | --- | ---|
| [Adding a link item in the pull request overview panel](../../tutorials-and-examples/pull-request-overview) | Create a plugin that puts custom content on the Overview tab of a pull request | <ul><li>Find plug-in points in the Bitbucket Server UI for adding content</li><li>Define a [Client Web Panel](../../reference/plugin-module-types/client-web-panel)</li><li>Use custom CSS and JS in your panel with [Web resource](https://developer.atlassian.com/display/DOCS/Web+Resource+Plugin+Module) dependencies</li></ul> |
| [Controlling when pull requests can be merged](../../tutorials-and-examples/controlling-when-pull-requests-can-be-merged) | Only allow repository administrators to merge pull requests by writing a plugin containing a [Merge Request Check](../../reference/plugin-module-types/merge-check)  | <ul><li>Implement a repository merge check in Java</li><li>Declare a repository merge check in your plugin's `atlassian-plugin.xml`</li><li>Internationalize your plugin's messages</li></ul> |
| [Decorating the user account](../../tutorials-and-examples/decorating-the-user-account) | Write a plugin that decorates a user's account management page with custom content | <ul><li>Create a servlet that extracts an account management page</li><li>Add a new tab to the account management page</li><li>Decorate the account management page using Soy to render its content</li></ul> |
| [Decorate the user profile page](../../tutorials-and-examples/decorating-the-user-profile) | Write a plugin that adds custom content to the public user profile | <ul><li>Create a servlet that extracts a user's profile</li><li>Add a new tab to the user profile page</li><li>Decorate the user profile using Soy to render its content</li></ul> |

##  Next steps

Now you are ready to start developing your own plugins with the Atlassian plugin framework. If you would like to keep learning 
about plugin development for Bitbucket Server, continue with the following:

- Go through the [experienced plugin development guide](./experienced-guide-to-plugin-development) for further help with building your own plugin
- Read through the [REST documentation for Bitbucket Server](./command-line-rest)
- Learn more about the [Atlassian plugin framework](https://developer.atlassian.com/docs/atlassian-platform-common-components/plugin-framework)
- List your plugin on the [Atlassian marketplace](https://developer.atlassian.com/platform/marketplace/creating-a-marketplace-listing/)