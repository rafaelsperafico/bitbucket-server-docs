---
title:  Building Bitbucket Server from Source Code
platform: server
product: bitbucketserver
category: devguide
subcategory: learning
date: "2017-12-14"
---
#  Building Bitbucket Server from Source Code

This guide describes building a Bitbucket Server distribution from the source code. Bitbucket Server
source code is available to all commercial license holders.

{{% warning %}}

Consider building a plugin instead!

If you are downloading the source with the intent of customizing Bitbucket Server, *you
should first consider writing a plugin instead*. Bitbucket Server provides a comprehensive
[Plugin SDK](https://developer.atlassian.com/server/framework/atlassian-sdk/)
and a [rich set of stable Java APIs](https://developer.atlassian.com/static/javadoc/bitbucket-server/latest/api/reference/packages.html)
for extending its functionality, which are backwards compatible between major releases.
If you choose to modify and build the source code yourself, upgrading to newer Bitbucket
Server versions may be problematic.

{{% /warning %}}

If you're really sure you want to modify Bitbucket Server's source instead of building a plugin,
here are the steps for building Bitbucket Server from source:

## Download Java and the Bitbucket Server source

1. Download and install the [Java Development Kit](http://www.oracle.com/technetwork/java/javase/downloads/index.html).  
Bitbucket Server requires the _JDK 1.8 Update 45_ release or higher.

2. Download and extract the
[Bitbucket Server source distribution](https://my.atlassian.com/download/source/stash)  
Use your [my.atlassian.com](https://my.atlassian.com) account to login (or contact Atlassian's
[sales department](mailto:sales@atlassian.com) if you do not see the link to download the distribution).

## Download the database drivers

Bitbucket Server is built using [Maven](http://maven.apache.org), which is bundled with the source distribution.
During the build, Maven will download the dependencies of Bitbucket Server and store them locally.
Two of those dependencies are Oracle's and Microsoft's database drivers, which need to be manually installed for legal reasons.
If you do not have those drivers in your local Maven repository, follow these instructions to install them:

1. Download the [Oracle JDBC driver](http://www.oracle.com/technetwork/database/features/jdbc/jdbc-ucp-122-3110062.html)
    (you might need to sign up for an Oracle account),
       - in the _Oracle Database 12c Release 2 (12.2.0.1) JDBC Drivers_ section, find and download the _ojdbc8.jar_ driver,
       - copy the driver in the directory where you extracted the source distribution;
2. And, in the same directory (containing the source distribution of Bitbucket Server), run the following command:
       - On **Mac OS X or Linux**:

    ```sh
    ./mvn3.sh install:install-file -DgroupId=com.oracle -DartifactId=ojdbc8 -Dversion=12.2.0.1 -Dpackaging=jar -Dfile=ojdbc8.jar -Dmaven.repo.local="`pwd`/localrepo"
    ```

       - On **Windows**:

    ```sh
    mvn3.bat install:install-file -DgroupId=com.oracle -DartifactId=ojdbc8 -Dversion=12.2.0.1 -Dpackaging=jar -Dfile=ojdbc8.jar -Dmaven.repo.local="%CD%\localrepo"
    ```

3. Next, download the [Microsoft JDBC driver](http://www.microsoft.com/en-us/download/details.aspx?id=11774):
       - If you use **Mac OS X or Linux**:
           * find and download the _sqljdbc\_4.0.2206.100\_enu.tar.gz_ driver,
           * extract the archive to a temporary directory (such as `/tmp/ms-driver`),
           * and, in the directory containing the source distribution of Bitbucket Server, run the following commands
             (after replacing the path of the driver's directory by the correct one on the first line):

    ```sh
    export MS_DRIVER_PATH="/tmp/ms-driver"
    ./mvn3.sh install:install-file -DgroupId=com.microsoft.sqlserver -DartifactId=sqljdbc -Dversion=4.0.2206 -Dpackaging=jar -Dfile=$MS_DRIVER_PATH/enu/sqljdbc.jar -Dmaven.repo.local="`pwd`/localrepo"
    ./mvn3.sh install:install-file -DgroupId=com.microsoft.sqlserver -DartifactId=sqljdbc_auth -Dversion=4.0.2206 -Dclassifier=x64 -Dpackaging=dll -Dtype=dll -Dfile=$MS_DRIVER_PATH/enu/auth/x64/sqljdbc_auth.dll -Dmaven.repo.local="`pwd`/localrepo"
    ./mvn3.sh install:install-file -DgroupId=com.microsoft.sqlserver -DartifactId=sqljdbc_auth -Dversion=4.0.2206 -Dclassifier=x86 -Dpackaging=dll -Dtype=dll -Dfile=$MS_DRIVER_PATH/enu/auth/x86/sqljdbc_auth.dll -Dmaven.repo.local="`pwd`/localrepo"
    ```

       - If you use **Windows**:
           * find and download the _sqljdbc\_4.0.2206.100\_enu.exe_ driver,
           * double click on the executable to decompress the driver in a temporary directory,
           * and, in the directory containing the source distribution of Bitbucket Server, run the following commands
             (after replacing the path of the driver's directory by the correct one on the first line):

    ```sh
    set MS_DRIVER_PATH=c:\Users\USERNAME\Downloads\Microsoft JDBC Driver 4.0 for SQL Server
    mvn3.bat install:install-file -DgroupId=com.microsoft.sqlserver -DartifactId=sqljdbc -Dversion=4.0.2206 -Dpackaging=jar -Dfile="%MS_DRIVER_PATH%\sqljdbc_4.0\enu\sqljdbc.jar" -Dmaven.repo.local="%CD%\localrepo"
    mvn3.bat install:install-file -DgroupId=com.microsoft.sqlserver -DartifactId=sqljdbc_auth -Dversion=4.0.2206 -Dclassifier=x64 -Dpackaging=dll -Dtype=dll -Dfile="%MS_DRIVER_PATH%\sqljdbc_4.0\enu\auth\x64\sqljdbc_auth.dll" -Dmaven.repo.local="%CD%\localrepo"
    mvn3.bat install:install-file -DgroupId=com.microsoft.sqlserver -DartifactId=sqljdbc_auth -Dversion=4.0.2206 -Dclassifier=x86 -Dpackaging=dll -Dtype=dll -Dfile="%MS_DRIVER_PATH%\sqljdbc_4.0\enu\auth\x86\sqljdbc_auth.dll" -Dmaven.repo.local="%CD%\localrepo"
    ```

## Build Bitbucket Server from source

1. Run the following command in the directory containing the source distribution of Bitbucket Server:
       - On **Mac OS X or Linux**:

    ```sh
    ./build.sh
    ```

       - On **Windows**:

    ```sh
    build.bat
    ```

    This will perform a full build of Bitbucket Server. The distribution ZIP will be created in the subdirectory `bitbucket-parent/distribution/default/target`.

2. Then, to develop Bitbucket Server in your IDE:
       - create a new project from the `bitbucket-parent/pom.xml` file in the directory of the source distribution,
       - enable the [Tomcat](http://tomcat.apache.org) integration in your IDE,
       - once the project is set up, deploy the Bitbucket Server [WAR file](http://en.wikipedia.org/wiki/WAR_file_format_\(Sun\)) using the Tomcat integration.

3. Alternatively, from the command line:
       - in the directory containing the source distribution (after running `build.sh` or `build.bat` in the step 1), run:

    ```sh
    cd bitbucket-parent/webapp/default
    mvn cargo:run
    ```

       - then, after each change, re-run `build.sh` or `build.bat`.
