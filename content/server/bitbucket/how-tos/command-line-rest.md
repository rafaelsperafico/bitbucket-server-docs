---
title:  Bitbucket Server REST APIs
platform: server
product: bitbucketserver
category: devguide
subcategory: learning
date: "2018-08-29"
---
#  Bitbucket Server REST APIs

Bitbucket Server's REST APIs are for developers who want to build functionality on top of
Bitbucket Server.

Quick links:
[Reference](../../reference/rest-api/)

## Overview

Whatever you want to build, our intention is to supply the platform for it.
New web or desktop applications, mobile apps, integrations, browser-based
extensions or apps, Atlassian plugins, and whatever you can dream up. The
Atlassian REST APIs provide a standard interface for interacting with Bitbucket Server
and our other applications.

REST APIs provide access to resources (data entities) via URI paths. To use a
REST API, your application will make an HTTP request and parse the response.
Your methods will be the standard HTTP methods like GET, PUT, POST and DELETE.
REST APIs operate over HTTP(s) making it easy to use with any programming
language or framework.

The input and output format for the Bitbucket Server REST APIs is
[JSON](http://www.json.org/).

Bitbucket Server uses the [Atlassian REST plugin](https://developer.atlassian.com/display/DOCS/REST+Plugin+Module) to
implement the Bitbucket Server APIs. The REST plugin is bundled with Bitbucket Server. You can add
your own REST APIs to Bitbucket Server by creating a Bitbucket Server plugin that includes the REST
plugin module. See the basic principles we employ in our
[REST API design](https://developer.atlassian.com/display/DOCS/Atlassian+REST+API+Design+Guidelines+version+1).

The examples below use [curl](http://curl.haxx.se/).

## Simple Example of Retrieving Repository Commits

Want to retrieve a list of commits for a repository in Bitbucket Server via REST? Here's
how easy it is.

Just GET the JSON from your Bitbucket Server server. In this example, the server is: `http://localhost:7990`.
This example also uses basic authentication, with a username 'fred' and password 'fred'.

To list the latest commits to the repository _my-repo_ in project _WORK_ on the
branch _master_:

##### Request

```sh
curl -u fred:fred http://localhost:7990/rest/api/1.0/projects/WORK/repos/my-repo/commits/?until=master
```

##### Response

The response provides the number of results returned, the maximum number
returned (limit), if there are more results (`isLastPage`), and a list of
commits. The commits array contains the commit hash, display hash, author
details, commit message and any relevant attributes (such as Jira keys).

```json
{
  "size": 2,
  "limit": 25,
  "isLastPage": true,
  "values": [
    {
      "id": "01f9c8680e9db9888463b61e423b7b1d18a5c2c1",
      "displayId": "01f9c86",
      "author": {
        "name": "Seb Ruiz",
        "emailAddress": "sruiz@atlassian.com"
      },
      "authorTimestamp": 1334730200000,
      "message": "NONE: Add groovy as java synhi\n+review @aahmed",
      "parents": [
        {
          "id": "06a499d51107533a4f24a3620280edbb342d89b7",
          "displayId": "06a499d"
        }
      ],
      "attributes": {}
    },
    {
      "id": "c9d6630b88143dab6a922c5cffe931dae68a612a",
      "displayId": "c9d6630",
      "author": {
        "name": "Pierre-Etienne Poirot",
        "emailAddress": "pepoirot@atlassian.com"
      },
      "authorTimestamp": 1334639525000,
      "message": "STASH-2121: fix typo (CR-STASH-1199)",
      "parents": [
        {
          "id": "2a58a084fef5b2246c996924d38e0d236c855794",
          "displayId": "2a58a08"
        }
      ],
      "attributes": {
        "jira-key": [
          "STASH-2121"
        ]
      }
    }
  ],
  "start": 0,
  "filter": null,
  "nextPageStart": 2
}
```

## Simple Example of a POST API Call

You can also use the REST API to watch a commit.  In this case, you must perform an HTTP POST. 

When doing a POST with `curl`, remember to set the Content-Type in your request to `application/json`,
regardless of whether or not you have any data in the body of the request. 

#### Example

##### Request

```sh
curl -u fred:fred -X POST -H "Content-Type: application/json" http://localhost:7990/rest/api/1.0/projects/WORK/repos/my-repo/commits/01f9c8680e9db9888463b61e423b7b1d18a5c2c1/watch
```

##### Response

The response in this example, if successful, will be an empty body with a `204 No Content` status code. 

## Authentication

The REST APIs support
[basic authentication](../example-basic-authentication), OAuth and session authentication.

## Browsing and Testing your REST APIs from the Application UI

The Atlassian REST API Browser (RAB) is a tool for discovering the REST APIs
and other remote APIs available in a given installation of an Atlassian
application. These applications include Jira, Confluence and the other
Atlassian products. RAB is implemented as a plugin in the host application.

RAB shows you all the REST and JSON-RPC resources in the application, displays
the methods for each resource, and allows you to make test calls against the
methods. If you have installed a plugin that creates additional REST resources
for the application, RAB will also discover those resources.

#### Quick Start

Download and install the latest version of the
[Atlassian Developer Toolbox Plugin](https://marketplace.atlassian.com/plugins/com.atlassian.devrel.developer-toolbox-plugin).
It includes the REST API Browser which allows a developer to
browse, discover, and test Atlassian's rich REST and JSON-RPC APIs.

After you install the Developer Toolbox, go into the product's Administration
page. You'll find the "Developer Toolbox" category on that page.

#### Atlassian Plugin SDK

Already using the [Atlassian Plugin SDK](https://developer.atlassian.com/display/DOCS/Working+with+the+SDK)?
Then you already have RAB. This is a quick start guide to using it:

  1. Do an `atlas-run` or `atlas-debug` or `atlas-run-standalone` as usual. The Atlassian application (Jira, Confluence, or any of the others) will be installed with the REST API Browser plugin enabled.
  2. Go to the application's administration screen in your web browser.
  3. Click **REST API Browser** on the administration screen.
  4. Choose an API from the dropdown list at the top left of the screen.
  5. Choose a resource from the list on the left of the screen. The REST API Browser will show you the methods (GET, POST, PUT, etc) and the parameters available for that resource.
  6. To test the resource, enter the parameter values as prompted then click **Execute**.

#### Details

For detailed instructions, see the [documentation for the REST API Browser](https://developer.atlassian.com/display/DOCS/Using+the+REST+API+Browser).

## Full REST API Documentation

See the details of each REST request and response in our
[reference documentation](../../reference/rest-api/).
