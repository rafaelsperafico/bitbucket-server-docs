---
title:  Bitbucket Server REST API Example - Basic Authentication
platform: server
product: bitbucketserver
category: devguide
subcategory: learning
date: "2017-12-14"
---
#  Bitbucket Server REST API Example - Basic Authentication

Bitbucket Server allows REST clients to authenticate themselves with a user name and
password using [basic authentication](http://en.wikipedia.org/wiki/Basic_access_authentication).

## Simple example

Most client software provides a simple mechanism for supplying a user name and
password and will build the required authentication headers automatically. For
example you can specify the `-u` argument with `curl` as follows

```sh
curl -D- -u fred:fred -X GET -H "Content-Type: application/json" http://localhost:7990/rest/api/1.0/projects
```

## Supplying Basic Auth headers

If you need to you may construct and send basic auth headers yourself. To do
this you need to perform the following steps:

  1. Build a string of the form username:password
  2. BASE64 encode the string
  3. Supply an "Authorization" header with content "Basic " followed by the encoded string, e.g. `Basic YWRtaW46YWRtaW4=`

    ```sh
    curl -D- -X GET -H "Authorization: Basic ZnJlZDpmcmVk" -H "Content-Type: application/json" http://localhost:7990/rest/api/1.0/projects
    ```

## Authentication challenge

Some http client software expects to receive an authentication challenge
before it will send an authorization header and this may mean that it may not
behave as expected. In this case you may need to configure it to supply the
authorization header as described above rather than relying on its default
mechanism.
