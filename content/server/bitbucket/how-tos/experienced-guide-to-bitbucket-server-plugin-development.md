---
title:  "Experienced guide to Bitbucket Server plugin development"
platform: server
product: bitbucketserver
category: devguide
subcategory: intro
date: "2018-01-05"
---
#   Experienced guide to Bitbucket Server plugin development

Welcome to the experienced plugin development guide for Bitbucket Server. This guide assumes you are familiar with the 
[Atlassian Plugin Framework](https://developer.atlassian.com/display/DOCS/Plugin+Framework) and have installed the 
[Atlassian Plugin SDK](https://developer.atlassian.com/display/DOCS/Getting+Started). If you are still new to plugin development for Atlassian products then start with the [beginner guide to Bitbucket 
Server plugin development](./beginner-guide-to-bitbucket-server-plugin-development).

We have two options for you to follow: start from working code by [forking an existing plugin](#Forking_an_exisitng_plugin), 
or begin completely [from scratch](#Starting_from_scratch).

##  Forking an existing plugin

Choose an example plugin that most resembles what you want to achieve and begin modifying it for your use case.

1. [Install the Atlassian Plugin SDK](https://developer.atlassian.com/display/DOCS/Set+up+the+Atlassian+Plugin+SDK+and+Build+a+Project).
1. Create a [Bitbucket account](https://bitbucket.org/).
1. Browse our [collection of open source Bitbucket Server plugins](https://atlassian.bitbucket.io/#bitbucket) and fork it 
into your personal account. You can choose whether to make your fork public or private.
1. Modify the plugin to suit your needs.

## Starting from scratch

Use our CLI tool to generate the plugin structure then begin developing your own behavior from a clean slate.

1. Install the [Atlassian Plugin SDK](https://developer.atlassian.com/display/DOCS/Set+up+the+Atlassian+Plugin+SDK+and+Build+a+Project).
1. Run `atlas-create-bitbucket-plugin` from a __command prompt__ window and follow the prompts to create your plugin.
1. Run `atlas-create-bitbucket-plugin-module` from the root directory of your plugin or hand edit your 
`atlassian-plugin.xml` file to add additional modules to your plugin.
1. Run `atlas-run` from the root directory of your plugin to start Bitbucket Server with your plugin installed. See the 
[Atlassian Plugin SDK Documentation](https://developer.atlassian.com/display/DOCS/Working+with+the+SDK) for more 
information on developing and debugging your plugin.
1. Modify the plugin to suit your needs.

{{% note %}}
You can see the above commands in action in this [30 minute video tutorial](https://developer.atlassian.com/blog/2015/01/beer-o-clock-stash-plugin-tutorial/).

Note: This tutorial predates the renaming of the product to "Bitbucket Server" from "Stash", but is still a useful resource.
{{% /note %}}

##  Next steps

To keep learning about plugin development for Bitbucket Server, continue with the following:

- Read through the [REST documentation for Bitbucket Server](./command-line-rest)
- List your plugin on the [Atlassian marketplace](https://developer.atlassian.com/platform/marketplace/creating-a-marketplace-listing/)