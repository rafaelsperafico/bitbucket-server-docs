---
title:  Responding to application events
platform: server
product: bitbucketserver
category: devguide
subcategory: learning
date: "2017-12-14"
---
#  Responding to application events

Using the Atlassian Event framework and the plugin system, Bitbucket Server makes it easy
to respond to events such as projects being created, removed and repositories
being pushed or pulled from.

## How to respond to events

  1. Create a Java class called `MyEventListener`.
  2. Create a new method that takes a single event (in our example, `RepositoryPushEvent`) as an argument and annotate
     it with [`@EventListener`](https://docs.atlassian.com/atlassian-event/3.0.0/apidocs/index.html?com/atlassian/event/api/EventListener.html).
     This annotation tells the event framework that the method is listening for the specified type of event.
     The method name does not matter.
  3. Implement the `@EventListener` method to respond to the event however you like.
  4. Ensure that an instance of your class is registered with the
     [`EventPublisher`](https://docs.atlassian.com/atlassian-event/3.0.0/apidocs/index.html?com/atlassian/event/api/EventPublisher.html).
     Starting from version 4.7, Bitbucket will automatically scan any components annotated with `@Named` and register
     all methods annotated with `@EventListener`. Note that if your plugin targets versions before 4.7, you'll need to
     manually register and unregister your event listener.

### Example - automatic registration - suitable for Bitbucket 4.7 and higher

**MyEventListener.java**

```java
package com.atlassian.bitbucket.example;

import com.atlassian.bitbucket.event.repository.RepositoryPushEvent;
import com.atlassian.event.api.EventListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Named;

@Named("myEventListener")
public class MyEventListener {
    private static final Logger log = LoggerFactory.getLogger(MyEventListener.class);

    //Whenever a push is made, the RepositoryPushEvent is fired and this listener is called
    @EventListener
    public void onPushEvent(RepositoryPushEvent pushEvent) {
        log.info("A push was made to {}", pushEvent.getRepository());
    }
}
```

### Example - manual registration - suitable for Bitbucket 4.0 and higher

This method is safe to use in Bitbucket 4.7 and higher as well. In 4.7 and higher, manual registration will
result in the component being registered twice (because starting with 4.7 any `@Named` components are automatically
scanned and registered). This is not a problem because the `EventPublisher` will recognize this and ensure that events
are sent to your listener only once.

**MyEventListener.java**

```java
package com.atlassian.bitbucket.example;

import com.atlassian.bitbucket.event.repository.RepositoryPushEvent;
import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.inject.Inject;
import javax.inject.Named;

@Named("myEventListener")
public class MyEventListener {
    private static final Logger log = LoggerFactory.getLogger(MyEventListener.class);

    private final EventPublisher eventPublisher;

    @Inject
    public MyEventListener(@ComponentImport EventPublisher eventPublisher) {
        this.eventPublisher = eventPublisher;
    }

    @PostConstruct
    public void init() {
        // Called after the instance is created (when the plugin is started)
        eventPublisher.register(this);
    }

    @PreDestroy
    public void destroy() {
        // Called just before the instance is destroyed (when the plugin is stopped)
        eventPublisher.unregister(this);
    }

    //Whenever a push is made, the RepositoryPushEvent is fired and this listener is called
    @EventListener
    public void onPushEvent(RepositoryPushEvent pushEvent) {
        log.info("A push was made to {}", pushEvent.getRepository());
    }
}
```

## Asynchronous events

Any event marked with the `@AsynchronousPreferred` annotation is likely to be executed on a pool
thread, so any code subscribing to these events should be thread safe.

## Available events

All events fired by Bitbucket Server core live in
[com.atlassian.bitbucket.event](https://developer.atlassian.com/static/javadoc/bitbucket-server/latest/api/reference/com/atlassian/bitbucket/event/package-summary.html)
and its child packages (see for example
[com.atlassian.bitbucket.event.pull](https://developer.atlassian.com/static/javadoc/bitbucket-server/latest/api/reference/com/atlassian/bitbucket/event/pull/package-summary.html)
and [com.atlassian.bitbucket.event.permission](https://developer.atlassian.com/static/javadoc/bitbucket-server/latest/api/reference/com/atlassian/bitbucket/event/permission/package-summary.html)).

Please [grab the source](../building-bitbucket-server-from-source-code) or
[browse the javadoc](https://developer.atlassian.com/static/javadoc/bitbucket-server/latest/api/reference/com/atlassian/bitbucket/event/package-summary.html)
for a full listing of available event classes.
