---
title:  Updating build status for commits
platform: server
product: bitbucketserver
category: devguide
subcategory: learning
date: "2017-12-14"
---
#  Updating build status for commits

Developers can integrate third-party build servers with Bitbucket Server using the
[Build REST API](https://developer.atlassian.com/static/rest/bitbucket-server/latest/bitbucket-build-rest.html).

By posting build results to a REST end-point in Bitbucket Server, build servers can store and update the build status of a commit.
Bitbucket Server keeps a running tally of which builds have passed and failed for each commit and displays build results in the UI.

The examples below use [cURL](http://curl.haxx.se/), a cross-platform command-line utility for transferring data over a
variety of protocols. In real life you can use any programming language or tool capable of making an HTTP request to
integrate with the build integration API.

## Adding a build result to a commit

To associate a build result with a particular commit, you need to `POST` a JSON object to the build status REST resource at:

```shell
https://<bitbucket-base-url>/rest/build-status/1.0/commits/<commit-hash>
```

The format of the JSON object that should be used as the request body is:

```json
{
	"state": "<INPROGRESS|SUCCESSFUL|FAILED>",
	"key": "<build-key>",
	"name": "<build-name>",
	"url": "<build-url>",
	"description": "<build-description>"
}
```

The `state`, `key` and `url` fields are required. The `name` and `description` fields are optional, but if present will
be displayed to users in the UI.

The `state` refers to the current state of the build that is the subject of the request:

* `INPROGRESS` indicates that a new build targeting the specified commit has started.
* `SUCCESSFUL` indicates that a build targeting the specified commit has completed successfully.
* `FAILED` indicates that a build targeting the specified commit has failed.

See [Updating Build Results](#Updating_build_results) for more about states.

Note that the request doesn't specify a repository. This is because the same build information will be shared across
all repositories containing the targeted commit. In practice this means forks of the same repository stored in Bitbucket Server
will all display the same build information for common commits.

### Multiple builds

If a single commit was built by multiple build plans, you can `POST` multiple results for the same commit. Each result
must have a different `key` attribute.

### Updating build results

Bitbucket Server stores one build result for each key per commit, so you can update a previous build result by
sending a request with:

* the same commit hash; and
* the same `key` attribute.

The new result will replace the previously submitted build result for that commit hash and `key`.

This is useful for transitioning a particular build result from `INPROGRESS` to `SUCCESSFUL` when a build completes, or
for replacing a `FAILED` build result with `SUCCESSFUL` one if a build is re-built at the same commit.

Note that a build result doesn't necessarily have to be submitted as `INPROGRESS` before being submitted as
`SUCCESSFUL` or `FAILED`.

### Viewing build results

The build status for a particular commit is visible in:

- the *commit list* view, for any repositories containing the commit;
- the *pull request* header, for any pull request where the commit is the head of the branch to merge; and
- the metadata section of the *commit page*.

You can also retrieve build status information and individual build results for commits using the GET methods on the
[Build REST API](https://developer.atlassian.com/static/rest/bitbucket-server/latest/bitbucket-build-rest.html).

## Example

In the example, we'll make three requests to the build integration API, each supplying a new build result for a
particular commit. This simulates a simple project with three different builds targeting building the same commit from
a repository. These could be a unit test build, an integration test build and a database matrix build (for example).

### One green build

The following curl command will add a build to the commit `9e72f04322c4a1f240e0b3158c67c3c19cdd16e7`:

```sh
curl -u username:password -H "Content-Type: application/json" -X POST http://localhost:7990/bitbucket/rest/build-status/1.0/commits/9e72f04322c4a1f240e0b3158c67c3c19cdd16e7 -d @build0.json
```

Where `build0.json` contains:

```json
{
	"state": "SUCCESSFUL",
	"key": "REPO-MASTER",
	"name": "REPO-MASTER-42",
	"url": "https://bamboo.example.com/browse/REPO-MASTER-42",
	"description": "Changes by John Doe"
}
```

If you're trying the examples above, note that you'll need to change the hash to one that references a commit in one of your repositories.

The result is that one *successful* build is associated with that commit. On the commit page you should now see a
**Builds** section with `1 build` and a green check mark, like this:

![One Green Build](/server/bitbucket/images/1-build-green.png)

### Two green builds

Posting a second successful build result to the same URL will update the commit's build status to include two successful builds.

```sh
curl -u username:password -H "Content-Type: application/json" -X POST http://localhost:7990/bitbucket/rest/build-status/1.0/commits/9e72f04322c4a1f240e0b3158c67c3c19cdd16e7 -d @build1.json
```

Where `build1.json` contains:

```json
{
    "state": "SUCCESSFUL",
    "key": "REPO-MASTER-INTEGRATION",
    "name": "REPO-MASTER-INTEGRATION-17",
    "url": "https://bamboo.example.com/browse/REPO-MASTER-INTEGRATION-17",
    "description": "Changes by John Doe"
}
```

This updates the build status shown in the UI:

![Two Green Builds](/server/bitbucket/images/2-builds-green.png)

Sweet! Two successful builds. But wait..

### Oh no! A red build

Posting a failed build result to the same URL will update the commit's build status to *failed*.

```sh
curl -u username:password -H "Content-Type: application/json" -X POST http://localhost:7990/bitbucket/rest/build-status/1.0/commits/9e72f04322c4a1f240e0b3158c67c3c19cdd16e7 -d @build2.json
```

Where `build2.json` contains:

```json
{
    "state": "FAILED",
    "key": "REPO-MASTER-DAO",
    "name": "REPO-MASTER-DAO-33",
    "url": "https://bamboo.example.com/browse/REPO-MASTER-DAO-33",
    "description": "Changes by John Doe"
}
```

This updates the build status shown in the UI to indicate that there is at least one failing build:

![One Green Build](/server/bitbucket/images/1-build-red.png)

Clicking on the build status link in the UI will display a list of all builds associated with the commit. It looks
like this:

![One Green Build](/server/bitbucket/images/build-list.png)

Note that the `name` and `description` fields from the posted `JSON` objects for each build result are being displayed
to the user. The name of each build is linked to the provided `url`.

### False alarm, it's green now

Perhaps that build failed due to some infrastructure issue. Let's pretend that the build has been re-run and a new
successful result has been posted to our commit's build status URL:

```sh
curl -u username:password -H "Content-Type: application/json" -X POST http://localhost:7990/bitbucket/rest/build-status/1.0/commits/9e72f04322c4a1f240e0b3158c67c3c19cdd16e7 -d @build3.json
```

Where `build3.json` contains:

```json
{
    "state": "SUCCESSFUL",
    "key": "REPO-MASTER-DAO",
    "name": "REPO-MASTER-DAO-34",
    "url": "https://bamboo.example.com/browse/REPO-MASTER-DAO-34",
    "description": "Re-run by Sys Admin"
}
```

Since this update has the same `key` as the previously failing build result, it overwrites it. This updates the UI,
showing users that there are now three passing builds:

![One Green Build](/server/bitbucket/images/3-builds-green.png)

