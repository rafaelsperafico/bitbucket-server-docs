---
title: Index page for Bitbucket Server Documentation
platform: server
product: bitbucketserver
category: devguide
subcategory: index
date: "2017-12-15"
---
# Latest Updates
We release new versions of Bitbucket Server frequently. As a Bitbucket Server developer it's important that you're
aware of the changes. The resources below will help you keep track of what's happening.

## API Changelog

Our API changelog is available [here](/server/bitbucket/reference/api-changelog).

## Atlassian Developer Blog

In the **Atlassian Developer blog** you'll find handy tips and articles related to Bitbucket development.

Check it out and subscribe here: [Atlassian Developer blog](/blog/categories/bitbucket-server/) *(Bitbucket-related posts)*.

