---
title: Adding commit metadata
platform: server
product: bitbucketserver
category: devguide
subcategory: learning
date: "2018-08-29"
---
# Adding commit metadata

Bitbucket Server provides a number of places plugins can display extra information about a commit, in fact we do precisely 
that with [Jira integration](http://quickstart.atlassian.com/download/stash/jira-integration) and
[build status](../../how-tos/updating-build-status-for-commits).

## Adding to the UI

Bitbucket Server adds commit metadata in a few places, the commits list (viewable in pull requests and general repository browsing)
and commits (usually navigated to by clicking a commit hashed id link).

### Commits list

For both [Jira integration](http://quickstart.atlassian.com/download/stash/jira-integration) and [build status](../../how-tos/updating-build-status-for-commits)
 we conditionally add a column to the commits list in Bitbucket Server. Plugins can do the same by by adding a [web section](../../reference/web-fragments) for the column to `bitbucket.commits.extras`.

### Commit view

Bitbucket Server allows you to extend the commit page using [a web panel extension point](../../reference/web-fragments).
We do this ourselves for the Jira integration using the `bitbucket.commit.related-entities` extension point. You take a look at the extension points available for yourself by navigating
to a commit and appending `?web.panels` to the URL for example
`https://bitbucket.example.com/projects/TEST/repos/test-repo/commits/5284ad9879d696876a16bdac71694237292fd93a?web.panels`

## Storing metadata

We use a few different methods to store the data we show in the UI.

### Commit Indexer

The [commit indexer](../../reference/plugin-module-types/commit-indexer) manages whether or not you have seen a commit before and
provides a way of storing a small amount of data against a commit. Bitbucket Server's Jira integration uses the commit indexer to be notified of new
commits then extract and store Jira issue keys from the commit messages.

### General Data Storage

We usually use [Active Objects](https://developer.atlassian.com/display/DOCS/Active+Objects) to store data in plugins,
 for example in the build status plugin. It is also possible to store simple key-value data in the
[plugin settings service](https://developer.atlassian.com/docs/common-coding-tasks/storing-plugin-settings) 
[example](https://developer.atlassian.com/docs/atlassian-platform-common-components/shared-access-layer/sal-code-samples)

## Accessing your data in the UI

There are a few methods to access data from the server, Bitbucket Server's Jira integration plugin uses a
[context provider](https://developer.atlassian.com/confdev/confluence-plugin-guide/confluence-plugin-module-types/web-ui-modules/web-item-plugin-module#WebItemPluginModule-Context-providerElement)
to supply data from the commit index which is used in its soy template.

Our build status plugin uses its own
[REST endpoint](https://developer.atlassian.com/static/rest/bitbucket-server/latest/bitbucket-build-rest.html)
to fetch data on page load. You can
[create your own REST plugin module](https://developer.atlassian.com/docs/atlassian-platform-common-components/rest-api-development/rest-plugin-module) 
to expose services and data entities as REST APIs. You can use [AJAX via jQuery](http://api.jquery.com/jQuery.ajax/) to call
your own REST endpoint, there’s really not much to it:

```javascript
$.ajax(AJS.contextPath() + "/rest/api/1.0/projects", {
        type: "GET",
        success: function(data) {
            //process the returned data
        },
        contentType: "application/json",
});
```
