---
title: Hosting events
platform: server
product: bitbucketserver
category: devguide
subcategory: learning
date: "2017-12-14"
---
# Hosting events

## Overview

Bitbucket Server 
[provides events](https://developer.atlassian.com/static/javadoc/bitbucket-server/latest/api/reference/com/atlassian/bitbucket/event/ApplicationEvent.html)
which can be used to hook into SCM hosting operations. Of particular interest for hosting are the
[push event](https://developer.atlassian.com/static/javadoc/bitbucket-server/latest/api/reference/com/atlassian/bitbucket/event/repository/RepositoryPushEvent.html)
and the
[pull event](https://developer.atlassian.com/static/javadoc/bitbucket-server/latest/api/reference/com/atlassian/bitbucket/event/repository/RepositoryPullEvent.html).

Using Bitbucket Server events you can respond to user operations which [repository hooks](../../how-tos/hooks-merge-checks-guide) cannot. 
You can also replicate the behavior of global hooks.

## Example: using an event to replicate global hooks

**MyBitbucketEventListener.java**

```java
package com.atlassian.bitbucket.example;

import com.atlassian.event.api.EventListener;

public class MyBitbucketEventListener 
{
    @EventListener
    public void mylistener(RepositoryPushEvent pushEvent) 
    {
        // call to your psuedo-hook with pushEvent.getRepository() 
	// and pushEvent.getRefChanges()
    }
}
```

Note: you won't be able to reject a push or write information back to the client using this method.

Some examples of where we use these events in Bitbucket Server:

- In pull requests, to work out if we need to update a pull request based on a client's push.
- In branch permissions, to work out if we can delete a branch permission (when the branch is deleted).

Responding to application events is [detailed here](../../how-tos/responding-to-application-events).
