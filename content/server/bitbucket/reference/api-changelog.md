---
title:  API Changelog
platform: server
product: bitbucketserver
category: reference
subcategory: other
date: "2017-12-14"
---
#  API Changelog

## Compatibility Policy

Bitbucket Server (formerly Stash) aims to maintain API compatibility between minor releases (e.g. between 5.0 and 5.1).
Methods and classes marked as deprecated will be removed in the next major release. For example, methods marked as
deprecated in Bitbucket Server 5.1 or 5.2 will be removed in Bitbucket Server 6.0.

## Expected future changes

### Hard breakage when plugins use internal non-API JS modules

Though they were never considered a stable API, some plugins have imported and relied on JS modules in the
`bitbucket/internal/*` namespace.

These usages will begin to break in a future release of Bitbucket Server as we migrate our internal modules into Webpack
bundles that aren't accessible from code outside the bundle.

Please modify your JS to stop relying on modules in the `bitbucket/internal/*` namespace. If there is a particular module
that would be useful to you as API, please
[raise a Suggestion](https://jira.atlassian.com/secure/CreateIssueDetails!init.jspa?pid=13310&issuetype=10000&summary=JS%20API%20request%20for%20___)

### Pull request server-rendered plugins

Some plugin points on the pull request page are rendered server-side, and some are rendered client-side. As more of this
page is rendered client-side, we will be deprecating the server-rendered plugin points in favor of client-rendered ones.

The following locations, sections, and decorators will be converted from server-rendered to
[client-rendered plugin points](../plugin-module-types/client-web-fragment), or removed entirely, in a future
version of Bitbucket Server:

#### Locations and sections
* bitbucket.pull-request.nav.overview.meta
* bitbucket.pull-request.nav.diff.meta
* bitbucket.pull-request.nav.commits.meta
* bitbucket.repository.difftree.header

When they are removed, we will make our best effort to support them in an altered, deprecated fashion until
the next major release. See the Bitbucket Server 4.4 notes below for how this will likely occur. To guard against this,
we recommend that you rely only on your own `styleClass` and `link id` when selecting your elements, and to select them
using jQuery live/delegated events.

## Bitbucket Server 5.16

### User Erasure API
In Bitbucket Server 5.16 we have introduced the ability for admins to erase personally identifiable user data for a deleted
user. Personally identifiable user data stored in third party plugins will not be erased, unless those plugins implement their
own `com.atlassian.bitbucket.user.UserErasureHandler`. For the erasure process to pick up these implementations, they have to
be defined as a `com.atlassian.bitbucket.user.UserErasureModuleDescriptor` module in the plugin's `atlassian-plugin.xml` file.
See the [module description for `<user-erasure-handler>`](https://developer.atlassian.com/server/bitbucket/reference/plugin-module-types/user-erasure/)
for more information.

## Bitbucket Server 5.15

### Code Insights API
In Bitbucket Server 5.15 we have introduced a feature to display the results of static analysis on a pull request
in the form of a report and annotations. This has included both Java and REST API for adding, modifying and
deleting reports and annotations. See [this how-to guide](../../how-tos/code-insights) for more details.

## Bitbucket Server 5.14

### Data Center Migrations API
In Bitbucket Server 5.14 we have introduced a feature for migrating projects and repositories between Bitbucket installations. 
Data provided by third party plugins will not be migrated, unless those plugins implement their own 
`com.atlassian.bitbucket.migration.Exporter` and `com.atlassian.bitbucket.migration.Importer`. For the migration process to
pick up these implementations, they have to be defined as a `com.atlassian.bitbucket.migration.MigrationHandlerModuleDescriptor` 
module in the plugin's `atlassian-plugin.xml` file. See the [module description for `<migration-handler>`](https://developer.atlassian.com/server/bitbucket/reference/plugin-module-types/data-center-migration/) 
for more information. 

## Bitbucket Server 5.11

### Pull Request Commit API
In Bitbucket Server 5.11 we are adding Java and REST API methods for retrieving the pull requests containing a
specified commit. `PullRequestService.countByCommit` and `PullRequestService.searchByCommit` introduce the ability
to count or search pull requests associated with a specific commit. A new REST endpoint 
(`/rest/api/latest/projects/KEY/repos/SLUG/commits/{commitId}/pull-requests`) allows retrieval of associated pull
requests via REST.

## Bitbucket Server 5.10

### Deprecated direct access to repositories on disk

In Bitbucket Server 5.10 we've made the difficult decision to deprecate direct access to our repositories on disk for
apps. The ability to access to repositories on disk will be removed in 6.0. Deprecated interfaces and methods which will
be removed _without replacement_ include:

* `ApplicationPropertiesService.getRepositoriesDir()`
* `ApplicationPropertiesService.getRepositoryDir(Repository)`
* `GitAgent` (All methods)
  * Accessing the default branch and resolving refs should be done using `RefService` instead
* `GitScmConfig` (All methods)
  * `GitScm.getVersion()` has been added to continue to provide access to the detected Git version
  * All other methods have no planned replacements
* `ScmHookDetails.getEnvironment()`

Apps which directly access repository directories and use JGit to interact with their contents will no longer be able to
do so. Apps which otherwise read or modify repository contents, such as installing, updating or removing hooks, will no
longer be able to do so. Executing processes other than `git` in repository directories will also no longer be possible.

Apps which use API services like `CommitService`, `ContentService` and `RefService` will be unaffected. Additionally, our
`CommandBuilder` API (including `GitCommandBuilderFactory` and `GitScmCommandBuilder`) is also unaffected, continuing to
allow app developers the ability to build and execute arbitrary `git` commands. Going forward, our APIs will be the only
available mechanism for interacting with repository contents.

Building a system which can scale from 10 users to 50,000+ users is complicated, and there's a balance that has to be struck
between our desire to provide a flexible, rich API and SPI to empower our ecosystem and the need to continuously improve how
the system scales for ever-increasing user counts. We recognize that this is a decision which will negatively impact some
existing apps, and those who use them. It's been a difficult decision to make internally, and one we take very seriously.
However, as we consider our roadmap going forward, and the features we want to add, especially around scalability, it has
become apparent that it will not be possible to deliver on our roadmap the way we want to while apps are able to make direct
modifications to repositories without using our API--aside from retrieving the directory--to do so. Supporting direct access
to repositories severely limits our guarantees around repository state, and about when repositories are and are not being
updated, and those limitations in turn prevent us from implementing much-requested features like read-only mode and sharded
repository storage.

### New Watcher API

In Bitbucket Server 5.10 we are adding an API service for managing watchable entities: the `WatcherService`.
`Watchable.getWatchers()` has been deprecated and `WatcherService.search` should be used instead. The existing `Watchable`'s
(`CommitDiscussion` and `PullRequest`) will still return their watchers from `getWatchers()` until the method is removed in 6.0.
`Repository` is a new `Watchable` and has had a `getWatchers()` method added to it for compatibility, but the method is a no-op
and will be removed in 6.0 also.

## Bitbucket Server 5.5

### New Permission API
In Bitbucket Server 5.5 we are adding API to allow plugins to restrict or extend the permissions granted to a user on
a resource. Plugins may implement `PermissionVoterProvider` to create a `PermissionVoter` and register the provider
in the `permission-voter-provider` tag in `atlassian-plugin.xml` in order to 'vote' on whether a user is allowed access 
to a given resource. For the permission check to succeed, at least one `PermissionVoter` must return a result of
`PermissionVote.GRANT`. If all `PermissionVoter`s abstain, or any `PermissionVoter` returns a result of `PermissionVote.VETO`,
the permission check is unsuccessful and the user is not allowed to carry out the requested operation.

In addition to this, we are also adding a new permission level, `Permission.USER_ADMIN`, which represents access to change
the account configuration of a user (such as SSH keys, GPG keys, personal tokens and password). The permission cannot be
granted to any user/group, but callers can check for this permission using the newly added
`PermissionService.hasUserPermission` methods.

### Updated Authentication API
`HttpAuthenticationHandler.authenticate` and `SshAuthenticationHandler.authenticate` return an `ApplicationUser` only, 
with no ability for `AuthenticationHandler`s to add their own context. This method has been deprecated (for removal in 6.0) 
in favor of the new `HttpAuthenticationHandler.performAuthentication` and `SshAuthenticationHandler.performAuthentication`
which returns an `AuthenticationResult` which contains the authenticated `ApplicationUser` and authentication properties 
provided by the handler.

## Bitbucket Server 5.4

### New Webhooks API
In Bitbucket Server 5.4 we are adding a Webhooks API to allow plugins to publish webhooks. Plugins will be able to import
the `WebhooksService` from `com.atlassian.webhooks` to use this new feature. Plugins are also able to register to filter
webhook invocations, as well as enrich invocations. This allows fine grained control over which webhooks are fired, as
well as the http details for each of them. To find out more, check out `WebhookFilter` and `WebhookRequestEnricher`.

If your plugin would like to set the payload body of a webhook, please export a `WebhookPayloadProvider`. By default
Bitbucket knows how to serialize certain application events, but if you're publishing custom webhooks, ensure that you
provide a payload provider if you require a HTTP body in your webhook.

### net.i2p.crypto.eddsa\* packages are no longer OSGi-exported

Bitbucket Server 5.0.0 added an OSGi export for `net.i2p.crypto.eddsa*` packages as part of upgrading the SSH library
the system uses. Bitbucket Server 5.4.0 removes that export, as newer versions of the SSH library require a new version
of the `net.i2p.crypto:eddsa` library which is API-incompatible with the previously-exported version.

Any app relying on `net.i2p.crypto.eddsa` packages via an OSGi import should be updated to bundle their required
version of the library instead.

## Bitbucket Server 5.2

### Updated Hooks API

In Bitbucket Server 5.2 we are updating the hooks API to allow hooks to be configured at both a `Project` and a
`Repsitory` level. Because of this, methods on the `RepositoryHookService` will now take a `Scope` object rather than
a `Repository` object to encapsulate the `Project` or `Repository` scope to which the settings and enabled/disabled
state can be applied. Additionally, in order to make the hooks API more usable, many methods now take 'request objects'
(e.g. `DeleteRepositoryHookRequest`, `RepositoryHookSearchRequest`, `SetRepositoryHookSettingsRequest`) which is a wrapper
around the `Scope` and other arguments required by the method. For more information on the new API and how to use it,
see the [Repository Hooks and Merge Checks Guide](../../how-tos/hooks-merge-checks-guide)

## Bitbucket Server 5.1

### Pull Request Deletion

In Bitbucket Server 5.1 we are introducing the ability to delete pull requests. A new method is available on the 
`PullRequestService` to trigger a pull request deletion. Furthermore a cancelable `PullRequestDeletionRequestedEvent`
was added, which allows plugins to veto a pull request's deletion.
We're calling attention to this change to remind plugin and integration developers that pull requests and all their 
associated data, like comments, _can_ disappear, and any code integrating with Bitbucket needs to account for this.

## Bitbucket Server 5.0

### New Comment API

As announced in the 4.11 release the Comment API has been replaced with a more usable and maintainable alternative.

A `Comment` now has a reference to a `CommentThread` which binds an entire conversation together. Comment threads
provide API clients with access to:
 * the entire comment graph through it's `rootComment`
 * an optional `CommentThreadDiffAnchor` (not available for pull request general comments)
 which points to the location in the diff where the comment was added
 * the `Commentable` (currently either a `PullRequest` or a `CommitDiscussion`) which serves as the context
 for the thread

We have also introduced the `CommentService` which centralizes all comment-related APIs removing the duplication
previously seen in `CommitService` and `PullRequestService`.

### New Hooks API

In Bitbucket Server 5.0 we are introducing a new API for Hooks. This API will replace the existing hooks API and
the merge checks API.
This new API makes retrieving added and removed commits much simpler and more efficient. For more information on the
new API and how to use it, see the [Repository Hooks and Merge Checks Guide](../../how-tos/hooks-merge-checks-guide).

The legacy Hooks API has been deprecated for removal in Bitbucket Server 6.0.

### Deprecated Java APIs removed

Interfaces, classes, and methods in the Bitbucket Server Java API that were previously marked as deprecated have been
removed.

Plugins that use any of these interfaces (which would have generated deprecation warnings when built against Bitbucket
Server 4.x) generally won't build with Bitbucket Server 5.x

Precompiled plugins that used any of the removed interfaces will fail to install or
run in Bitbucket Server 5.x, typically with `java.lang.NoSuchMethodError`.

Deprecated APIs have been documented in the
[Bitbucket Server 4.14 Java API Reference](https://developer.atlassian.com/bitbucket/server/docs/4.14.4/reference/java-api.html)
for Bitbucket Server 5.0.

The following classes and interfaces have been removed in Bitbucket Server 5.0. Please see the linked Javadoc for
Bitbucket Server 4.14 for informing regarding deprecation and possible alternative classes and interfaces.

* [`AbstractAddCommentRequest`](https://developer.atlassian.com/static/javadoc/bitbucket-server/4.14.4/api/reference/com/atlassian/bitbucket/comment/AbstractAddCommentRequest.html)
* [`AbstractAddFileCommentRequest`](https://developer.atlassian.com/static/javadoc/bitbucket-server/4.14.4/api/reference/com/atlassian/bitbucket/comment/AbstractAddFileCommentRequest.html)
* [`AbstractAddLineCommentRequest`](https://developer.atlassian.com/static/javadoc/bitbucket-server/4.14.4/api/reference/com/atlassian/bitbucket/comment/AbstractAddLineCommentRequest.html)
* [`AddDiffCommentRequest`](https://developer.atlassian.com/static/javadoc/bitbucket-server/4.14.4/api/reference/com/atlassian/bitbucket/comment/AddDiffCommentRequest.html)
* [`DiffCommentAnchor`](https://developer.atlassian.com/static/javadoc/bitbucket-server/4.14.4/api/reference/com/atlassian/bitbucket/comment/DiffCommentAnchor.html)
* [`AddCommitFileCommentRequest`](https://developer.atlassian.com/static/javadoc/bitbucket-server/4.14.4/api/reference/com/atlassian/bitbucket/comment/commit/AddCommitFileCommentRequest.html)
* [`AddCommitLineCommentRequest`](https://developer.atlassian.com/static/javadoc/bitbucket-server/4.14.4/api/reference/com/atlassian/bitbucket/comment/commit/AddCommitLineCommentRequest.html)
* [`AddPullRequestFileCommentRequest`](https://developer.atlassian.com/static/javadoc/bitbucket-server/4.14.4/api/reference/com/atlassian/bitbucket/comment/pull/AddPullRequestFileCommentRequest.html)
* [`AddPullRequestLineCommentRequest`](https://developer.atlassian.com/static/javadoc/bitbucket-server/4.14.4/api/reference/com/atlassian/bitbucket/comment/pull/AddPullRequestLineCommentRequest.html)
* [`CommitCommentAnchorSearchRequest`](https://developer.atlassian.com/static/javadoc/bitbucket-server/4.14.4/api/reference/com/atlassian/bitbucket/commit/CommitCommentAnchorSearchRequest.html)
* [`CommitDiscussionCommentAnchor`](https://developer.atlassian.com/static/javadoc/bitbucket-server/4.14.4/api/reference/com/atlassian/bitbucket/commit/CommitDiscussionCommentAnchor.html)
* [`VersionTracker`](https://developer.atlassian.com/static/javadoc/bitbucket-server/4.14.4/api/reference/com/atlassian/bitbucket/concurrent/VersionTracker.html)
* [`FileEditCanceledException`](https://developer.atlassian.com/static/javadoc/bitbucket-server/4.14.4/api/reference/com/atlassian/bitbucket/content/FileEditCanceledException.html)
* [`BranchCreationCanceledException`](https://developer.atlassian.com/static/javadoc/bitbucket-server/4.14.4/api/reference/com/atlassian/bitbucket/event/branch/BranchCreationCanceledException.html)
* [`BranchDeletionCanceledException`](https://developer.atlassian.com/static/javadoc/bitbucket-server/4.14.4/api/reference/com/atlassian/bitbucket/event/branch/BranchDeletionCanceledException.html)
* [`PullRequestApprovalEvent`](https://developer.atlassian.com/static/javadoc/bitbucket-server/4.14.4/api/reference/com/atlassian/bitbucket/event/pull/PullRequestApprovalEvent.html)
* [`PullRequestApprovedEvent`](https://developer.atlassian.com/static/javadoc/bitbucket-server/4.14.4/api/reference/com/atlassian/bitbucket/event/pull/PullRequestApprovedEvent.html)
* [`PullRequestRolesUpdatedEvent`](https://developer.atlassian.com/static/javadoc/bitbucket-server/4.14.4/api/reference/com/atlassian/bitbucket/event/pull/PullRequestRolesUpdatedEvent.html)
* [`PullRequestUnapprovedEvent`](https://developer.atlassian.com/static/javadoc/bitbucket-server/4.14.4/api/reference/com/atlassian/bitbucket/event/pull/PullRequestUnapprovedEvent.html)
* [`TagCreationCanceledException`](https://developer.atlassian.com/static/javadoc/bitbucket-server/4.14.4/api/reference/com/atlassian/bitbucket/event/tag/TagCreationCanceledException.html)
* [`TagDeletionCanceledException`](https://developer.atlassian.com/static/javadoc/bitbucket-server/4.14.4/api/reference/com/atlassian/bitbucket/event/tag/TagDeletionCanceledException.html)
* [`PostReceiveHookModuleDescriptor`](https://developer.atlassian.com/static/javadoc/bitbucket-server/4.14.4/spi/reference/com/atlassian/bitbucket/hook/PostReceiveHookModuleDescriptor.html)
* [`PreReceiveHookModuleDescriptor`](https://developer.atlassian.com/static/javadoc/bitbucket-server/4.14.4/spi/reference/com/atlassian/bitbucket/hook/PreReceiveHookModuleDescriptor.html)
* [`CommitCommentAddedPullRequestNotification`](https://developer.atlassian.com/static/javadoc/bitbucket-server/4.14.4/spi/reference/com/atlassian/bitbucket/notification/pull/CommitCommentAddedPullRequestNotification.html)
* [`CommitCommentRepliedPullRequestNotification`](https://developer.atlassian.com/static/javadoc/bitbucket-server/4.14.4/spi/reference/com/atlassian/bitbucket/notification/pull/CommitCommentRepliedPullRequestNotification.html)
* [`PullRequestCommentAnchorDiffType`](https://developer.atlassian.com/static/javadoc/bitbucket-server/4.14.4/api/reference/com/atlassian/bitbucket/pull/PullRequestCommentAnchorDiffType.html)
* [`PullRequestDiffCommentAnchor`](https://developer.atlassian.com/static/javadoc/bitbucket-server/4.14.4/api/reference/com/atlassian/bitbucket/pull/PullRequestDiffCommentAnchor.html)
* [`PullRequestMergeCanceledException`](https://developer.atlassian.com/static/javadoc/bitbucket-server/4.14.4/api/reference/com/atlassian/bitbucket/pull/PullRequestMergeCanceledException.html)
* [`PullRequestRescopeAnalyzer`](https://developer.atlassian.com/static/javadoc/bitbucket-server/4.14.4/scm-common/reference/com/atlassian/bitbucket/pull/PullRequestRescopeAnalyzer.html)
* [`RestDiffCommentAnchor`](https://developer.atlassian.com/static/javadoc/bitbucket-server/4.14.4/rest-model/reference/com/atlassian/bitbucket/rest/comment/RestDiffCommentAnchor.html)
* [`RestPullRequestDiffCommentAnchor`](https://developer.atlassian.com/static/javadoc/bitbucket-server/4.14.4/rest-model/reference/com/atlassian/bitbucket/rest/pull/RestPullRequestDiffCommentAnchor.html)
* [`MergeCanceledException`](https://developer.atlassian.com/static/javadoc/bitbucket-server/4.14.4/api/reference/com/atlassian/bitbucket/scm/MergeCanceledException.html)
* [`MergeRequestCheckModuleDescriptor`](https://developer.atlassian.com/static/javadoc/bitbucket-server/4.14.4/spi/reference/com/atlassian/bitbucket/scm/pull/MergeRequestCheckModuleDescriptor.html)
* [`RepositoryRescopeCommandParameters`](https://developer.atlassian.com/static/javadoc/bitbucket-server/4.14.4/spi/reference/com/atlassian/bitbucket/scm/pull/RepositoryRescopeCommandParameters.html)
* [`HasPullRequestApproveCondition`](https://developer.atlassian.com/static/javadoc/bitbucket-server/4.14.4/web-common/reference/com/atlassian/bitbucket/web/conditions/HasPullRequestApproveCondition.html)
* [`IsNormalUserCondition`](https://developer.atlassian.com/static/javadoc/bitbucket-server/4.14.4/web-common/reference/com/atlassian/bitbucket/web/conditions/IsNormalUserCondition.html)

In addition, deprecated methods in existing classes and interfaces across the whole
Bitbucket Server API have been removed.

Consult the [Bitbucket Server 4.14 Java API Reference](https://developer.atlassian.com/bitbucket/server/docs/4.14.4/reference/java-api.html)
for details on the removed methods, and their alternatives in Bitbucket Server 5.x.

## Bitbucket Server 4.13

### Mockito 2.x upgrade

Although the `bitbucket-parent` Maven POM file is not considered stable API, an upgrade to the version of the Mockito
mocking framework is worth noting. Plugins that either use `bitbucket-parent` as their parent POM or import it for
the purposes of "dependencyManagement" may see changes to test code as the specific Mockito version dependency may
be inherited from `bitbucket-parent`.

*Note: The Mockito framework is a test dependency only and its upgrade will not impact a plugin's compatibility with
Bitbucket Server 4.13.*

The upgrade from Mockito 1.x to 2.x involves a number of breaking changes. Plugin developers are encouraged to update
unit tests where Mockito is used, and can find more information at [What's new in Mockito 2](https://github.com/mockito/mockito/wiki/What's-new-in-Mockito-2).

For plugin developers who wish to depend on the `bitbucket-parent` artifact at version 4.13 or later, but still wish
to continue using Mockito 1.x, can override the dependency in the POM for their own plugin. Simply add the following
dependency to your plugin's pom.xml:

```xml
<dependencies>
    <dependency>
        <groupId>org.mockito</groupId>
        <artifactId>mockito-core</artifactId>
        <version>1.10.19</version>
        <scope>test</scope>
    </dependency>
</dependencies>
```

## Bitbucket Server 4.11

### Comments API deprecation

In this release we've deprecated the Comments API to make way for a more usable and maintainable alternative.

In our efforts to evolve, maintain and consume the API we've found:

* Conversations are not explicitly represented. This causes a problem when trying to access conversation
metadata (such as the anchor or the `Commentable`) from a `Comment`
* Methods are duplicated across services
* Tables are duplicated in the database
* There are schema issues when backing up and restoring data in clustered systems

The upcoming API (to be published in our 5.0 release) will address these issues through the following semantic changes (and possibly others):

* A new `CommentThread` interface will be introduced to be the common denominator in a conversation
* A `Comment` will link back to the thread it belongs to for easy access to the conversation
* `Comment` anchors will be redesigned to remove duplicate information
* A `CommentService` will be made available to simplify how plugin developers create, edit and remove comments
* Event and activity items produced by the comment API will be replaced (and in some cases removed)

## Bitbucket Server 4.10

### AUI bundled Raphael is deprecated

In Bitbucket Server 4.10 we upgraded AUI to version 6.0.0, which no longer bundles [Raphael](https://dmitrybaranovskiy.github.io/raphael/).
The Raphael bundled with AUI was never a part of our API, it was an AUI implementation detail, however we have included
a separate Raphael dependency in this version as we are aware of some apps using the Raphael that AUI exposed. This
dependency is deprecated and will be removed in Bitbucket Server 5.0.  If your app requires Raphael, you should
bundle your own version of Raphael (or another drawing library).

## Bitbucket Server 4.6

### Shared Access Layer (SAL) LocaleResolver getLocale implementation change

Prior to Bitbucket Server 4.6 in the case where a user had set their Bitbucket Server language in their Account settings
to "Automatically detect browser setting", and had a language set in their browser that was not one of the languages for
which a language pack was installed in Bitbucket Server, calling `getLocale` on the SAL `LocaleResolver` would return
a `Locale` based on the preferred locale they had set in their browser.

In Bitbucket Server 4.6+ the Locale returned will be the Locale that is the closest match for an installed language.
This is a more correct implementation of the `LocaleResolver` API description which states that `getLocale` should
return "the Locale that should be used in internationalization and localization", as now this Locale will match that
used by other parts of the system for internationalization and localization.

## Bitbucket Server 4.4

### Pull request toolbar plugins moved and deprecated

The web sections and web items at the following locations and sections have been deprecated:

* bitbucket.pull-request.toolbar
* bitbucket.pull-request.toolbar.actions
* bitbucket.pull-request.toolbar.section.user-actions

The web items have been moved into a '...' dropdown menu as menu items, and if you were relying on specific DOM
rendering of your plugin, it may cease to function. We no longer refresh the page after a merge or decline, and a
reviewer can be added to the pull request without a page refresh. This means your web-item may be out of date in these
cases.

A live JS event handling the click event of elements matching your `styleClass` or `<link id="" />` will continue to
function until 5.0. You should convert your web-items into [client-web-items](../plugin-module-types/client-web-item)
and add them directly to the new section 'bitbucket.pullrequest.action'. You should remove any intermediary web-sections.

### Roles updated event has been deprecated

`PullRequestRolesUpdatedEvent` has been deprecated for removal in 5.0. Plugin developers who wish to be notified
when a user is added/removed as a reviewer should use `PullRequestReviewersUpdatedEvent` instead

## Bitbucket Server 4.2

### This public constructor on FileContentCache has been deprecated

FileContentCache now requires a `FileContentCacheManager` as a constructor dependency. The recommended way to build a
`FileContentCache` is now to use the `FileContentCacheManager.Builder` a `FileContentCache` can then be created by
calling `FileContentCacheManager#createContentCache`. *NOTE* Special care must be taken when migrating to the
`FileContentCacheManager` to ensure that `cacheDirectory` paths remain correct.

e.g.

```java
File cacheDirectory = new File(propertiesService.getCacheDir(), CACHE_REGION);
new FileContentCache(CACHE_KEY, cacheDirectory, new TtlCacheExpiryStrategy(), minFreeSpace, cachePump);
```

becomes

```java
File cacheDirectory = new File(propertiesService.getCacheDir());
FileContentCache cache = new FileContentCacheManager.Builder(cacheDirectory)
            .minFreeSpaceBytes(minFreeSpace)
            .streamPumper(cachePump)
            .build()
            .getCache(CACHE_REGION);
```

## Stash 3.10

### Branch permissions get an update

The `BranchPermissionService` and version 1.0 of the branch permissions REST API are being deprecated.

In Stash 3.10 we are introducing several new types of branch permissions and are adding the ability to add restrictions
based on the branching model.

As a result, we are introducing a new service to interact with branch permissions, the
[`RefRestrictionService`](https://developer.atlassian.com/static/javadoc/stash/3.11.0/ref-restriction-api/reference/com/atlassian/stash/repository/ref/restriction/RefRestrictionService.html).
This service can be accessed by adding `ref-restriction-api` to your `pom.xml` as a dependency using:

```xml
<dependency>
    <groupId>com.atlassian.stash</groupId>
    <artifactId>stash-ref-restriction-api</artifactId>
    <scope>provided</scope>
</dependency>
```

Along with this change, we are updating the version of the branch permissions REST API to version 2.0.
It is important to note that any REST calls to branch permissions version 1.0 using 'latest' in the REST URLs should
be changed to use '1.0' instead, as version 2.0 of the REST API is not backwards compatible.

We've added new APIs to create, update and remove branch permissions:

* in your Java plugin code you can use [`RefRestrictionService`](https://developer.atlassian.com/static/javadoc/stash/3.11.0/ref-restriction-api/reference/com/atlassian/stash/repository/ref/restriction/RefRestrictionService.html)
* remote and browser clients can use [its REST API](https://developer.atlassian.com/static/rest/stash/3.11.0/stash-branch-permissions-rest.html)

Additionally we have added the `ref-restriction-spi`, which provides interfaces to allow plugin developers to create
their own `RefMatcher` implementations. This opens up the possibility of customizing what type of `Ref` is restricted
by a given branch permission. Add the following to your dependencies to use the `ref-restriction-spi`:

```xml
<dependency>
    <groupId>com.atlassian.stash</groupId>
    <artifactId>stash-ref-restriction-spi</artifactId>
    <scope>provided</scope>
</dependency>
```

### RepositoryMetadataService has been renamed to `RefService`

The `RepositoryMetadataService` is used to retrieve branches and tags for a repository. The "repository metadata" name
isn't very self-documenting. Since the service is used to interact with the refs in a repository, it has been renamed
to the `RefService`. The `RepositoryMetadataService` will remain available to plugins until it is removed in with the
4.0 release.

### ChangesetReader is deprecated; use CommitReader

As part of the ongoing work to normalize the API to use "commit" instead of "changeset", the `ChangesetReader` has
been deprecated and `CommitReader` has been added to take its place.

The format constants previously public on `ChangesetReader` are now private on `CommitReader`. Having the constants
exposed made changing the format a semantically incompatible change because any plugins relying directly on those
constants and not using the `ChangesetReadert` to parse the output would be broken. If plugins are parsing output
for themselves, they should use their own format. If they're using the `CommitReader` to parse the output, they
should call `getFormat()` on the instantiated reader to get the format and make _no assumptions_ about the format
returned.

## Stash 3.7

### Changeset is deprecated; long live Commit

Throughout the Stash codebase there's been a bit of a split between the term "changeset" and the term "commit". The two
have often been used interchangeably, but each represents a distinct concept:

* A _commit_ represents a snapshot of a _complete repository_ at a given point in time. It captures metadata about who
  authored the changes and when as well as, optionally, a message about why the changes were made.
    * Different SCMs may include additional metadata in their commits, but Stash's API doesn't include it
    * This was represented by `Changeset` in Stash
* A _changeset_ represents how the repository's contents changed _between_ two commits
    * Merge commits, for example, have _at least two_ changesets
    * This was represented by `DetailedChangeset` in Stash because the name `Changeset` was already taken

Starting from 3.7 Stash's API is being normalized, replacing `Changeset` with `Commit` and replacing `DetailedChangeset`
with `Changeset`. Because this normalization will touch so many classes and interfaces, the deprecated elements will be
retained through the _entire 4.x release cycle_ and will be removed in 4.0. This means plugins developed using the now-
deprecated names will continue to work, unchanged, until 4.0.

Stash 3.7 includes the following changes related to this API normalization:

* The existing [`MinimalChangeset`](https://developer.atlassian.com/static/javadoc/stash/3.11.0/api/reference/com/atlassian/stash/content/MinimalChangeset.html),
  [`Changeset`](https://developer.atlassian.com/static/javadoc/stash/3.11.0/api/reference/com/atlassian/stash/content/Changeset.html)
  and [`DetailedChangeset`](https://developer.atlassian.com/static/javadoc/stash/3.11.0/api/reference/com/atlassian/stash/content/DetailedChangeset.html)
  interfaces have been deprecated
    * All of the related data classes (`InternalMinimalChangeset`, `InternalChangeset` and `InternalDetailedChangeset`)
      have been deprecated
    * All of the related request objects (`ChangesetsBetweenRequest`, `DetailedChangesetsRequest`) have been deprecated
    * `ChangesetCallback`, and all of its related classes, has been deprecated
* In parallel, _completely independent_ [`MinimalCommit`](https://developer.atlassian.com/static/javadoc/stash/3.11.0/api/reference/com/atlassian/stash/commit/MinimalCommit.html),
  [`Commit`](https://developer.atlassian.com/static/javadoc/stash/3.11.0/api/reference/com/atlassian/stash/commit/Commit.html)
  and [`Changeset`](https://developer.atlassian.com/static/javadoc/stash/3.11.0/api/reference/com/atlassian/stash/commit/Changeset.html)
  interfaces have been added
    * New data classes (`SimpleMinimalCommit`, `SimpleCommit` and `SimpleChangeset`) have been added
    * New request objects (`CommitRequest`, `CommitsRequest`, `CommitsBetweenRequest` and `ChangesetsRequest`) have
      been added
    * `CommitCallback`, and related classes, has been added
* All methods which return the legacy `Changeset` and `DetailedChangeset` types have been deprecated, with new
  variants that return the new `Commit` and `Changeset` types added alongside them

For those implementing the SCM SPI or _directly using_ the SCM API, Stash 3.7 introduces:

* [`Scm3`](https://developer.atlassian.com/static/javadoc/stash/3.11.0/spi/reference/com/atlassian/stash/scm/Scm3.html)
* [`PluginCommandFactory2`](https://developer.atlassian.com/static/javadoc/stash/3.11.0/spi/reference/com/atlassian/stash/scm/PluginCommandFactory2.html)
* [`PluginCompareCommandFactory2`](https://developer.atlassian.com/static/javadoc/stash/3.11.0/spi/reference/com/atlassian/stash/scm/compare/PluginCompareCommandFactory2.html)
* [`PluginPullRequestCommandFactory2`](https://developer.atlassian.com/static/javadoc/stash/3.11.0/spi/reference/com/atlassian/stash/scm/pull/PluginPullRequestCommandFactory2.html)

Unfortunately, because `PluginCommandFactory` (and the more user-facing `ScmCommandFactory`) use the names `commit` and
`commits` for their methods which return `Command<Changeset>` and `Command<Page<Changeset>>`, respectively, there is no
backward-compatible way to introduce methods which return `Command<Commit>` and `Command<Page<Commit>>`. _Because of
this, the SCM contract will be *broken* in the Stash 4.0 release._

* If you are _calling_ any of these methods, you should update your plugin to call one of Stash's API services
  instead. The API services exposing each SCM command are documented on the
  [`ScmCommandFactory`](https://developer.atlassian.com/static/javadoc/stash/3.11.0/api/reference/com/atlassian/stash/scm/ScmCommandFactory.html)
  interface
    * Switching to an API service will allow your plugin to be compatible with both Stash 3.x and Stash 4.x, which will
      not be possible if you use the SCM interfaces directly
    * If you cannot switch, you'll need to release distinct versions of your plugin for 3.x and 4.x compatibility
* If you have _implemented_ an SCM plugin, you will need to release distinct versions of it for 3.x and 4.x compatibility

Additional changes may be made in Stash 3.8, and subsequent releases, to further normalize the API. The goal of these
changes is for Stash 4.0 to have a _consistent_, clear API. If your plugins use the existing `Changeset` API we
_strongly_ encourage you to start updating them to use the new `Commit` API as soon as possible, and to keep an eye out
for EAP builds of Stash 4.0 so you can verify their compatibility with the next major release.

### Last authentication timestamps for users

In Stash 3.7, the last timestamp for users' most recent authentication is now tracked. This information cannot be
reconstructed retroactively, so after the upgrade each user will have an unknown timestamp. Their next authentication
will set that to the current time, and it will be tracked and updated going forward.

Authenticating via the web UI's login screen, via HTTP (pushing to or pulling from a repository over HTTP, for example)
or via SSH will all update the last authentication timestamp. Browsing via the web UI will _not_ update the timestamp
for each page viewed; the timestamp will only be updated when the user gets a new session with the server. If the user
has checked "Remember me", each time they get a new session with the server (generally after ~30 minutes of inactivity),
their timestamp will be updated when their new session is created.

The existing [`DetailedUser`](https://developer.atlassian.com/static/javadoc/stash/3.11.0/api/reference/com/atlassian/stash/user/DetailedUser.html)
has a new `getLastAuthenticationTimestamp()` accessor. That property is marshaled as "lastAuthenticationTimestamp" when
the `DetailedUser` is returned via [REST](https://developer.atlassian.com/static/rest/stash/3.11.0/stash-rest.html).
See the documentation for `/admin/users` for example JSON.

### Custom pull request notification SPI

In Stash 3.7, plugins can add custom sections in the pull request notifications. For an example, see the new
Comment Likes notifications. Adding custom notifications is performed as follows:

- The plugin fires an event extending `CustomPullRequestNotificationEvent` (using [SAL](https://developer.atlassian.com/x/QgBQ)'s
  `EventPublisher`); the event should include a _renderer ID_, the intended recipients of the notification, plus any
  data needed to render the notification encapsulated in a `CustomNotificationData` object (for example in Comment Like
  notifications, this includes the _comment ID_, plus the _user ID_ of the user who liked the comment);
- When the pull request notification is rendered, the notifications plugin in Stash calls the registered renderer with
  the _renderer ID_ included in the ```CustomPullRequestNotificationEvent```, transforming the `CustomNotificationData`
  into a `CustomNotificationSection` rendered in the notification's email.
- Users' notification preferences determine when notifications will be rendered. Notifications may be rendered several
  minutes later for users with batch notifications enabled.

Note that the data provided by the event (`CustomPullRequestNotificationEvent.getData()`) should be as lightweight as
possible because it is persisted as JSON data in the database for any recipient configured to receive batched
notifications. For example, use IDs (such as the _repository ID_ and _pull request ID_ for a pull request) rather than
the full object (such as a pull request instance).

Custom notification renderers are registered by implementing the interface
[`CustomPullRequestNotificationRenderer`](https://developer.atlassian.com/static/javadoc/stash/3.11.0/notification/reference/com/atlassian/stash/notification/custom/pull/CustomPullRequestNotificationRenderer.html)
in a plugin and declaring it in the plugin's `atlassian-plugin.xml` as follows:

```xml
<custom-pull-request-notification-renderer key="my-custom-notification-renderer" class="com.myorganisation.MyCustomNotificationRenderer"/>
```

### Custom pull request comment actions

In Stash 3.7, plugins can add custom actions to notification emails associated with pull request comments. For example,
the 'Like' and 'Reply' links in pull request comment emails are implemented as custom actions. To add your own custom
action to pull request notifications, declare a web-item in your plugin's `atlassian-plugin.xml` with
`section="stash.notification.comment.actions"`, and a `<link>` to the URL you want the user to navigate to when clicking
on the action. For example:

```xml
<web-item key="my-comment-action" name="My Comment Action" section="stash.notification.comment.actions">
    <label key="com.myorganisation.comment.action.label" />
    <tooltip key="com.myorganisation.comment.action.tooltip" />
    <link>https://stash.mycompany.com/plugins/servlet/custom-action-servlet/${pullRequest.toRef.project.key}/${pullRequest.toRef.repository.slug}/${pullRequest.id}?commentId=${comment.id}</link>
</web-item>
```

The custom action renders as a link below each comment in immediate and batched email notifications. Define the link
that your custom action navigates to, e.g. a servlet in your plugin that performs an action before navigating to the
pull request page, or a link that navigates to the comment on the pull request page and performs some action there.
The `${pullRequest}` and `${comment}` variables are provided in the notification context to help render the web-item's
link (e.g. `${pullRequest.id}` and `${comment.id}`).

## Stash 3.5

### Atlassian Soy 3.x

In Stash 3.4, the closure template (soy) compiler was upgraded to the latest version as part of the upgrade of
Atlassian Soy. This includes the removal of some legacy APIs as well as a stricter compiler of soy templates. The
compiler is stricter than the old compiler so some templates which were previously allow may start to have compilation
errors. It is possible to write templates which are compatible with both versions. Below are some of the common errors
which may occur as part of the upgrade and how to fix them.

#### Missing required param

Example:

```sh
template stash.feature.pullRequest.mergeHelpDialog: Call to 'aui.dialog.dialog2' is missing required param 'content'.
```

This is due to the strictness change in the soy compiler. the parameter on the sub template is defined as required but
you are not providing it to the call. You must either mark the parameter on the sub template as optional or pass in a
value for the parameter.

#### ClassCastException: java.lang.Long cannot be cast to java.lang.Integer

This is due to a change in the soy compiler where it represents numbers as longs where it previously represented them
as integers. This exception is most likely coming from a custom soy function you have written. Cast to a `Number`
instead and call `.intValue()` or `.longValue()`.

### Comment Likes API

The [comment likes](https://confluence.atlassian.com/display/STASH/Stash+3.5+release+notes#Stash3.5releasenotes-Commentlikes)
feature introduced in 3.5 comes with Java and REST APIs that may be used to add, remove and query comment likes
programmatically:

* in your Java plugin code, use [`CommentLikeService`](https://developer.atlassian.com/static/javadoc/stash/3.11.0/comment-likes/reference/com/atlassian/stash/comment/like/CommentLikeService.html)
* remote and browser clients can use the [Comment Likes REST API](https://developer.atlassian.com/static/rest/stash/3.11.0/stash-comment-likes-rest.html).

## Stash 3.4

### Properties API

Selected domain objects in Stash that inherit from [`PropertySupport`](https://developer.atlassian.com/static/javadoc/stash/3.4.0/api/reference/com/atlassian/stash/property/PropertySupport.html)
can now have custom information associated with them. The data is stored against an entity using
[`PropertyMap`](https://developer.atlassian.com/static/javadoc/stash/3.4.0/api/reference/com/atlassian/stash/property/PropertyMap.html),
which is essentially a map of string keys to any type of value. Properties supersede the
[`AttributeSupport`](https://developer.atlassian.com/static/javadoc/stash/3.4.0/api/reference/com/atlassian/stash/content/AttributeSupport.html)
API as a more flexible alternative, with the latter being deprecated and scheduled for removal in Stash 4.0.

Alongside `AttributeSupport` the following attribute provider plugin modules have been deprecated and replaced with a
property provider alternative. Plugin developers are encouraged to switch to property providers, as the replaced modules
will be removed in Stash 4.0:

* comments - use [`comment-property-provider`](https://developer.atlassian.com/static/javadoc/stash/3.4.0/spi/reference/com/atlassian/stash/comment/CommentPropertyProvider.html)
instead of the `comment-attribute-provider`
* pull requests - use [`pull-request-property-provider`](https://developer.atlassian.com/static/javadoc/stash/3.4.0/spi/reference/com/atlassian/stash/pull/PullRequestPropertyProvider.html)
instead of the `pull-request-attribute-provider`

Properties must be convertible to JSON, or otherwise REST requests to resources including properties will fail.

## Stash 3.3

### Short (display) SHA-1 changes

In Stash 3.3, short SHA-1s are no longer generated by Git. Instead, they are now generated using a fixed length of 11
characters. This change was made because generating short SHA-1s in Git requires it to load the SHA-1s of every object
in the repository for consideration. For larger repositories this produces a significant amount of overhead, slowing
down several pages in Stash such as listing a repository's commits and displaying individual commits. This overhead is
compounded when using a filesystem such as NFS for `STASH_HOME` or `STASH_SHARED_HOME`.

11 characters was chosen as the default length because it greatly increases the number of objects a repository needs to
have in order to have a high chance of conflict on a given short SHA-1:

* 7 characters requires only 20,000 objects in the repository to have a 50% chance of collision
* 11 characters requires nearly 5,000,000 objects to have a 50% chance of collision

All URLs in Stash use full 40 character SHA-1s so, even if there is a conflict on a given 11 character short SHA-1,
navigating Stash will _not_ be affected. Longer short SHA-1s are used to reduce the likelihood that copying a short
SHA-1 from the UI, since those are all the UI generally displays, and pasting it into a console window, or any other
external Git tool, will fail due to ambiguity.

As part of making this change, `ChangesetReader.FORMAT`, part of `stash-scm-git-common`, was changed to eliminate the
`%h` and `%p` elements. Any plugin code which just _uses_ the `ChangesetReader` should not require a change, as long as
they pass `ChangesetReader.getFormat()` to `--format` on their command, but any plugin code which directly references
`ChangesetReader.FORMAT` and then does its own parsing will no longer work.

## Stash 3.2

### Home directory re-organization

As part of Stash 3.2, the home directory was re-organized. The following changes have been made:

* The `data/` directory has been moved to `shared/data/`
* The `config/` directory has moved to `shared/config`
* The `plugins/installed-plugins/` directory has moved to `shared/plugins/installed-plugins/`
* The `stash-config.properties` file has moved to `shared/stash-config.properties`

The corresponding methods on [ApplicationPropertiesService](https://developer.atlassian.com/static/javadoc/stash/3.2.0/api/reference/com/atlassian/stash/server/ApplicationPropertiesService.html)
for these directories will return the new locations so plugins should be able to continue using these seamlessly.

### Atlassian Plugin SDK compatibility

As a result of the home directory re-organization, Stash 3.2 requires Atlassian Plugin SDK 5.0.3
and above. Stash will fail to start if you attempt to run your plugin via the SDK in a previous
version.

### Asynchronous startup

Previously, Stash would initialize synchronously with the web container and be unable to service requests. In
Stash 3.2 the startup was changed to be asynchronous, with Stash displaying progress of its initialization in the
web UI.

Currently this can cause issues when trying to run integration tests against a Stash application as the Atlassian
Plugin SDK will start running the tests before the application is fully initialized. In an upcoming release of
the Atlassian Plugin SDK this will be fixed but as a workaround you can add the following configuration to your pom.xml
to force Stash to initialize synchronously.

```xml
<configuration>
    <products>
        <product>
            <id>stash</id>
            <instanceId>stash</instanceId>
            <version>${stash.version}</version>
            <dataVersion>${stash.data.version}</dataVersion>
            <systemPropertyVariables>
                <!-- Force Stash to start up synchronously -->
                <johnson.spring.lifecycle.synchronousStartup>true</johnson.spring.lifecycle.synchronousStartup>
            </systemPropertyVariables>
        </product>
    </products>
</configuration>
```

### Atlassian SAL UserSettingsService

Stash now correctly implements the SAL [UserSettingsService](https://docs.atlassian.com/sal-api/2.12.1/sal-api/apidocs/com/atlassian/sal/api/usersettings/UserSettingsService.html).
Plugins can now use this service to retrieve and update settings associated with users. Prior to Stash 3.2 the service could be injected but it would not persist any settings.

## Stash 3.0

Stash 3.0 is the first major release of Stash in over a year.  All interfaces that were
marked deprecated in Stash 2.11 and earlier have been removed completely.  As a result,
existing Stash 2.x plugins that use any deprecated interfaces are **not** be automatically
compatible with Stash 3.0.

### Deprecated Java APIs removed

Interfaces, classes, and methods in the Java API that were marked deprecated in Stash
versions from 2.0 up to and including 2.11 have been removed.

Plugins that use any of these interfaces
(which would have generated deprecation warnings when built against Stash 2.12)
generally won't compile with Stash 3.0.

Precompiled plugins that used any of the deprecated interfaces will fail to install or
run in Stash 3.0, typically with `java.lang.NoSuchMethodError`.

Updating your plugin for Stash 3.0 shouldn't be too difficult, as alternatives to most
deprecated APIs have been documented in the
[Java API Reference](https://developer.atlassian.com/stash/docs/2.12.1/reference/java-api.html)
for Stash 2.12.

The following classes and interfaces have been moved to new packages or superseded by
newer alternatives.

 * [`AuthenticationFailedException`](https://developer.atlassian.com/static/javadoc/stash/2.12.1/api/reference/com/atlassian/stash/exception/AuthenticationFailedException.html)
 * [`AuthenticationFailureHandlerModuleDescriptor`](https://developer.atlassian.com/static/javadoc/stash/2.12.1/spi/reference/com/atlassian/stash/auth/AuthenticationFailureHandlerModuleDescriptor.html)
 * [`AuthenticationFailureHandler`](https://developer.atlassian.com/static/javadoc/stash/2.12.1/spi/reference/com/atlassian/stash/auth/AuthenticationFailureHandler.html)
 * [`AvatarRequest`](https://developer.atlassian.com/static/javadoc/stash/2.12.1/api/reference/com/atlassian/stash/user/AvatarRequest.html)
 * [`AvatarService`](https://developer.atlassian.com/static/javadoc/stash/2.12.1/api/reference/com/atlassian/stash/user/AvatarService.html)
 * [`ChangeCallback2`](https://developer.atlassian.com/static/javadoc/stash/2.12.1/api/reference/com/atlassian/stash/content/ChangeCallback2.html)
 * [`ChangesetCallback2`](https://developer.atlassian.com/static/javadoc/stash/2.12.1/api/reference/com/atlassian/stash/content/ChangesetCallback2.html)
 * [`CommentAddedEvent`](https://developer.atlassian.com/static/javadoc/stash/2.12.1/api/reference/com/atlassian/stash/event/comment/CommentAddedEvent.html)
 * [`CommentDeletedEvent`](https://developer.atlassian.com/static/javadoc/stash/2.12.1/api/reference/com/atlassian/stash/event/comment/CommentDeletedEvent.html)
 * [`CommentEditedEvent`](https://developer.atlassian.com/static/javadoc/stash/2.12.1/api/reference/com/atlassian/stash/event/comment/CommentEditedEvent.html)
 * [`CommentEvent`](https://developer.atlassian.com/static/javadoc/stash/2.12.1/api/reference/com/atlassian/stash/event/comment/CommentEvent.html)
 * [`CommentRepliedEvent`](https://developer.atlassian.com/static/javadoc/stash/2.12.1/api/reference/com/atlassian/stash/event/comment/CommentRepliedEvent.html)
 * [`CommonHistoryService`](https://developer.atlassian.com/static/javadoc/stash/2.12.1/api/reference/com/atlassian/stash/history/CommonHistoryService.html)
 * [`ContentTreeCallback2`](https://developer.atlassian.com/static/javadoc/stash/2.12.1/api/reference/com/atlassian/stash/content/ContentTreeCallback2.html)
 * [`ContentTreeCallback3`](https://developer.atlassian.com/static/javadoc/stash/2.12.1/api/reference/com/atlassian/stash/content/ContentTreeCallback3.html)
 * [`CurrentRequestResolver`](https://developer.atlassian.com/static/javadoc/stash/2.12.1/api/reference/com/atlassian/stash/web/CurrentRequestResolver.html)
 * [`DiffContentCallback2`](https://developer.atlassian.com/static/javadoc/stash/2.12.1/api/reference/com/atlassian/stash/content/DiffContentCallback2.html)
 * [`FileContentCallback2`](https://developer.atlassian.com/static/javadoc/stash/2.12.1/api/reference/com/atlassian/stash/content/FileContentCallback2.html)
 * [`FormFragment`](https://developer.atlassian.com/static/javadoc/stash/2.12.1/spi/reference/com/atlassian/stash/ui/FormFragment.html)
 * [`HistoryService`](https://developer.atlassian.com/static/javadoc/stash/2.12.1/api/reference/com/atlassian/stash/history/HistoryService.html)
 * [`ProjectDeleteRequestedEvent`](https://developer.atlassian.com/static/javadoc/stash/2.12.1/api/reference/com/atlassian/stash/event/ProjectDeleteRequestedEvent.html)
 * [`ProjectSearchCriteria`](https://developer.atlassian.com/static/javadoc/stash/2.12.1/api/reference/com/atlassian/stash/project/ProjectSearchCriteria.html)
 * [`RefCallback2`](https://developer.atlassian.com/static/javadoc/stash/2.12.1/api/reference/com/atlassian/stash/repository/RefCallback2.html)
 * [`RepositoryDeleteRequestedEvent`](https://developer.atlassian.com/static/javadoc/stash/2.12.1/api/reference/com/atlassian/stash/event/RepositoryDeleteRequestedEvent.html)

A few classes and interfaces have also been removed altogether, with no equivalent
alternative.

 * [`CaptchaResponse`](https://developer.atlassian.com/static/javadoc/stash/2.12.1/api/reference/com/atlassian/stash/user/CaptchaResponse.html)
 * [`CommentEditedPullRequestNotification`](https://developer.atlassian.com/static/javadoc/stash/2.12.1/spi/reference/com/atlassian/stash/notification/pull/CommentEditedPullRequestNotification.html)
 * [`DeprecatedPermissionAdminService`](https://developer.atlassian.com/static/javadoc/stash/2.12.1/api/reference/com/atlassian/stash/user/DeprecatedPermissionAdminService.html)
 * [`DeprecatedUserService`](https://developer.atlassian.com/static/javadoc/stash/2.12.1/api/reference/com/atlassian/stash/user/DeprecatedUserService.html)
 * [`GrantedPermission`](https://developer.atlassian.com/static/javadoc/stash/2.12.1/api/reference/com/atlassian/stash/user/GrantedPermission.html)

In addition, deprecated methods in existing classes and interfaces across the whole
Stash API (too many to list here) have been removed.

Consult the [Java API Reference](https://developer.atlassian.com/stash/docs/2.12.1/reference/java-api.html)
for Stash 2.12 for details on the removed methods, and their alternatives in Stash 3.0.

### Deprecated Web UI components removed

Stash 3.0 has also removed Web UI Components (JavaScript, Soy, and LESS) that were marked
as deprecated in Stash versions 2.0 to 2.11 inclusive.

Plugins should not be depending on any of these deprecated components, as they do not
include any Atlassian standard [AUI](https://docs.atlassian.com/aui/latest/) components
or published Stash extensions.

Nevertheless, if you want to verify that your plugin is not affected by the removal of
deprecated Web UI APIs, try installing it in the latest 2.x release of Stash.  All
the removed interfaces were present in this release and annotated with `@deprecated`,
so any use should result in deprecation warnings in the Developer Tools of your browser.

### Internationalization (I18n)

Stash 3.0 now has internationalization (i18n) support, and includes three language packs: French, German and Japanese!
Internationalizing your plugin for these languages is highly recommended to provide a
consistent look and feel for customers in their own native language.
The i18n API's for plugins have been available for some time,
see [Internationalizing your plugin](https://developer.atlassian.com/display/DOCS/Internationalising+your+plugin) for details.

### Java 1.6

Stash 3.0 requires Java 1.7 (Oracle or OpenJDK) or higher, for both plugin development
and runtime environment.  Java 1.6 is no longer supported.

### Scala 2.10

Stash 3.0 requires Scala 2.10 for plugin development. Scala 2.9 and earlier versions
are no longer supported.

### Help Soy functions deprecated

The Soy functions `cav_help_url` and `cav_help_title` for referencing help documentation are deprecated for removal
in 4.0. Use `stash_help_url` and `stash_help_title` instead.

### PullRequestLock and RepositoryLock moved to stash-api

The [PullRequestLock](https://developer.atlassian.com/static/javadoc/stash/3.11.0/api/reference/com/atlassian/stash/concurrent/PullRequestLock.html)
and [RepositoryLock](https://developer.atlassian.com/static/javadoc/stash/3.11.0/api/reference/com/atlassian/stash/concurrent/RepositoryLock.html)
which were previous in the stash-util module have been moved to the stash-api to facilitate the implementation of a
[LockService](https://developer.atlassian.com/static/javadoc/stash/3.11.0/api/reference/com/atlassian/stash/concurrent/LockService.html).
The interfaces were moved verbatim and thus maintain their binary compatibility.

## Stash 2.12

`Changeset.getAuthor()` and `Person.getName()` have always been expected to return a non-null value.
Explicit `@Nonnull` annotations have now been added to those methods to explicitly document that expectation.

Scala 2.9 is deprecated for plugin development and will be removed in Stash 3.0. Scala 2.10 has been available
to plugins since Stash 2.11.

## Stash 2.11

### Commit Comments

The `HistoryService` has been deprecated and renamed to `CommitService`. New methods have been added in `CommitService`
which support commenting on commits. New REST resources have been added, similar to those used for pull requests, for
adding, deleting and updating comments and for watching and unwatching commits.

* To allow the new REST URLs to match the URL structure for pull requests, *ref names are no longer supported by the
  commits REST resource* (`rest/api/latest/projects/KEY/repos/SLUG/commits/ID`). Instead, you must provide a SHA1 to
  identify the commit. For example, `rest/api/latest/projects/KEY/repos/SLUG/commits/refs/heads/master` will no longer
  work. Instead, use the SHA1 for the latest commit on `refs/heads/master`, like
  `rest/api/latest/projects/KEY/repos/SLUG/commits/5bad17727d52c17fa7f3a20ed433ebd2c1cdfa21`.
* `rest/api/latest/projects/KEY/repos/SLUG/changes` is now _deprecated_ and will be removed in 3.0. Instead, use
  `rest/api/latest/projects/KEY/repos/SLUG/commits/ID/changes`, where `ID` is the value previously sent as `?until`
* `rest/api/latest/projects/KEY/repos/SLUG/diff` is now _deprecated_ and will be removed in 3.0. Instead, use
  `rest/api/latest/projects/KEY/repos/SLUG/commits/ID/diff`, where `ID` is the value previously sent as `?until`

The pullRequest context param provided to the `stash.comments.actions` and `stash.comments.info` web item locations for
comments has been deprecated to be optional for 3.0. In 3.0, web-items for these locations should not always expect the
pullRequest object as comments can also be made on commits.

### Branching Model API

The Branch Model feature introduced in Stash 2.8 now offers Java and REST API, allowing plugin developers to:

* Query whether a repository has a Branching Model associated with it
* Get the Development and the Production branch
* Get enabled branch types (Bugfix, Feature, Hotfix, Release)
* Get a list of branches belonging to a given branch type
* Classify a branch (get its category, if any)

#### Java API

The following interfaces are the entry point to the API and may be consumed from the `branch-utils` artifact:

* [`BranchModelService`](https://developer.atlassian.com/static/javadoc/stash/3.11.0/branch-utils/reference/com/atlassian/stash/branch/model/BranchModelService.html),
* [`BranchModel`](https://developer.atlassian.com/static/javadoc/stash/3.11.0/branch-utils/reference/com/atlassian/stash/branch/model/BranchModel.html)

#### REST API

As of Stash 2.11, the branch model for a given repository may be also queried via the
[REST API](https://developer.atlassian.com/static/rest/stash/3.11.0/stash-branch-utils-rest.html).

## Stash 2.10

### Log level when developing a plugin

When starting Stash with [AMPS](https://developer.atlassian.com/display/DOCS/Using+the+AMPS+Maven+Plugin+Directly),
the default log level of the Stash instance was changed from _DEBUG_ to _WARN_.

### Empty repository web panels

The `stash.web.repository.empty` location for web-panels has been deprecated and replaced with
client-web-panels at the location `stash.empty.repository.instructions`. These client-web-panels will now receive
the user preferred clone url (HTTP or SSH) as a context param and re-rendered when the user changes their clone URL preference.

### Updating the base URL

Updating the base URL with `ApplicationPropertiesService.setBaseUrl(URI)` now requires `SYS_ADMIN` permission.

### Streaming callbacks

The following callbacks have been added, extending existing callbacks:

- [`ChangeCallback2`](https://developer.atlassian.com/static/javadoc/stash/2.12.1/api/reference/com/atlassian/stash/content/ChangeCallback2.html),
- [`ChangesetCallback2`](https://developer.atlassian.com/static/javadoc/stash/2.12.1/api/reference/com/atlassian/stash/content/ChangesetCallback2.html),
- [`ContentTreeCallback3`](https://developer.atlassian.com/static/javadoc/stash/2.12.1/api/reference/com/atlassian/stash/content/ContentTreeCallback3.html),
- [`DiffContentCallback2`](https://developer.atlassian.com/static/javadoc/stash/2.12.1/api/reference/com/atlassian/stash/content/DiffContentCallback2.html),
- [`FileContentCallback2`](https://developer.atlassian.com/static/javadoc/stash/2.12.1/api/reference/com/atlassian/stash/content/FileContentCallback2.html),
- [`RefCallback2`](https://developer.atlassian.com/static/javadoc/stash/2.12.1/api/reference/com/atlassian/stash/repository/RefCallback2.html),

These new interfaces make the callbacks more consistent with each other. They also introduce new `onStart` and `onEnd`
methods which accept readily-extensible context and summary objects, respectively. The context provided to `onStart`
offers insight into the shape of the command for which results are being streamed, with a summary of those results
provided to `onEnd`. Some of these objects are simple placeholders, in this release, and will be fleshed out in future
releases. Those changes, when they happen, will be backwards-compatible and will not require introducing new interfaces.

The existing interfaces are unchanged, so these new interfaces should not break any existing plugins. They are included
in 2.10 to give plugin developers more time to update to the new interfaces. For each callback, an abstract base class
is provided. Plugin developers should _always_ extend from one of these base classes when implementing any callback. The
base classes will make the callback implementations more resilient to future changes to the callbacks.

- [`AbstractChangeCallback`](https://developer.atlassian.com/static/javadoc/stash/3.11.0/api/reference/com/atlassian/stash/content/AbstractChangeCallback.html),
- [`AbstractChangesetCallback`](https://developer.atlassian.com/static/javadoc/stash/3.11.0/api/reference/com/atlassian/stash/content/AbstractChangesetCallback.html),
- [`AbstractContentTreeCallback`](https://developer.atlassian.com/static/javadoc/stash/3.11.0/api/reference/com/atlassian/stash/content/AbstractContentTreeCallback.html),
- [`AbstractDiffContentCallback`](https://developer.atlassian.com/static/javadoc/stash/3.11.0/api/reference/com/atlassian/stash/content/AbstractDiffContentCallback.html),
- [`AbstractFileContentCallback`](https://developer.atlassian.com/static/javadoc/stash/3.11.0/api/reference/com/atlassian/stash/content/AbstractFileContentCallback.html),
- [`AbstractRefCallback`](https://developer.atlassian.com/static/javadoc/stash/3.11.0/api/reference/com/atlassian/stash/repository/AbstractRefCallback.html),

All of the new interfaces are _deprecated_, and will be folded back into their respective unversioned interfaces and
removed in Stash 3.0. Plugin developers who extend their implementations from one of the base classes should not be
affected by that change. Each new interface describes how its unversioned interface will change in the 3.0 release.

#### `FileContentCallback.appendBlame(List)` and `FileContentCallback.onEndPage(Page)`

In previous releases, `FileContentCallback.appendBlame(List)` was called _after_ `FileContentCallback.onEndPage(Page)`.
Stash 2.10 introduces a _non-backwards-compatible change_ where `appendBlame` is now called _before_ `onEndPage`. The
`onEnd` method on each callback is intended to always be the _final_ method invoked, so this ordering change makes the
`FileContentCallback` consistent with all of the other callbacks, where `onEnd` is already always called last.

### New columns in the audit log file

The audit log format has been extended with two new columns: the ID of the request in scope and the
ID of the session of the request in scope.

## Stash 2.9

### Renaming users

Stash 2.9 allows for users to be renamed in both internal and remote directories. Plugins which are
using the username as a key to store data against will break when a user is renamed.

In order to cope with user renames, Stash-specific plugins should use [StashUser.getId()](https://developer.atlassian.com/static/javadoc/stash/latest/api/reference/com/atlassian/stash/user/StashUser.html),
whereas Cross-product plugins should use SAL's [UserKey](https://docs.atlassian.com/sal-api/2.10.0/sal-api/xref/com/atlassian/sal/api/user/UserKey.html)
to store data against a user. In order to detect when a rename occurs, plugins should listen for the
[UserRenamedEvent](https://docs.atlassian.com/crowd/current/com/atlassian/crowd/event/user/UserRenamedEvent.html).

### Stash now built with Maven 3.0.x

As of 2.5.5, 2.6.6, 2.7.7, 2.8.5 and 2.9.0, Stash uses maven 3.0 to build. This should not affect the
development of plugins using the Atlassian SDK nor how Stash is [built from source](../../how-tos/building-bitbucket-server-from-source-code).

## Stash 2.8

### Pull Request Merge SPI

Stash 2.8 introduces a new SPI to allow plugin developers to inspect pull request merges before they are applied to
the repository. In the `stash-scm-git-api` module, there is a new `GitPullRequestMergeRequestEvent` which they can
listen for.

This event includes the pull request being merged, as well as the SHA1 of the merge commit. It is raised before the
pull request's target ref is updated, and can be canceled to prevent the update. When canceling a merge, the plugin
must provide a message explaining why. This message will be shown to the user, so it should be clear and descriptive
to help the user resolve whatever error is preventing the merge.

For more information, see [STASH-3122](https://jira.atlassian.com/browse/STASH-3122).

### Ref Metadata Provider SPI

Stash 2.8 introduces an SPI for providing metadata associated with refs. This metadata can be accessed via REST
when retrieving a list of branches.

See the [reference documentation](../plugin-module-types/ref-metadata-provider) for more information.

### Changeset Plugin Formatting

Previously the web fragment `stash.changeset.extras` would expect `<dl>` elements for display, with labels.
For consistency with the pull request page, and making things pretty, Stash has introduced a new fragment
`stash.changeset.related-entities` which is shown in the same location, but prefers the use of an icon over a label.
The old `stash.changeset.extras` is now considered deprecated and will be removed in 3.0.

### New placeholder text param for Branch Selector Field Soy template

The `Stash.template.branchSelector.input` template now takes in a `text` param which can be used to override the
default placeholder text for the branch selector dropdown when no ref is selected.

### Moving repositories

Stash 2.8 now supports moving a repository between different projects. When moved, repositories retain the same
unique ID, even though the `Project` is changed. Plugins which use project key and repository slug together as
a key to store data against instead of the repository's ID, will break when a repository is moved. Plugins should
instead use the repository's ID.

## Stash 2.7

### Backup/restore (beta)

Stash 2.7 supports a BETA version of backup and restore. In order for your plugin's data to be included within
the backup archive you will need to ensure it is stored in one of two locations:

* In database tables managed by ActiveObjects
* In the `<STASH_HOME>` directory with the exception of the a few excludes the most notable of which are the
  `<STASH_HOME>/tmp` and `<STASH_HOME>/export` directories.

### Inactive users

Stash 2.7 now respects the active state of users which are synchronized from an external user directory.
Inactive users are will no longer be counted towards the license count and will not be returned from
any of Stash's services unless explicitly requested. This is consistent with how Stash handles deleted users.
More detailed documentation on how inactive users are handled can be found in the documentation for the
[UserService](https://developer.atlassian.com/static/javadoc/stash/latest/api/reference/com/atlassian/stash/user/UserService.html).

## Stash 2.6

### Page limit

Stash is very strict about performance requirements, and as such uses
[Page](https://developer.atlassian.com/static/javadoc/stash/latest/api/reference/com/atlassian/stash/util/Page.html)
everywhere to help encourage this behavior.

Previously it was possible to see an extremely large
[PageRequest](https://developer.atlassian.com/static/javadoc/stash/latest/api/reference/com/atlassian/stash/util/PageRequest.html)
limit size. In many cases this is reduced to a hard-limit, but in others it was respected and may have resulted in poor
performance.

Now setting the limit larger than `PageRequest.MAX_PAGE_LIMIT` will result in an exception, which may break some plugins.
Instead of using a large limit, it is strongly suggested to page with a reasonable limit and process in batches.

## Stash 2.5

### Anonymous access

Stash 2.5 introduces the concept of anonymous access to projects and repositories. There are a few implications for
plugins that need to be taken into consideration.

#### Null user

The
[`StashAuthenticationContext.getCurrentUser()`](https://developer.atlassian.com/static/javadoc/stash/3.11.0/api/reference/com/atlassian/stash/user/StashAuthenticationContext.html#getCurrentUser%28%29)
method has always been annotated with `@Nullable`, but until now it was fairly unlikely for a `null` to be returned.
With anonymous access previous invocations of plugin that code that assumed a non-`null` result may now throw an exception.
This also include calls to
[`StashEvent.getUser()`](https://developer.atlassian.com/static/javadoc/stash/3.11.0/api/reference/com/atlassian/stash/event/StashEvent.html#getUser%28%29)
and JavaScript calls to `pageState.getCurrentUser()`.

Callers of any of any of these methods should _always_ check the result to avoid errors caused by anonymous users.

#### Permissions

To confirm if an anonymous user has access to a resource an extra check must be made in addition to verifying standard permissions.
It's worth noting that calls to the standard
[`ProjectService`](https://developer.atlassian.com/static/javadoc/stash/3.11.0/api/reference/com/atlassian/stash/project/ProjectService.html)
and
[`RepositoryService`](https://developer.atlassian.com/static/javadoc/stash/3.11.0/api/reference/com/atlassian/stash/repository/RepositoryService.html)
already have the appropriate permissions, and manual validation is not required in the majority of cases.

Manual anonymous permission checking is possible through new methods added to the
[`PermissionService`](https://developer.atlassian.com/static/javadoc/stash/3.11.0/api/reference/com/atlassian/stash/user/PermissionService.html)
such as
[`isPublicAccessEnabled()`](https://developer.atlassian.com/static/javadoc/stash/3.11.0/api/reference/com/atlassian/stash/user/PermissionService.html#isPublicAccessEnabled%28%29),
[`isProjectAccessible()`](https://developer.atlassian.com/static/javadoc/stash/3.11.0/api/reference/com/atlassian/stash/user/PermissionService.html#isProjectAccessible%28com.atlassian.stash.project.Project%29)
and
[`isRepositoryAccessible()`](https://developer.atlassian.com/static/javadoc/stash/3.11.0/api/reference/com/atlassian/stash/user/PermissionService.html#isRepositoryAccessible%28com.atlassian.stash.repository.Repository%29).
Plugins that only check permissions such as
[`REPO_READ`](https://developer.atlassian.com/static/javadoc/stash/3.11.0/api/reference/com/atlassian/stash/user/Permission.html)
will continue to return `false` for anonymous users.

## Stash 2.4

### Support for forks

#### Prefer changesets to branches/tag

With the introduction of forks it is now crucial to recognize what
[repository](https://developer.atlassian.com/static/javadoc/stash/3.11.0/api/reference/com/atlassian/stash/repository/Repository.html)
is being handled when making SCM or history calls and the implications of which branches/tags are visible.

To explain further, consider creating a pull request from a fork to the parent repository. When trying to calculate the
changes between a branch on the fork to master on the parent, which branches are available will depend entirely
on which repository is being referenced. In short the branch on the fork doesn't actually exist on the parent and _must_
be referenced by changeset instead.

#### Hooks

There are some implications to hook developers who plan on enforcing a specific workflow for forks as well as branches.
Please see the [hook documentation](../../how-tos/hooks-merge-checks-guide) for more details.

### Repository permissions

Stash 2.4 has introduced Repository Permissions which, among other things, alter the concept of what it means to access
a [Project](https://developer.atlassian.com/static/javadoc/stash/3.11.0/api/reference/com/atlassian/stash/project/Project.html)
and [Repository](https://developer.atlassian.com/static/javadoc/stash/3.11.0/api/reference/com/atlassian/stash/repository/Repository.html).

One important change is the introduction of a _virtual_ permission
[`PROJECT_VIEW`](https://developer.atlassian.com/static/javadoc/stash/3.11.0/api/reference/com/atlassian/stash/user/Permission.html).
The `PROJECT_VIEW` permission is transient and is granted when a user is able to view a project,
when they have either a repository permission on a single repository, or a direct permission on the
project itself. An example of the `PROJECT_VIEW` permission can be seen on the screen listing the projects.
By comparison, the `PROJECT_READ` permission is a super-set and allows a user to access the entire project and
all the child repositories.

This change has implications to plugin developers who are either granting permissions, or using something like the
[`SecurityService`](https://developer.atlassian.com/static/javadoc/stash/3.11.0/api/reference/com/atlassian/stash/user/SecurityService.html)
to elevate permissions for a particular operation. Previously there was no technical difference between granting
`PROJECT_READ` and `REPOSITORY_READ`, but after 2.4 some operations may not function correctly.

### User profiles and accounts

Stash 2.4 introduces a public user profile page as well as a private account management page. The following changes have been introduced to the way that plugins can interact with these pages:

#### User slugs

There is no restriction on characters that may be present in a username, since the Stash userbase may be sourced from an external directory, such as LDAP. As such, the `username` property of a User is not guaranteed to be URL safe (consider a forward or backward slash in the username).

Inserting the username of a User into a path component of a URL is highly ill-advised, and the `StashUser.slug` property should be used instead. For example, `/plugins/servlet/my-plugin/{$user.slug}`. Relatedly, use `UserService.getUserBySlug(String slug)` to retrieve a user from Stash.

#### User profile

Prior to Stash 2.4, the profile was only visible to the current user; a user's profile is now visible to any user.

Plugins injecting resources into the `stash.page.userProfile` context are now only rendered when a user is viewing his or her _own profile_. This context is deprecated, and `stash.page.user.profile.self` should be used in preference. If your plugin was using this context to render user preference fields, you should use the Account Management pages instead.

The architecture of the profile page has also changed in Stash 2.4; previously, a plugin would inject a navigation tab and render content using a web panel. Due to the restructuring of these pages, this is no longer possible and you will need to now use a decorated page to inject a new tab. Please follow our how-to on [decorating the profile page](../../tutorials-and-examples/decorating-the-user-profile).

Other notes:

* The context `atl.userprofile` is available on the _account pages_ only (see below), not the public user profile. This is in contrast to Stash 2.3 and below which made no distinction as there was no account management pages. Use `stash.page.user.profile` to target these pages; see [web resource contexts](../web-resource-contexts) for other options.
* `stash.web.userprofile.secondary.tabs` web item location has been deprecated. Use `stash.user.profile.secondary.tabs` instead, which provides the `profileUser` and `project` (User's project object) in it's context.

#### Account management

The account management pages are used to view and modify user account information and user preferences. These are new in Stash 2.4.

Please follow our how-to on [decorating the user account page](../../tutorials-and-examples/decorating-the-user-account) for details on how to inject data here with your plugin.

#### Project list

The 'stash.project.help.basics' and 'stash.project.help.reference' web item plugin points on the Project list page sidebar are deprecated. You should use 'stash.project.list.sidebar.items' instead.

#### Repository header

The `stash.web.repository.clone.before` and `stash.web.repository.clone.after` web panel locations have been deprecated. Use `stash.web.repository.header.buttons` for web items and web sections or `stash.web.repository.header` for web panels.

## Stash 2.3

### Detecting when the repository has changed

The new [`RepositoryRefsChangedEvent`](https://developer.atlassian.com/static/javadoc/stash/3.11.0/api/reference/com/atlassian/stash/event/RepositoryRefsChangedEvent.html) gives developers an easy way to receive notification
of changes to a repository.  The two existing ref change events, [`RepositoryPushEvent`](https://developer.atlassian.com/static/javadoc/stash/3.11.0/api/reference/com/atlassian/stash/event/RepositoryPushEvent.html)
and [`PullRequestMergedEvent`](https://developer.atlassian.com/static/javadoc/stash/3.11.0/api/reference/com/atlassian/stash/event/pull/PullRequestMergedEvent.html),
now implement `RepositoryRefsChangedEvent`.  As a result, developers needing to know about changes
can use a single event listener:

```java
@EventListener
public void onRefsChangedEvent(final RepositoryRefsChangedEvent event) {
    // Take action when refs have changed
}
```

### New cancelable events

Two new [`CancelableEvent`](https://developer.atlassian.com/static/javadoc/stash/3.11.0/api/reference/com/atlassian/stash/event/CancelableEvent.html)s
allow plugins to veto modifications to [project settings](https://developer.atlassian.com/static/javadoc/stash/3.11.0/api/reference/com/atlassian/stash/event/ProjectModificationRequestedEvent.html) and
[repository settings](https://developer.atlassian.com/static/javadoc/stash/3.11.0/api/reference/com/atlassian/stash/event/RepositoryModificationRequestedEvent.html).

### Stash authentication plugin modules <span class="aui-lozenge aui-lozenge-complete" style="margin-left:5px;">Beta</span>

We have added three experimental plugin modules designed to support custom authentication in Stash, such as Kerberos or
custom SSO integration. See the documentation for the [Http Authentication Handler](../plugin-module-types/http-authentication-handler),
[Http Authentication Success Handler](../plugin-module-types/http-authentication-success-handler) and
[Http Authentication Failure Handler](../plugin-module-types/http-authentication-failure-handler) module types.

### Hook settings permissions

Previously hook settings were available (also via REST) for users granted the `REPO_READ` permission. This has been increased
to `REPO_ADMIN` and the documentation updated. The implications for this are plugin developers that require these
settings on non-admin pages will need to introduce their own REST endpoint and manually elevate permission as required.

### ContentTreeCallback changes

Introduced a new [`ContentTreeCallback2`](https://developer.atlassian.com/static/javadoc/stash/2.12.1/api/reference/com/atlassian/stash/content/ContentTreeCallback2.html),
which extends the original [`ContentTreeCallback`](https://developer.atlassian.com/static/javadoc/stash/3.11.0/api/reference/com/atlassian/stash/content/ContentTreeCallback.html),
and introduces a new [`onTreeNode()`](https://developer.atlassian.com/static/javadoc/stash/2.12.1/api/reference/com/atlassian/stash/content/ContentTreeCallback2.html))
method for handling extra parameters on the [`ContentTreeNode`](https://developer.atlassian.com/static/javadoc/stash/3.11.0/api/reference/com/atlassian/stash/content/ContentTreeNode.html),
such as URLs for the newly introduced [`Submodule`](https://developer.atlassian.com/static/javadoc/stash/3.11.0/api/reference/com/atlassian/stash/content/Submodule.html).

Plugin developers are strongly advised to extend [`AbstractContentTreeCallback`](https://developer.atlassian.com/static/javadoc/stash/3.11.0/api/reference/com/atlassian/stash/content/AbstractContentTreeCallback.html)
which provides better compatibility for future changes to either of these two interfaces.

## Stash 2.0

### Cancelable events

The [`CancelableEvent`](https://developer.atlassian.com/static/javadoc/stash/3.11.0/api/reference/com/atlassian/stash/event/CancelableEvent.html)
interface gives developers a chance to cancel certain operations.  Stash currently allows cancellation of
[project creation](https://developer.atlassian.com/static/javadoc/stash/3.11.0/api/reference/com/atlassian/stash/event/ProjectCreationRequestedEvent.html),
[project deletion](https://developer.atlassian.com/static/javadoc/stash/3.11.0/api/reference/com/atlassian/stash/event/ProjectDeletionRequestedEvent.html),
[repository creation](https://developer.atlassian.com/static/javadoc/stash/3.11.0/api/reference/com/atlassian/stash/event/RepositoryCreationRequestedEvent.html) and
[repository deletion](https://developer.atlassian.com/static/javadoc/stash/3.11.0/api/reference/com/atlassian/stash/event/RepositoryDeletionRequestedEvent.html).
If you are listening to a cancelable event, you can cancel the operation by calling the the `cancel()` method on the event, passing
a [`KeyedMessage`](https://developer.atlassian.com/static/javadoc/stash/3.11.0/api/reference/com/atlassian/stash/i18n/KeyedMessage.html)
that explains the reason for cancellation.


## Stash 1.3

As part of the 1.3 release Stash has made some small breaking API changes in
respect to version 1.2.

### API

  1. The content Callback interfaces `com.atlassian.stash.content.*Callback` have been modified slightly to include extra arguments. For example the `onHunkStart` method now requires 4 arguments instead of the original 2. These changes will require the extra parameters to be added to any implementations, and the plugins recompiled.
  2. `com.atlassian.stash.content.PagedCallback` has been removed and the `onStartPage` and `onEndPage` methods have been lifted into the various implementations. This affects `ContentTreeCallback` and `FileContentCallback`. These changes are a binary-incompatible update, and do not require any source-level changes, but do need to be recompiled.

### Scm-Git

  1. `com.atlassian.stash.scm.git.remote.GitRemote*Builder.build()` now returns `GitCommand` instead of the super-class `Command`. This is a binary-incompatible change, and does not require any source-level changes, but does need to be recompiled.
  2. `com.atlassian.stash.scm.git.GitScmCommandBuilder.config()` now returns `GitConfig` instead of `GitConfigBuilder`.
  3. `com.atlassian.stash.scm.git.config.GitConfigBuilder` has been split into `GitConfigGetBuilder`, `GitConfigSetBuilder` and `GitConfigUnsetBuilder`.


