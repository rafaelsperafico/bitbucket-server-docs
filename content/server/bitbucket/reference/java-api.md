---
title:  Java API
platform: server
product: bitbucketserver
category: reference
subcategory: java
date: "2018-08-29"
---
#  Java API

The Bitbucket Server Java API is split up into multiple modules, each of which export various classes and services that can be
consumed by plugins. Each module is published as a separate maven artifact each Bitbucket Server release, with a version number
matching the Bitbucket Server release number.

## Javadoc

#### [Bitbucket Server API](https://developer.atlassian.com/static/javadoc/bitbucket-server/latest/api/)

Provides a collection of services,
  events and utility classes for interacting with core Bitbucket features such as server administration, projects,
  repositories, pull requests and user management.

#### [Bitbucket Server SPI](https://developer.atlassian.com/static/javadoc/bitbucket-server/latest/spi/)

Provides a number of plugin module interfaces, module descriptor classes and utilities that plugins can use to extend
Bitbucket's stock functionality. See [Plugin Module Types](../plugin-module-types/plugin-modules) for more details of
specific modules that can be implemented.

#### [Branches](https://developer.atlassian.com/static/javadoc/bitbucket-server/latest/branch-api/reference/com/atlassian/bitbucket/branch/model/package-summary.html)

Services exported by this plugin that provide extended branch information and enable advanced branch management, such as
programmatically creating branches and setting up
[branching models](https://confluence.atlassian.com/display/BitbucketServer/Using+branches+in+Bitbucket+Server#UsingbranchesinBitbucketServer-Configuringthebranchingmodel).

#### [Builds](https://developer.atlassian.com/static/javadoc/bitbucket-server/latest/build-api/reference/com/atlassian/bitbucket/build/package-summary.html)

Services exported by this plugin to query and set the build status on individual commits.

#### [Code Insights](https://docs.atlassian.com/bitbucket-server/javadoc/latest/code-insights-api/reference/com/atlassian/bitbucket/codeinsights/package-summary.html)

Provides an API to create, modify and delete code insight reports and annotations.
See [this how-to guide](../../how-tos/code-insights) for more details or 
[this tutorial](../../tutorials-and-examples/code-insights-with-PMD) to see a REST-based insight provider.

#### [Comment Likes](https://developer.atlassian.com/static/javadoc/bitbucket-server/latest/comment-likes-api/reference/com/atlassian/bitbucket/comment/like/package-summary.html)

Provides an API to add and remove comment likes, as well as query for users who have liked comments.

#### [Git](https://developer.atlassian.com/static/javadoc/bitbucket-server/latest/git-api/)

Provides a collection of richly typed command builders for easily invoking native git commands on repositories hosted in Bitbucket.

#### [Jira](https://developer.atlassian.com/static/javadoc/bitbucket-server/latest/jira-api/reference/com/atlassian/bitbucket/integration/jira/package-summary.html)

Services exported by this plugin to query the set of Jira issues linked to individual commits.

#### [Notifications](https://developer.atlassian.com/static/javadoc/bitbucket-server/latest/notification-spi/reference/com/atlassian/bitbucket/notification/custom/pull/package-summary.html)

SPI to allow 3rd party plugins to include custom notifications about changes to the pull requests.

#### [Ref Restrictions](https://developer.atlassian.com/static/javadoc/bitbucket-server/latest/ref-restriction-api/reference/com/atlassian/bitbucket/repository/ref/restriction/package-summary.html)

Services exported by this plugin to query and set the branch permissions on a repository.

#### [SCM Common](https://developer.atlassian.com/static/javadoc/bitbucket-server/latest/scm-common/)

Provides common utilities for dealing with SCMs.

#### [SSH](https://developer.atlassian.com/static/javadoc/bitbucket-server/latest/ssh-api/)

Services exported by this plugin to query and set the SSH server configuration and users SSH keys.

#### [Web Common](https://developer.atlassian.com/static/javadoc/bitbucket-server/latest/web-common/reference/com/atlassian/bitbucket/web/conditions/package-summary.html)

Provides common [Web Fragment Conditions](../web-fragments#Conditions) you can use to control when your Web Fragments appear.

## Importing in Maven

Add the following dependencies to your `pom.xml`

### Bitbucket Server API

```xml
<dependency>
    <groupId>com.atlassian.bitbucket.server</groupId>
    <artifactId>bitbucket-api</artifactId>
    <scope>provided</scope>
    <version>${bitbucket.version}</version>
</dependency>
```

### Bitbucket Server SPI

```xml
<dependency>
    <groupId>com.atlassian.bitbucket.server</groupId>
    <artifactId>bitbucket-spi</artifactId>
    <scope>provided</scope>
    <version>${bitbucket.version}</version>
</dependency>
```

### Git API

```xml
<dependency>
    <groupId>com.atlassian.bitbucket.server</groupId>
    <artifactId>bitbucket-git-api</artifactId>
    <scope>provided</scope>
    <version>${bitbucket.version}</version>
</dependency>
```

### Build API

```xml
<dependency>
    <groupId>com.atlassian.bitbucket.server</groupId>
    <artifactId>bitbucket-build-api</artifactId>
    <scope>provided</scope>
    <version>${bitbucket.version}</version>
</dependency>
```

### Branch Permissions API

```xml
<dependency>
    <groupId>com.atlassian.bitbucket.server</groupId>
    <artifactId>bitbucket-ref-restriction-api</artifactId>
    <scope>provided</scope>
    <version>${bitbucket.version}</version>
</dependency>
```

### Branch Permissions SPI

```xml
<dependency>
    <groupId>com.atlassian.bitbucket.server</groupId>
    <artifactId>bitbucket-ref-restriction-spi</artifactId>
    <scope>provided</scope>
    <version>${bitbucket.version}</version>
</dependency>
```

### Branch API

```xml
<dependency>
    <groupId>com.atlassian.bitbucket.server</groupId>
    <artifactId>bitbucket-branch-api</artifactId>
    <scope>provided</scope>
    <version>${bitbucket.version}</version>
</dependency>
```


### Comment Likes

```xml
<dependency>
    <groupId>com.atlassian.bitbucket.server</groupId>
    <artifactId>bitbucket-comment-likes-api</artifactId>
    <scope>provided</scope>
    <version>${bitbucket.version}</version>
</dependency>
```

### Jira Integration

```xml
<dependency>
    <groupId>com.atlassian.bitbucket.server</groupId>
    <artifactId>bitbucket-jira-api</artifactId>
    <scope>provided</scope>
    <version>${bitbucket.version}</version>
</dependency>
```

### SSH Support

```xml
<dependency>
    <groupId>com.atlassian.bitbucket.server</groupId>
    <artifactId>bitbucket-ssh-api</artifactId>
    <scope>provided</scope>
    <version>${bitbucket.version}</version>
</dependency>
```

### SCM Common

```xml
<dependency>
    <groupId>com.atlassian.bitbucket.server</groupId>
    <artifactId>bitbucket-scm-common</artifactId>
    <scope>provided</scope>
    <version>${bitbucket.version}</version>
</dependency>
```

### Web Fragment Conditions

```xml
<dependency>
    <groupId>com.atlassian.bitbucket.server</groupId>
    <artifactId>bitbucket-web-common</artifactId>
    <scope>provided</scope>
    <version>${bitbucket.version}</version>
</dependency>
```
