---
title: LESS API
platform: server
product: bitbucketserver
category: reference
subcategory: other
date: "2017-12-14"
---
# LESS API - FUTURE

**Bitbucket Server currently does not have a LESS API. This page is unlinked from the documentation. Its contents don't apply to any current version of Bitbucket Server.**

The LESS API in Bitbucket Server is a set of variables and mixins that you may `@import` into your own LESS files when trying
to match the look and feel of Bitbucket Server.

## Compatibility Policy

The LESS API's compatibility policy matches that of Bitbucket Server's other APIs. LESS APIs will not contain breaking changes between
minor versions (e.g. 3.0 and 3.1). Instead, any removed or modified templates and parameters will documented as deprecated
in the [API changelog](../api-changelog), and the legacy behavior will cease to work in the next major release (e.g. Bitbucket Server 4.0).

"Breaking changes" describes removal of LESS files, variables or mixins, or changes to accepted mixin parameter values
that would cause previously valid input to generate invalid CSS, or throw an error. Behavior for _invalid_ inputs may change
at any release. See [API Scope](#api-scope) for details.

## Loading the APIs

LESS API files are loaded directly from the JAR resources in Bitbucket Server. You load them by adding an `@import` to your LESS file
with a URL schema of `webstatic`, and a path. E.g.

```css
@import webstatic://api.less

.my-class {
    color: @bitbucket-text-color;
}
```

## API Scope

The entirety of the LESS API is available by importing `api.less`. New files may be added in future releases.

Supported variables and mixins are prefixed with `@bitbucket-`. Other variables and mixins should be considered unstable and
**SHOULD NOT** be used.

Variables will have the following guarantees between releases:

* The type of a variable will not change between minor releases. Numeric properties will remain numeric, color properties
will remain colors, urls will remain urls.
* The unit of a variable will not change between minor releases. If it is `px`, it will continue to be `px`. This is
true for `em`, `vh`, etc. This means you can safely use variables in your calculations, and rest assured that the units will resolve consistently.

### Explicitly out of scope

Overwriting LESS variable values or mixin definitions is **NOT** supported and will likely break Bitbucket Server's UI.

Mixins **MAY** produce differing CSS properties and values between minor releases. However the overall effect will be the same.
This does mean that mixins may begin to conflict with other CSS properties you have set between minor releases.

Variable values **MAY** change between minor releases. You should use variables consistently in linked properties to avoid
mismatches when variable values change.

The version of LESS used within Bitbucket Server **MAY** change. We will make a best effort attempt not to introduce breaking changes
through LESS upgrades.
