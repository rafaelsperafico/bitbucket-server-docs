---
title: Plugin decorators
platform: server
product: bitbucketserver
category: reference
subcategory: other
date: "2017-12-14"
---
# Plugin decorators

By decorating a page, Bitbucket Server plugins are able to inject data into preexisting page layouts.

## References

* Example reference plugin: [bitbucket-example-plugin](http://bitbucket.org/atlassian/bitbucket-example-plugin)
* Tutorial: [decorating the user profile](../../tutorials-and-examples/decorating-the-user-profile)
* Tutorial: [decorating the user account](../../tutorials-and-examples/decorating-the-user-account)

## Decorators

Meta tags _must_ be provided when listed, for example:

```xml
<head>
    <meta name="decorator" content="bitbucket.project.general" />
    <meta name="projectKey" content="{$project.key}" />
    <meta name="activeTab" content="project-plugin-tab" />
    <title>{$project.key} / Example Tab</title>
</head>
```

The following decorators are available:

<table>
    <tr>
        <th>Decorator</th>
        <th>Description</th>
        <th>Since</th>
        <th>Required Metadata</th>
    </tr>
    <tr>
        <td>atl.general</td>
        <td>Generic page layout</td>
        <td>4.0</td>
        <td><i>None</i></td>
    </tr>
    <tr>
        <td>atl.popup</td>
        <td>Generic page layout</td>
        <td>4.0</td>
        <td><i>None</i></td>
    </tr>
    <tr>
        <td>atl.userprofile</td>
        <td>User account decorator, same as <i>bitbucket.users.account</i></td>
        <td>1.0</td>
        <td>
            <pre><code class="xml">&lt;meta name="userSlug" content="{$user.slug}"&gt;
&lt;meta name="tab" content="your-web-item-key"&gt;</code></pre>
        </td>
    </tr>
    <tr>
        <td>atl.admin</td>
        <td>Admin layout</td>
        <td>4.0</td>
        <td><i>None</i></td>
    </tr>
    <tr>
        <td>bitbucket.form</td>
        <td><i>Deprecated</i>. Use a specific <code>bitbucket.focused.*</code> decorator instead.</td>
        <td>4.0</td>
        <td><i>None</i></td>
    </tr>
    <tr>
        <td>bitbucket.focused.small</td>
        <td>Small form layout, for example the Bitbucket Server login page.</td>
        <td>4.0</td>
        <td><i>None</i></td>
    </tr>
    <tr>
        <td>bitbucket.focused.medium</td>
        <td>The most widely used layout in Bitbucket Server, such as the create repository page.</td>
        <td>4.0</td>
        <td><i>None</i></td>
    </tr>
    <tr>
        <td>bitbucket.focused.large</td>
        <td>A wide page layout.</td>
        <td>4.0</td>
        <td><i>None</i></td>
    </tr>
    <tr>
        <td>bitbucket.focused.xlarge</td>
        <td>Close to full width page layout, for example the create pull request page.</td>
        <td>4.0</td>
        <td><i>None</i></td>
    </tr>
    <tr>
        <td>bitbucket.project.general</td>
        <td>Project page layout</td>
        <td>4.0</td>
        <td>
            <pre><code class="xml">&lt;meta name="projectKey" content="{$project.key}"&gt;
&lt;meta name="activeTab" content="your-web-item-key"&gt;</code></pre>
        </td>
    </tr>
    <tr>
        <td>bitbucket.project.settings</td>
        <td>Project settings page layout</td>
        <td>4.0</td>
        <td>
            <pre><code class="xml">&lt;meta name="projectKey" content="{$project.key}"&gt;
&lt;meta name="activeTab" content="your-web-item-key"&gt;</code></pre>
        </td>
    </tr>
    <tr>
        <td>bitbucket.repository.general</td>
        <td>Repository page layout</td>
        <td>4.0</td>
        <td>
            <pre><code class="xml">&lt;meta name="projectKey" content="{$repository.project.key}"&gt;
&lt;meta name="repositorySlug" content="{$repository.slug}"&gt;
&lt;meta name="activeTab" content="your-web-item-key"&gt;</code></pre>
        </td>
    </tr>
    <tr>
        <td>bitbucket.repository.settings</td>
        <td>Repository settings layout</td>
        <td>4.0</td>
        <td>
            <pre><code class="xml">&lt;meta name="projectKey" content="{$repository.project.key}"&gt;
&lt;meta name="repositorySlug" content="{$repository.slug}"&gt;
&lt;meta name="activeTab" content="your-web-item-key"&gt;</code></pre>
        </td>
    </tr>
    <tr>
        <td>bitbucket.pullrequest.view</td>
        <td>Pull request layout</td>
        <td>4.0</td>
        <td>
            <pre><code class="xml">&lt;meta name="projectKey" content="{$repository.project.key}"&gt;
&lt;meta name="repositorySlug" content="{$repository.slug}"&gt;
&lt;meta name="pullRequestId" content="{$pullRequest.id}"&gt;
&lt;meta name="activeTab" content="your-web-item-key"&gt;</code></pre>
        </td>
    </tr>
    <tr>
        <td>bitbucket.users.account</td>
        <td>User account layout, same as <i>atl.userprofile</i></td>
        <td>4.0</td>
        <td>
            <pre><code class="xml">&lt;meta name="userSlug" content="{$user.slug}"&gt;
&lt;meta name="activeTab" content="your-web-item-key"&gt;</code></pre>
        </td>
    </tr>
    <tr>
        <td>bitbucket.users.profile</td>
        <td>User profile layout</td>
        <td>4.0</td>
        <td>
            <pre><code class="xml">&lt;meta name="userSlug" content="{$user.slug}"&gt;
&lt;meta name="activeTab" content="your-web-item-key"&gt;</code></pre>
        </td>
    </tr>
</table>
