---
title: Commit Property Config Plugin Module
platform: server
product: bitbucketserver
category: reference
subcategory: modules
date: "2017-12-14"
---
# Commit Property Config Plugin Module

{{% warning %}}

This plugin point is only useful if you already have a [Commit Indexer](../commit-indexer) defined.
You should read the [Commit Indexer documentation](../commit-indexer) first!

{{% /warning %}}

## Introduction

The Commit Property Config module descriptor can be used to automatically decorate `Commit` objects returned by
Bitbucket Server's Java and REST APIs with properties indexed by your plugin using [Commit Indexers](../commit-indexer).

It is important to note that **you do not have to define a Commit Property Config module for your properties to be
accessible**. The `CommitService` and `CommitIndex` have other methods for looking them up. You *only* need to define a
Commit Property Config module if you want your indexed properties to be automatically included by all `Commit` objects
returned from the REST and Java APIs.

## REST API

This mechanism provides a simple way to expose your plugin's indexed data over REST without defining separate resources.

For example, the regular response from:

```sh
GET /rest/api/1.0/projects/PRJ/repos/my-repo/commits/cafebabecafebabecafebabecafebabecafebabe
```

looks like:

```json
{
  "id": "cafebabecafebabecafebabecafebabecafebabe",
  "displayId": "cafebabecaf",
  "author": {...},
  "authorTimestamp": 1234567890123,
  "message": "STASH-1234: s/magic/more magic",
  "parents": [...]
}
```

But if you define a Commit Property Config module, responses from this resource will be decorated with your indexed
properties:

```json
{
  "id": "cafebabecafebabecafebabecafebabecafebabe",
  "displayId": "cafebabecaf",
  "author": {...},
  "authorTimestamp": 1234567890123,
  "message": "STASH-1234: s/magic/more magic",
  "parents": [...],
  "properties": {
    "byAtlassian": true
  }
}
```

## Java API

Similarly, [`Commit`](https://developer.atlassian.com/static/javadoc/bitbucket-server/latest/api/reference/com/atlassian/bitbucket/commit/Commit.html)
objects returned from the [`CommitService`](https://developer.atlassian.com/static/javadoc/bitbucket-server/latest/api/reference/com/atlassian/bitbucket/commit/CommitService.html)
will be automatically populated with any indexed properties that you have defined as preloaded in a Commit Property
Config plugin module. These properties are accessible via the [`PropertySupport`](https://developer.atlassian.com/static/javadoc/bitbucket-server/latest/api/reference/com/atlassian/bitbucket/property/PropertySupport.html)
methods on `Commit`.

Note, however, that the request objects for [`CommitService`](https://developer.atlassian.com/static/javadoc/bitbucket-server/latest/api/reference/com/atlassian/bitbucket/commit/CommitService.html)
methods returning commits include `Set<String> getPropertyKeys()` allowing the caller to request specific properties,
so a Commit Property Config plugin module isn't a requirement to retrieve your indexed properties. A Commit Property
Config plugin module should only be used if you want _all_ [`Commit`](https://developer.atlassian.com/static/javadoc/bitbucket-server/latest/api/reference/com/atlassian/bitbucket/commit/Commit.html)
objects to automatically have your properties loaded.

* [`CommitRequest`](https://developer.atlassian.com/static/javadoc/bitbucket-server/latest/api/reference/com/atlassian/bitbucket/commit/CommitRequest.html)
* [`CommitsBetweenRequest`](https://developer.atlassian.com/static/javadoc/bitbucket-server/latest/api/reference/com/atlassian/bitbucket/commit/CommitsBetweenRequest.html)
* [`CommitsRequest`](https://developer.atlassian.com/static/javadoc/bitbucket-server/latest/api/reference/com/atlassian/bitbucket/commit/CommitsRequest.html)

## Configuration

The root element for the Commit Property Config plugin module is `<commit-property-config/>`. It allows the
following two attributes for configuration:

### Attributes

<table>
    <tr>
        <th>Name</th>
        <th>Required</th>
        <th>Description</th>
        <th>Default</th>
    </tr>
    <tr>
        <td>key</td>
        <td>Yes</td>
        <td>The identifier of the plugin module. This key must be unique within the plugin where it is defined.</td>
        <td>N/A</td>
    </tr>
    <tr>
        <td>class</td>
        <td>Yes</td>
        <td>
        The fully qualified Java class name of your commit property configuration class. This class must
        implement <a href="https://developer.atlassian.com/static/javadoc/bitbucket-server/latest/spi/reference/com/atlassian/bitbucket/idx/CommitPropertyConfiguration.html"><tt>CommitPropertyConfiguration</tt></a>.
        </td>
        <td>N/A</td>
    </tr>
</table>

### Example

Here is an example `atlassian-plugin.xml` file containing a single commit indexer and accompanying property config:

```xml
<atlassian-plugin name="My Commit Indexer" key="example.plugin.indexer" plugins-version="2">
    <plugin-info>
        <description>A basic component import module test</description>
        <vendor name="My Company" url="http://www.mycompany.com"/>
        <version>1.0</version>
    </plugin-info>

    <commit-indexer key="myCommitIndexer" class="com.mycompany.example.plugin.MyCommitIndexer"/>
    <commit-property-config key="myCommitPropertyConfig" class="com.mycompany.example.plugin.MyCommitPropertyConfiguration"/>
</atlassian-plugin>
```

And here is an example [CommitPropertyConfiguration](https://developer.atlassian.com/static/javadoc/bitbucket-server/latest/spi/reference/com/atlassian/bitbucket/idx/CommitPropertyConfiguration.html)
implementation that preloads the `byAtlassian` property used in the [Commit Indexer plugin module](../commit-indexer)
example.

```java
public class MyCommitPropertyConfiguration implements CommitPropertyConfiguration {

    private static final Set<String> PRELOADED_PROPERTIES = Collections.singleton("byAtlassian");

    @Override
    public Collection<String> getPropertiesToPreload(Repository repository) {
        return PRELOADED_PROPERTIES;
    }
}
```
