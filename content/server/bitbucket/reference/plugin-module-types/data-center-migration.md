---
title: Data Center Migration
platform: server
product: bitbucketserver
category: reference
subcategory: modules
date: "2018-08-21"
---
# Data Center Migration Plugin Module

## Introduction

Data Center Migration provides two plugin points that can be used by plugin developer to integrate into the migration process

* [`Exporter`](https://docs.atlassian.com/bitbucket-server/javadoc/latest/spi/reference/com/atlassian/bitbucket/migration/Exporter.html) - used to add plugin data to a migration archive
* [`Importer`](https://docs.atlassian.com/bitbucket-server/javadoc/latest/spi/reference/com/atlassian/bitbucket/migration/Exporter.html) - used to read plugin data from a migration archive

## Exporter

Registered export modules are called when the export process encounters one of three top level "scopes"

1. [`Project`](https://docs.atlassian.com/bitbucket-server/javadoc/latest/spi/reference/com/atlassian/bitbucket/migration/Exporter.html#export(com.atlassian.bitbucket.migration.ExportContext,%20com.atlassian.bitbucket.project.Project))
2. [`PullRequest`](https://docs.atlassian.com/bitbucket-server/javadoc/latest/spi/reference/com/atlassian/bitbucket/migration/Exporter.html#export(com.atlassian.bitbucket.migration.ExportContext,%20com.atlassian.bitbucket.pull.PullRequest))
3. [`Repository`](https://docs.atlassian.com/bitbucket-server/javadoc/latest/spi/reference/com/atlassian/bitbucket/migration/Exporter.html#export(com.atlassian.bitbucket.migration.ExportContext,%20com.atlassian.bitbucket.repository.Repository))

### Export Context

In addition to the entity being exported an [`ExportContext`](https://docs.atlassian.com/bitbucket-server/javadoc/latest/spi/reference/com/atlassian/bitbucket/migration/ExportContext.html) is also supplied to 
the relevant method. The export context can be used to [add data to the export archive](https://docs.atlassian.com/bitbucket-server/javadoc/latest/spi/reference/com/atlassian/bitbucket/migration/ExportContext.html#addSectionIfAbsent(java.nio.file.Path,%20java.util.function.Consumer%3Ccom.atlassian.bitbucket.migration.ExportSection%3E)), add [errors](https://docs.atlassian.com/bitbucket-server/javadoc/latest/spi/reference/com/atlassian/bitbucket/migration/ExportContext.html#addError(com.atlassian.bitbucket.i18n.KeyedMessage,%20java.lang.Object)), [warnings](https://docs.atlassian.com/bitbucket-server/javadoc/latest/spi/reference/com/atlassian/bitbucket/migration/ExportContext.html#addWarning(com.atlassian.bitbucket.i18n.KeyedMessage,%20java.lang.Object)) and [entity mappings]((https://docs.atlassian.com/bitbucket-server/javadoc/latest/spi/reference/com/atlassian/bitbucket/migration/MigrationEntityType.html)) to the export job.

## Importer

When the import process encounters an entry that is the responsibility of a [`Importer`] (https://docs.atlassian.com/bitbucket-server/javadoc/latest/spi/reference/com/atlassian/bitbucket/migration/Importer.html) either the 
[`onArchiveEntry`](https://docs.atlassian.com/bitbucket-server/javadoc/latest/spi/reference/com/atlassian/bitbucket/migration/Importer.html#onArchiveEntry(com.atlassian.bitbucket.migration.ImportContext,%20com.atlassian.bitbucket.migration.ArchiveSource)) or [`onEntry`](https://docs.atlassian.com/bitbucket-server/javadoc/latest/spi/reference/com/atlassian/bitbucket/migration/Importer.html#onEntry(com.atlassian.bitbucket.migration.ImportContext,%20com.atlassian.bitbucket.migration.EntrySource)) will be called depending on the type of entry encountered. This will allow the importer to access the content of the entry and perform the import.

### Import Context

An [`ImportContext`](https://docs.atlassian.com/bitbucket-server/javadoc/latest/spi/reference/com/atlassian/bitbucket/migration/Importer.html#onStart(com.atlassian.bitbucket.migration.ImportContext)) is used to add  [errors](https://docs.atlassian.com/bitbucket-server/javadoc/latest/spi/reference/com/atlassian/bitbucket/migration/ImportContext.html#addError(com.atlassian.bitbucket.i18n.KeyedMessage,%20java.lang.Object)), [warnings](https://docs.atlassian.com/bitbucket-server/javadoc/latest/spi/reference/com/atlassian/bitbucket/migration/ImportContext.html#addWarning(com.atlassian.bitbucket.i18n.KeyedMessage,%20java.lang.Object)) and provide [entity mappings]((https://docs.atlassian.com/bitbucket-server/javadoc/latest/spi/reference/com/atlassian/bitbucket/migration/MigrationEntityType.html)) to the import job.

### Entity Mappings

Bitbucket provides no guarantee the ID's of entities being imported and exported will be stable across instances. This is a problem because entities need to refer to each other e.g. a `PullRequest` references a `Repository`.
The solution: [`ExportContext`](https://docs.atlassian.com/bitbucket-server/javadoc/latest/spi/reference/com/atlassian/bitbucket/migration/ImportContext.html) provides the concept of a [`MigrationEntityType`](https://docs.atlassian.com/bitbucket-server/javadoc/latest/spi/reference/com/atlassian/bitbucket/migration/MigrationEntityType.html) that is supplied to [`getEntityMapping`](https://docs.atlassian.com/bitbucket-server/javadoc/5.13.0-rc4/spi/reference/com/atlassian/bitbucket/migration/ImportContext.html#getEntityMapping(com.atlassian.bitbucket.migration.MigrationEntityType%3CT%3E)) which provides a mapping between the original id of an entity on the _source_ instance and the id of the entity on the _target_ instance. 

## Configuration

The root element for the Data Center Migration plugin module is `<migration-handler/>`. It allows the following
configuration attributes:

#### Attributes

<table>
    <tr>
        <th>Name</th>
        <th>Required</th>
        <th>Description</th>
    </tr>
    <tr>
        <td>key</td>
        <td>Yes</td>
        <td>The identifier of the plugin module. This key must be unique within the plugin where it is defined.</td>
    </tr>
    <tr>
        <td>exporter</td>
        <td>Yes</td>
        <td>
            An exporter definition containing a fully qualified class name
            This class must implement <a href="https://docs.atlassian.com/bitbucket-server/javadoc/latest/spi/reference/com/atlassian/bitbucket/migration/Exporter.html">Exporter</a>
        </td>
    </tr>
    <tr>
        <td>importer</td>
        <td>Yes</td>
        <td>
            An importer definition containing a fully qualified class name.
            This class must implement <a href="https://docs.atlassian.com/bitbucket-server/javadoc/latest/spi/reference/com/atlassian/bitbucket/migration/Importer.html">Importer</a>
        </td>
    </tr>
    <tr>
        <td>weight</td>
        <td></td>
        <td>The (integer) weight of the plugin module. Migration handlers with a higher weight will be processed later. Third party plugins must use a weight greater than 100</td>
    </tr>
</table>

#### Example

Here is an example for a custom Data Center Migration handler

```xml

<atlassian-plugin key="${project.groupId}.${project.artifactId}" name="${project.name}" plugins-version="2">
    <plugin-info>
        <description>${project.description}</description>
        <version>${project.version}</version>
        <vendor name="${project.organization.name}" url="${project.organization.url}" />
    </plugin-info>

    <migration-handler key="ExampleMigrationHandler" weight="101">
        <exporter class="this.is.an.ExampleExporter"/>
        <importer class="this.is.an.ExampleImporter"/>
    </migration-handler>

</atlassian-plugin>
```

#### Weights

Third party plugins <b>must</b> use a plugin weight greater than 100
