---
title: HTTP Authentication Failure Handler Plugin Module
platform: server
product: bitbucketserver
category: reference
subcategory: modules
date: "2017-12-14"
---
# HTTP Authentication Failure Handler Plugin Module

## Introduction

Bitbucket Server allows plugins to participate in the authentication chain through three plugin module types.

* [`http-authentication-handler`](../http-authentication-handler) - used to authenticate users and validate whether
  the current authentication session is still valid.
* [`http-authentication-success-handler`](../http-authentication-success-handler) - called when a user is authenticated
  successfully using any of the installed `http-authentication-handler` modules.
* `http-authentication-failure-handler` - called when authentication using any of the installed
  `http-authentication-handler` modules failed.


## Purpose of this Module Type

A HTTP Authentication Failure Handler plugin module can customize the behavior on authentication failure. All
available failure handlers are called in order of their configured `weight` (from low to high). See the
[HttpAuthenticationFailureHandler](https://developer.atlassian.com/static/javadoc/bitbucket-server/latest/spi/reference/com/atlassian/bitbucket/auth/HttpAuthenticationFailureHandler.html)
 interface for a complete description of how to implement a `HttpAuthenticationFailureHandler`.

The built-in handlers do the following:

* For REST requests, return a HTTP 401 authentication challenge with the authentication error message as a JSON payload.
* For SCM (git) requests, write out the authentication error message to the remote client.
* For BASIC authentication, send a HTTP 401 authentication challenge.
* For all other cases redirect to the login page.

In a typical SSO scenario, the HTTP Authentication Failure Handler module can be used to redirect the user to an external
login page.

## Configuration

The root element for the HTTP Authentication Failure Handler plugin module is `<http-auth-failure-handler/>`. It allows
the following configuration attributes:

### Attributes

<table>
    <tr>
        <th>Name</th>
        <th>Required</th>
        <th>Description</th>
        <th>Default</th>
    </tr>
    <tr>
        <td>key</td>
        <td>Yes</td>
        <td>The identifier of the plugin module. This key must be unique within the plugin where it is defined.</td>
        <td>N/A</td>
    </tr>
    <tr>
        <td>class</td>
        <td>Yes</td>
        <td>
            The fully qualified Java class name of the HTTP Authentication Failure Handler.
            This class must implement <a href="https://developer.atlassian.com/static/javadoc/bitbucket-server/latest/spi/reference/com/atlassian/bitbucket/auth/HttpAuthenticationFailureHandler.html">
            <tt>HttpAuthenticationFailureHandler</tt></a>.
        </td>
        <td>N/A</td>
    </tr>
    <tr>
        <td>weight</td>
        <td></td>
        <td>The (integer) weight of the plugin module. Authentication failure handlers with a higher weight will be processed later.</td>
        <td>50</td>
    </tr>
</table>

### Built-in authentication failure handlers

Bitbucket Server bundles a number of authentication failure handlers. When choosing the `weight` of your authentication failure 
handler, consider whether your `http-authentication-failure-handler` should be applied before or after the built-in 
handlers:

<table>
    <tr>
        <th>Name</th>
        <th>Weight</th>
        <th>Description</th>
    </tr>
    <tr>
        <td>REST failure handler</td>
        <td>60</td>
        <td>Writes authentication error messages in JSON format and sends a HTTP 401 WWW-Authenticate response</td> 
    </tr>
    <tr>
        <td>SCM authentication failure handler</td>
        <td>80</td>
        <td>Writes authentication error messages to the SCM client using the SCM HTTP protocol for hosting commands</td> 
    </tr>
    <tr>
        <td>Basic auth challenge failure handler</td>
        <td>90</td>
        <td>If the request was authenticated using BASIC auth, this handler sends a HTTP 401 WWW-Authenticate response</td>
    </tr>
    <tr>
        <td>Redirecting failure handler</td>
        <td>100</td>
        <td>Redirects the user to the login page</td>
    </tr>
</table>

### Example

```xml
<atlassian-plugin key="com.your.domain.custom.auth" name="Bitbucket Server Authentication plugin">

    <plugin-info>
        <description>Configuration example</description>
        <version>1.0</version>
        <vendor name="Atlassian" url="http://www.atlassian.com"/>
    </plugin-info>

    <http-auth-failure-handler key="customFailureHandler"
                               class="com.your.domain.custom.auth.CustomAuthenticationFailureHandler"
                               weight="90"/>

</atlassian-plugin>
```
