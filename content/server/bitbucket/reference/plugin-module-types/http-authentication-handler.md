---
title: HTTP Authentication Handler Plugin Module
platform: server
product: bitbucketserver
category: reference
subcategory: modules
date: "2017-12-14"
---
# HTTP Authentication Handler Plugin Module

## Introduction

Bitbucket Server allows plugins to participate in the authentication chain through three plugin module types.

* `http-authentication-handler` - used to authenticate users and validate whether the current authentication session is
  still valid.
* [`http-authentication-success-handler`](../http-authentication-success-handler) - called when a user is authenticated
  successfully using any of the installed `http-authentication-handler` modules.
* [`http-authentication-failure-handler`](../http-authentication-failure-handler) - called when authentication using any
  of the installed `http-authentication-handler` modules failed.


## Purpose of this Module Type

A HTTP Authentication Handler plugin module provides a mechanism of authenticating users. The module has two
responsibilities: authenticating users based on a HTTP request and validating that the current session is still valid.
As an example, an SSO authentication module could authenticate a user based on a custom cookie. After the initial
authentication succeeds, the SSO module should validate that the cookie is still provided on subsequent requests and may
need to check with a remote server whether the SSO session is still valid.

All available authentication handlers are called in order of their configured `weight` (from low to high). See the
<a href="https://developer.atlassian.com/static/javadoc/bitbucket-server/latest/spi/reference/com/atlassian/bitbucket/auth/HttpAuthenticationHandler.html">
<tt>HttpAuthenticationHandler</tt></a> interface for a complete description of how to implement a `HttpAuthenticationHandler`.

HTTP Authentication Handlers can optionally implement
<a href="https://developer.atlassian.com/static/javadoc/bitbucket-server/latest/spi/reference/com/atlassian/bitbucket/auth/HttpLogoutHandler.html">
<tt>HttpLogoutHandler</tt></a> to receive a callback when a user logs out. `HttpLogoutHandler`s may manipulate
the HTTP response on logout (e.g. redirect to an external login screen).

## Configuration

The root element for the HTTP Authentication Handler plugin module is `<http-auth-handler/>`. It allows the following
configuration attributes:

### Attributes

<table>
    <tr>
        <th>Name</th>
        <th>Required</th>
        <th>Description</th>
        <th>Default</th>
    </tr>
    <tr>
        <td>key</td>
        <td>Yes</td>
        <td>The identifier of the plugin module. This key must be unique within the plugin where it is defined.</td>
        <td>N/A</td>
    </tr>
    <tr>
        <td>class</td>
        <td>Yes</td>
        <td>
            The fully qualified Java class name of the HTTP Authentication Handler.
            This class must implement <a href="https://developer.atlassian.com/static/javadoc/bitbucket-server/latest/spi/reference/com/atlassian/bitbucket/auth/HttpAuthenticationHandler.html">
            <tt>HttpAuthenticationHandler</tt></a>.
            The class may also implement <a href="https://developer.atlassian.com/static/javadoc/bitbucket-server/latest/spi/reference/com/atlassian/bitbucket/auth/HttpLogoutHandler.html">
            <tt>HttpLogoutHandler</tt></a> to receive a callback on logout.
        </td>
        <td>N/A</td>
    </tr>
    <tr>
        <td>captcha-support</td>
        <td></td>
        <td>Whether authentication failures should count against CAPTCHA limits.</td>
        <td><tt>true</tt></td>
    </tr>
    <tr>
        <td>weight</td>
        <td></td>
        <td>The (integer) weight of the plugin module. Authentication handlers with a higher weight will be processed later.</td>
        <td>50</td>
    </tr>
</table>

### Built-in authentication handlers

Bitbucket Server bundles a number of authentication handlers. When choosing the `weight` of your authentication handler, consider 
whether your `http-authentication-handler` should be applied before or after the built-in authentication handlers:

<table>
    <tr>
        <th>Name</th>
        <th>Weight</th>
        <th>Description</th>
    </tr>
    <tr>
        <td>Crowd SSO authentication handler</td>
        <td>20</td>
        <td>Disabled by default, can be enabled in bitbucket.properties</td>
    </tr>
    <tr>
        <td>Embedded Crowd authentication handler</td>
        <td>100</td>
        <td>Authenticates based on username/password using the configured user directories. Opts out of authentication 
            when no username is provided</td>
    </tr>
    <tr>
        <td>Remember-me authentication handler</td>
        <td>110</td>
        <td>Authenticates using the remember-me cookie, if found. Opts out of authentication if no cookie is detected</td>
    </tr>
</table>

### Example

Here is the atlassian-plugin.xml from an example 
[container based authentication plugin](https://bitbucket.org/atlassian/bitbucket-auth-plugin-example), which defines a 
custom `http-authentication-handler`: 

```xml

<atlassian-plugin key="${project.groupId}.${project.artifactId}" name="${project.name}" plugins-version="2">
    <plugin-info>
        <description>${project.description}</description>
        <version>${project.version}</version>
        <vendor name="${project.organization.name}" url="${project.organization.url}" />
    </plugin-info>

    <component-import key="i18nService" interface="com.atlassian.bitbucket.i18n.I18nService"/>
    <component-import key="userService" interface="com.atlassian.bitbucket.user.UserService"/>

    <http-auth-handler key="containerAuthenticationHandler"
                       class="com.atlassian.bitbucket.auth.container.RemoteUserAuthenticationHandler"
                       captcha-support="false"
                       weight="100"/>

</atlassian-plugin>
```
