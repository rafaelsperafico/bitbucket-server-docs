---
title: HTTP Authentication Success Handler Plugin Module
platform: server
product: bitbucketserver
category: reference
subcategory: modules
date: "2017-12-14"
---
# HTTP Authentication Success Handler Plugin Module

## Introduction

Bitbucket Server allows plugins to participate in the authentication chain through three plugin module types.

* [`http-authentication-handler`](../http-authentication-handler) - used to authenticate users and validate whether
  the current authentication session is still valid.
* `http-authentication-success-handler` - called when a user is authenticated
  successfully using any of the installed `http-authentication-handler` modules.
* [`http-authentication-failure-handler`](../http-authentication-failure-handler) - called when authentication using
  any of the installed `http-authentication-handler` modules failed.

## Purpose of this Module Type

HTTP Authentication Success Handler plugin modules receive a callback on authentication success and can customize the
HTTP response, usually by redirecting to a custom page. The built-in authentication success handler redirects the user
back to the URL they requested prior to being sent to the login screen, or to the projects list if they requested
the login screen explicitly.

All available authentication success handlers are called in order of their configured `weight` (from low to high). See the
<a href="https://developer.atlassian.com/static/javadoc/bitbucket-server/latest/spi/reference/com/atlassian/bitbucket/auth/HttpAuthenticationSuccessHandler.html">
<tt>HttpAuthenticationSuccessHandler</tt></a> interface for a complete description of how to implement a `HttpAuthenticationSuccessHandler`.


## Configuration

The root element for the HTTP Authentication Success Handler plugin module is `<http-auth-success-handler/>`. It allows
the following configuration attributes:

### Attributes

<table>
    <tr>
        <th>Name</th>
        <th>Required</th>
        <th>Description</th>
        <th>Default</th>
    </tr>
    <tr>
        <td>key</td>
        <td>Yes</td>
        <td>The identifier of the plugin module. This key must be unique within the plugin where it is defined.</td>
        <td>N/A</td>
    </tr>
    <tr>
        <td>class</td>
        <td>Yes</td>
        <td>
            The fully qualified Java class name of the HTTP Authentication Success Handler.
            This class must implement <a href="https://developer.atlassian.com/static/javadoc/bitbucket-server/latest/spi/reference/com/atlassian/bitbucket/auth/HttpAuthenticationSuccessHandler.html">
            <tt>HttpAuthenticationSuccessHandler</tt></a>.
        </td>
        <td>N/A</td>
    </tr>
    <tr>
        <td>weight</td>
        <td></td>
        <td>The (integer) weight of the plugin module. Authentication success handlers with a higher weight will be processed later.</td>
        <td>50</td>
    </tr>
</table>

### Built-in authentication success handlers

Bitbucket Server bundles a number of authentication success handlers. When choosing the `weight` of your authentication success 
handler, consider whether your `http-authentication-success-handler` should be applied before or after the built-in 
handlers:

<table>
    <tr>
        <th>Name</th>
        <th>Weight</th>
        <th>Description</th>
    </tr>
    <tr>
        <td>Crowd SSO authentication success handler</td>
        <td>20</td>
        <td>When SSO integration has been enabled this starts an SSO session even if another authenticator authenticated
            the user</td> 
    </tr>
    <tr>
        <td>Session creating success handler</td>
        <td>50</td>
        <td>Creates an HttpSession if it does not exist yet, except for requests authenticated using BASIC authentication</td>
    </tr>
    <tr>
        <td>Redirecting success handler</td>
        <td>100</td>
        <td>Redirects the user to the URL provided in the `next` request parameter field or the projects page if none
            was provided</td>
    </tr>
</table>

### Example

```xml
<atlassian-plugin key="com.your.domain.custom.auth" name="Bitbucket Server Authentication plugin">

    <plugin-info>
        <description>Configuration example</description>
        <version>1.0</version>
        <vendor name="Atlassian" url="http://www.atlassian.com"/>
    </plugin-info>

    <http-auth-success-handler key="customSuccessHandler"
                               class="com.your.domain.custom.auth.CustomAuthenticationSuccessHandler"
                               weight="90"/>

</atlassian-plugin>
```
