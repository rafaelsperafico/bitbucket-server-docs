---
title:  Merge Request Check Plugin Module
platform: server
product: bitbucketserver
category: reference
subcategory: modules
date: "2017-12-14"
---
#  Merge Request Check Plugin Module

{{% warning %}}

Deprecated in 5.0

The merge-check module type has been deprecated in Bitbucket Server 5.0 and will be removed in 6.0. Please
use the [repository-merge-check](../repository-merge-check-plugin-module) module type instead. 
More details can be found in the [Repository Hooks and Merge Checks Guide](../../../how-tos/hooks-merge-checks-guide).

{{% /warning %}}

## Introduction

A merge request check module defines a Java component that can veto a request by a user to merge a pull request.
A vetoed pull request will be prevented from merging and feedback is given to the user explaining why the merge
can't proceed.

Example merge request checks might include:

* A check that requires at least two reviewers on a pull request (to ensure adequate review coverage)
* A check that requires all reviewers must first approve a pull request prior to it being merged (to achieve consensus)
* A check that requires the latest Bamboo build related to a pull request's source branch should be green (to help
preserve CI success in the target branch)

Note: although merge request checks restrict the merge operation within Bitbucket Server, they do not prevent users from merging
branches in their local clones and pushing this change. Please look at branch permissions for this functionality.

## Configuration

The root element for the Merge Request Check plugin module is `<merge-check/>`. It allows the following two attributes for
configuration:

### Attributes

<table>
    <tr>
        <th>Name</th>
        <th>Required</th>
        <th>Description</th>
        <th>Default</th>
    </tr>
    <tr>
        <td>key</td>
        <td>Yes</td>
        <td>The identifier of the plugin module. This key must be unique within the plugin where it is defined.</td>
        <td>N/A</td>
    </tr>
    <tr>
        <td>class</td>
        <td>Yes</td>
        <td>The fully qualified Java class name of merge request check. This class must implement <a href="https://developer.atlassian.com/static/javadoc/bitbucket-server/latest/spi/reference/com/atlassian/bitbucket/scm/pull/MergeRequestCheck.html"><tt>MergeRequestCheck</tt></a>.</td>
        <td>N/A</td>
    </tr>
</table>

### Example

Here is an example `atlassian-plugin.xml` file containing a single merge check:

```xml
<atlassian-plugin name="My Merge Request Check" key="example.plugin.mycheck" plugins-version="2">
    <plugin-info>
        <description>A basic component import module test</description>
        <vendor name="My Company" url="http://www.mycompany.com"/>
        <version>1.0</version>
    </plugin-info>

    <merge-check key="myMergeCheck" class="com.mycompany.example.plugin.myCheck.MyMergeRequestCheck"/>
</atlassian-plugin>
```

See also the tutorial on [Controlling when pull requests can be merged](../../../tutorials-and-examples/controlling-when-pull-requests-can-be-merged)
