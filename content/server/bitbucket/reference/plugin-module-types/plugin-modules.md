---
title:  Common Atlassian Plugin Modules
platform: server
product: bitbucketserver
category: reference
subcategory: modules
date: "2017-12-14"
---
## Common Atlassian Plugin Modules

The Atlassian Plugin Framework is comprised of a common set of modules shared across other Atlassian products.

- [Common Atlassian Plugin Modules](https://developer.atlassian.com/server/framework/atlassian-sdk/plugin-modules/)
- [Downloadable Plugin Resource](https://developer.atlassian.com/server/framework/atlassian-sdk/adding-resources-to-your-project/)

## Specialized Bitbucket Server Plugin Modules

Bitbucket Server also defines its own set of module types, for tightly integrating with and extending Bitbucket Server-specific functionality.

- [Client Web Item Plugin Module](../client-web-item)
- [Client Web Panel Plugin Module](../client-web-panel)
- [Client Web Section Plugin Module](../client-web-section)
- [Commit Indexer Plugin Module](../commit-indexer)
- [Commit Property Config Plugin Module](../commit-property-config)
- [HTTP Authentication Handler Plugin Module](../http-authentication-handler)
- [HTTP Authentication Failure Handler Plugin Module](../http-authentication-failure-handler)
- [HTTP Authentication Success Handler Plugin Module](../http-authentication-success-handler)
- [Keyboard Shortcut Plugin Module](../keyboard-shortcut)
- [Merge Request Check Plugin Module](../merge-check) <span class="aui-lozenge aui-lozenge-current">DEPRECATED in 5.0</span>
- [Post-Receive Hook Plugin Module](../post-receive-hook-plugin-module) <span class="aui-lozenge aui-lozenge-current">DEPRECATED in 5.0</span>
- [Pre-receive Hook Plugin Module](../pre-receive-hook-plugin-module) <span class="aui-lozenge aui-lozenge-current">DEPRECATED in 5.0</span>
- [Ref Metadata Provider Module](../ref-metadata-provider)
- [REST Fragment Plugin Module](../rest-fragment)
- [Repository Hook Plugin Module](../repository-hook-plugin-module)
- [Repository Merge Check Plugin Module](../repository-merge-check-plugin-module)
- [SCM Request Check Plugin Module](../scm-request-check)
- [SSH Request Handler Plugin Module](../ssh-request-handler)
