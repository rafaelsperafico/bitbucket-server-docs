---
title: Post-Receive Hook Plugin Module
platform: server
product: bitbucketserver
category: reference
subcategory: modules
date: "2017-12-14"
---
# Post-Receive Hook Plugin Module

{{% warning %}}

Deprecated in 5.0

The post-receive module type has been deprecated in Bitbucket Server 5.0 and will be removed in 6.0. Please
use the [repository hook](../repository-hook-plugin-module) module type instead. More details can
be found in the [Repository Hooks and Merge Checks Guide](../../../how-tos/hooks-merge-checks-guide).

{{% /warning %}}

## Introduction

Post-receive hooks are similar to [pre-receive-hooks](../pre-receive-hook-plugin-module) except that the push cannot be cancelled.
They are useful for outputting messages to the client. If no message is required consider listening for
[RepositoryPushEvent](https://developer.atlassian.com/static/javadoc/bitbucket-server/latest/api/reference/com/atlassian/bitbucket/event/repository/RepositoryPushEvent.html) instead.

**Note** This hook module is enabled across all repositories.

## Configuration

### Attributes

<table>
    <tr>
        <th>Name</th>
        <th>Required</th>
        <th>Description</th>
        <th>Default</th>
    </tr>
    <tr>
        <td>key</td>
        <td>Yes</td>
        <td>The identifier of the plugin module. This key must be unique within the plugin where it is defined.</td>
        <td>N/A</td>
    </tr>
    <tr>
        <td>class</td>
        <td>Yes</td>
        <td>The fully qualified Java class name of the hook. This class must implement <code>com.atlassian.bitbucket.hook.PostReceiveHook</code>.</td>
        <td>N/A</td>
    </tr>
    <tr>
        <td>name</td>
        <td></td>
        <td>The human-readable name of the plugin module. I.e. the human-readable name of the hook.</td>
        <td>N/A</td>
    </tr>
    <tr>
        <td>weight</td>
        <td></td>
        <td>
            The (integer) weight of the plugin module. Hooks with a higher weight will be processed later.
            It should almost never be necessary to set this. Your hook implementation should not rely on a particular order
            for it to function correctly as the absolute ordering also depends on the weight set on other hooks.
        </td>
        <td>1000</td>
    </tr>
</table>

### Elements

<table>
    <tr>
        <th>Name</th>
        <th>Required</th>
        <th>Description</th>
        <th>Default</th>
    </tr>
    <tr>
        <td>description</td>
        <td></td>
        <td>
            The description of the plugin module. The 'key' attribute can be specified to
            declare a localisation key for the value instead of text in the element body.
            I.e. the description of the servlet.
        </td>
        <td>N/A</td>
    </tr>
</table>

## Example

Here is an example `atlassian-plugin.xml` file containing a single merge check:

```xml
<atlassian-plugin name="My Merge Request Check" key="example.plugin.preceive" plugins-version="2">
    <plugin-info>
        <description>A basic component import module test</description>
        <vendor name="My Company" url="http://www.mycompany.com"/>
        <version>1.0</version>
    </plugin-info>

    <post-receive-hook key="myPostReceiveHook" name="Show some example" class="com.mycompany.example.plugin.myhook.MyPostReceiveHook">
        <description>A post-receive hook example that thanks the user for pushing</description>
    </post-receive-hook>
</atlassian-plugin>
```

And the corresponding hook:

```java
package com.mycompany.example.plugin.myhook;

import com.atlassian.bitbucket.hook.HookResponse;
import com.atlassian.bitbucket.hook.PostReceiveHook;

public class MyPostReceiveHook implements PostReceiveHook {

    @Override
    public void onReceive(Repository repository, Collection<RefChange> refChanges, HookResponse hookResponse) {
        hookResponse.out().println("Thank you for pushing to Bitbucket Server");
    }
}
```
