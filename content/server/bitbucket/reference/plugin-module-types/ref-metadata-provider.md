---
title: Ref Metadata Provider Plugin Module
platform: server
product: bitbucketserver
category: reference
subcategory: modules
date: "2017-12-14"
---
# Ref Metadata Provider Plugin Module

## Introduction

Ref metadata provider modules are used to associate metadata to refs (branches or tags) within a repository in Bitbucket Server.
This metadata can be accessed via REST when retrieving a list of branches and then can then (optionally) be used to
display additional columns within the branch listing table.

Please see the [how-to guide](../../../how-tos/adding-column-to-branch-listing) to see the ref metadata provider in action.

## Configuration

### Attributes

<table>
    <tr>
        <th>Name</th>
        <th>Required</th>
        <th>Description</th>
        <th>Default</th>
    </tr>
    <tr>
        <td>key</td>
        <td>Yes</td>
        <td>The identifier of the plugin module. This key must be unique within the plugin where it is defined.</td>
        <td>N/A</td>
    </tr>
    <tr>
        <td>class</td>
        <td>Yes</td>
        <td>The fully qualified Java class name of the hook. This class must implement
            <a href="https://developer.atlassian.com/static/javadoc/bitbucket-server/latest/spi/reference/com/atlassian/bitbucket/repository/RefMetadataProvider.html">RefMetadataProvider</a>
         </td>
        <td>N/A</td>
    </tr>
    <tr>
        <td>name</td>
        <td></td>
        <td>The human-readable name of the plugin module.</td>
        <td>N/A</td>
    </tr>
</table>

### Elements

<table>
    <tr>
        <th>Name</th>
        <th>Required</th>
        <th>Description</th>
        <th>Default</th>
    </tr>
    <tr>
        <td>description</td>
        <td></td>
        <td>
            The description of the plugin module. The 'key' attribute can be specified to
            declare a localisation key for the value instead of text in the element body.
        </td>
        <td>N/A</td>
    </tr>
</table>

## Simple Example

Here is an example `atlassian-plugin.xml` file containing just a ref metadata provider.

```xml
<atlassian-plugin name="My Ref Metadata Provider Example" key="example.plugin.myprovider" plugins-version="2">
    <plugin-info>
        <description>A basic ref metadata provider plugin</description>
        <vendor name="My Company" url="http://www.mycompany.com"/>
        <version>1.0</version>
    </plugin-info>

    <ref-metadata-provider key="example-metadata" name="Branch color provider" class="com.mycompany.example.plugin.myprovider.MyRefMetadataProvider" />

</atlassian-plugin>
```

## More Examples

You can find a more concrete example in the [how-to guide](../../../how-tos/adding-column-to-branch-listing).
