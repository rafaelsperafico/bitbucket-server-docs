---
title: Repository Hook Plugin Module
platform: server
product: bitbucketserver
category: reference
subcategory: modules
date: "2017-12-14"
---
# Repository Hook Plugin Module

## Introduction

Repository Hook modules are used to plug in to the different ways in which branches and tags are updated in
Bitbucket Server. This can be useful for blocking some pushes, merges or updating an external entity based
on the new repository state.

Please see the [how-to guide](../../../how-tos/hooks-merge-checks-guide) for more information on creating a 
repository hook.

## Configuration

### Attributes

<table>
    <tr>
        <th>Name</th>
        <th>Required</th>
        <th>Description</th>
        <th>Default</th>
    </tr>
    <tr>
        <td>key</td>
        <td>Yes</td>
        <td>The identifier of the plugin module. This key must be unique within the plugin where it is defined.</td>
        <td>N/A</td>
    </tr>
    <tr>
        <td>class</td>
        <td>Yes</td>
        <td>The fully qualified Java class name of the hook. This class must implement
            <a href="https://developer.atlassian.com/static/javadoc/bitbucket-server/latest/spi/reference/com/atlassian/bitbucket/hook/repository/PreRepositoryHook.html">PreRepositoryHook</a> or
            <a href="https://developer.atlassian.com/static/javadoc/bitbucket-server/latest/spi/reference/com/atlassian/bitbucket/hook/repository/PostRepositoryHook.html">PostRepositoryHook</a>
         </td>
        <td>N/A</td>
    </tr>
    <tr>
        <td>name</td>
        <td>Yes</td>
        <td>The human-readable name of the plugin module.</td>
        <td>N/A</td>
    </tr>
    <tr>
        <td>i18n-name-key</td>
        <td></td>
        <td>
          The i18n key of the human-readable name of the plugin module. If defined, this name will be displayed in 
          the Repository Settings > Hooks section instead of the value provided in the 'name' attribute</td>
        <td>N/A</td>
    </tr>
    <tr>
        <td>configurable</td>
        <td></td>
        <td>
          Whether the hook can be enabled or disabled at the repository level. If set to false, the hook will enabled 
          for all repositories and will not be displayed in the Repository Settings > Hooks section.
         </td>
        <td>N/A</td>
    </tr>
</table>

### Elements

<table>
    <tr>
        <th>Name</th>
        <th>Required</th>
        <th>Description</th>
        <th>Default</th>
    </tr>
    <tr>
        <td>description</td>
        <td></td>
        <td>
            The description of the plugin module. The 'key' attribute can be specified to
            declare a localisation key for the value instead of text in the element body.
        </td>
        <td>N/A</td>
    </tr>
    <tr>
        <td>icon</td>
        <td></td>
        <td>
            Repository hooks are able to display a customised icon on the administration screen.
            Regardless of the original size, this icon will be scaled to 48x48 px.
        </td>
        <td>N/A</td>
    </tr>
    <tr>
        <td>config-form</td>
        <td></td>
        <td>
            Repository hooks are able to have additional per-repository settings that are automatically passed to your hook.
            Configuration form modules are used to describe a simple configuration form that will be automatically
            saved, loaded and validated.
            See <a href="#Config-Form_Elements">below</a> for more details.
        </td>
        <td>N/A</td>
    </tr>
    <tr>
        <td>validator</td>
        <td></td>
        <td>
            The class of the validation component that will perform setting validation before save.
            This class must implement <code>com.atlassian.bitbucket.setting.RepositorySettingsValidator</code>.
            Alternatively this interface can be applied on the hook instead, and this element can be ignored.
        </td>
        <td>N/A</td>
    </tr>
</table>

### Config-Form Elements

These are the elements required for the `config-form` element.

<table>
    <tr>
        <th>Name</th>
        <th>Required</th>
        <th>Description</th>
        <th>Default</th>
    </tr>
    <tr>
        <td>directory</td>
        <td>Yes</td>
        <td>
            A directory that contains any custom Soy, JavaScript and CSS files required to implement the view.
        </td>
        <td>N/A</td>
    </tr>
    <tr>
        <td>view</td>
        <td>Yes</td>
        <td>
            A JavaScript function that will be invoked to display the form. This function will be expected to return
            HTML which may be generated via Soy or some other mechanism, ideally using AUI for consistency.
            <p/>
            For more information about AUI please visit the <a href="http://docs.atlassian.com/aui/latest">sandbox</a>.
            For an a list of available Soy templates that you can use in your own configuration form, consult the
            <a href="https://bitbucket.org/atlassian/aui/src/5.8.14/src/soy/form.soy">source</a>. </td>
        <td>N/A</td>
    </tr>
</table>

## Simple Example

Here is an example `atlassian-plugin.xml` file containing just a repository hook.

```xml
<atlassian-plugin name="My Repository Hook Example" key="example.plugin.myhook" plugins-version="2">
    <plugin-info>
        <description>A basic hook plugin</description>
        <vendor name="My Company" url="http://www.mycompany.com"/>
        <version>1.0</version>
    </plugin-info>

    <repository-hook key="examplePreReceiveHook" name="Stop force pushes" 
                     class="com.mycompany.example.plugin.myhook.MyRepositoryHook" />

</atlassian-plugin>
```

## Full Example

Here is an example `atlassian-plugin.xml` file containing a repository hook and an configuration form that is validated.

```xml
<atlassian-plugin name="My Repository Hook Example" key="example.plugin.myhook" plugins-version="2">
    <plugin-info>
        <description>A basic hook plugin</description>
        <vendor name="My Company" url="http://www.mycompany.com"/>
        <version>1.0</version>
    </plugin-info>

    <repository-hook key="examplePreReceiveHook" name="Stop force pushes" 
                     class="com.mycompany.example.plugin.myhook.MyRepositoryHook">
        <!-- Optional -->
        <icon>icons/hook.png</icon>
        <!-- Optional -->
        <config-form name="Simple Hook Config" key="simpleHook-config">
            <view>bitbucket.config.example.hook.simple</view>
            <directory location="/static/"/>
        </config-form>
        <!-- Optional -->
        <validator>com.mycompany.example.plugin.myhook.MyValidator</validator>
    </repository-hook>

</atlassian-plugin>
```

## More Examples

You can find some concrete examples on the [how-to guide](../../../how-tos/hooks-merge-checks-guide).
