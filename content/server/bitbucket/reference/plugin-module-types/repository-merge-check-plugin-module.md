---
title: Repository Hook Plugin Module
platform: server
product: bitbucketserver
category: reference
subcategory: modules
date: "2017-12-14"
---
# Repository Merge Check Plugin Module

## Introduction

A repository merge check module defines a Java component that can veto a request by a user to merge a pull request.
A vetoed pull request will be prevented from merging and feedback is given to the user explaining why the merge
can't proceed.

Example merge checks might include:

* A check that requires at least two reviewers on a pull request (to ensure adequate review coverage)
* A check that requires all reviewers must first approve a pull request prior to it being merged (to achieve consensus)
* A check that requires the latest Bamboo build related to a pull request's source branch should be green (to help
preserve CI success in the target branch)

Note: although merge checks restrict the merge operation within Bitbucket Server, they do not prevent users 
from merging branches in their local clones and pushing this change. Please look at branch permissions for this 
functionality.

Please see the [how-to guide](../../../how-tos/hooks-merge-checks-guide) for more information on creating a 
repository merge check.

## Configuration

The root element for the Repository Merge Check plugin module is `<repository-merge-check/>`. It allows the following 
attributes for configuration:

### Attributes

<table>
    <tr>
        <th>Name</th>
        <th>Required</th>
        <th>Description</th>
        <th>Default</th>
    </tr>
    <tr>
        <td>key</td>
        <td>Yes</td>
        <td>The identifier of the plugin module. This key must be unique within the plugin where it is defined.</td>
        <td>N/A</td>
    </tr>
    <tr>
        <td>class</td>
        <td>Yes</td>
        <td>The fully qualified Java class name of the merge check. This class must implement
            <a href="https://developer.atlassian.com/static/javadoc/bitbucket-server/latest/spi/reference/com/atlassian/bitbucket/hook/repository/RepositoryMergeCheck.html">RepositoryMergeCheck</a>.
         </td>
        <td>N/A</td>
    </tr>
    <tr>
        <td>name</td>
        <td>Yes</td>
        <td>The human-readable name of the plugin module.</td>
        <td>N/A</td>
    </tr>
    <tr>
        <td>i18n-name-key</td>
        <td></td>
        <td>
          The i18n key of the human-readable name of the plugin module. If defined, this name will be displayed in 
          the Repository Settings > Hooks section instead of the value provided in the 'name' attribute</td>
        <td>N/A</td>
    </tr>
    <tr>
        <td>configurable</td>
        <td></td>
        <td>
          Whether the merge check can be enabled or disabled at the repository level. If set to false, the merge check
          will enabled for all repositories and will not be displayed in the Repository Settings > Hooks section.
         </td>
        <td>true</td>
    </tr>
</table>

### Elements

<table>
    <tr>
        <th>Name</th>
        <th>Required</th>
        <th>Description</th>
        <th>Default</th>
    </tr>
    <tr>
        <td>description</td>
        <td></td>
        <td>
            The description of the plugin module. The 'key' attribute can be specified to
            declare a localisation key for the value instead of text in the element body.
        </td>
        <td>N/A</td>
    </tr>
    <tr>
        <td>icon</td>
        <td></td>
        <td>
            Repository merge checks are able to display a customised icon on the administration screen.
            Regardless of the original size, this icon will be scaled to 48x48 px.
        </td>
        <td>N/A</td>
    </tr>
    <tr>
        <td>config-form</td>
        <td></td>
        <td>
            Repository merge checks are able to have additional per-repository settings that are automatically passed 
            to your merge check. Configuration form modules are used to describe a simple configuration form that will 
            be automatically saved, loaded and validated.
            See <a href="#Config-Form_Elements">below</a> for more details.
        </td>
        <td>N/A</td>
    </tr>
    <tr>
        <td>validator</td>
        <td></td>
        <td>
            The class of the validation component that will perform setting validation before save.
            This class must implement <code>com.atlassian.bitbucket.setting.RepositorySettingsValidator</code>.
            Alternatively this interface can be applied on the merge check instead, and this element can be ignored.
        </td>
        <td>N/A</td>
    </tr>
</table>

### Config-Form Elements

These are the elements required for the `config-form` element.

<table>
    <tr>
        <th>Name</th>
        <th>Required</th>
        <th>Description</th>
        <th>Default</th>
    </tr>
    <tr>
        <td>directory</td>
        <td>Yes</td>
        <td>
            A directory that contains any custom Soy, JavaScript and CSS files required to implement the view.
        </td>
        <td>N/A</td>
    </tr>
    <tr>
        <td>view</td>
        <td>Yes</td>
        <td>
            A JavaScript function that will be invoked to display the form. This function will be expected to return
            HTML which may be generated via Soy or some other mechanism, ideally using AUI for consistency.
            <p/>
            For more information about AUI please visit the <a href="http://docs.atlassian.com/aui/latest">sandbox</a>.
            For an a list of available Soy templates that you can use in your own configuration form, consult the
            <a href="https://bitbucket.org/atlassian/aui/src/5.8.14/src/soy/form.soy">source</a>. </td>
        <td>N/A</td>
    </tr>
</table>

## Simple Example

Here is an example `atlassian-plugin.xml` file containing just a repository merge check. The merge check in this 
example is enabled for all repositories (because of configurable="false").

```xml
<atlassian-plugin key="${atlassian.plugin.key}" name="${project.name}" plugins-version="2">
    <plugin-info>
        <description>${project.description}</description>
        <version>${project.version}</version>
        <vendor name="${project.organization.name}" url="${project.organization.url}" />
        <param name="plugin-icon">images/pluginIcon.png</param>
        <param name="plugin-logo">images/pluginLogo.png</param>
    </plugin-info>

    <!-- add our i18n resource -->
    <resource type="i18n" name="i18n" location="is-admin-merge-check"/>

    <repository-merge-check key="isAdmin" class="bean:isAdminMergeCheck" configurable="false"/>
</atlassian-plugin>
```

## Full Example

Here is an example `atlassian-plugin.xml` file containing a repository merge check and an configuration form that is 
validated.

```xml
<atlassian-plugin name="My Repository Merge Check Example" key="example.plugin.mymergecheck" plugins-version="2">
    <plugin-info>
        <description>A basic merge check plugin</description>
        <vendor name="My Company" url="http://www.mycompany.com"/>
        <version>1.0</version>
    </plugin-info>

    <repository-merge-check key="exampleMergeCheck" name="Requires Green Build" 
                     class="com.mycompany.example.plugin.mymergecheck.MyRepositoryMergeCheck">
        <!-- Optional -->
        <icon>icons/greenbuild.png</icon>
        <!-- Optional -->
        <config-form name="Simple Config" key="simpleMergeCheck-config">
            <view>bitbucket.config.example.mergecheck.simple</view>
            <directory location="/static/"/>
        </config-form>
        <!-- Optional -->
        <validator>com.mycompany.example.plugin.mymergecheck.MyValidator</validator>
    </repository-merge-check>

</atlassian-plugin>
```

## More Examples

You can find some concrete examples on the [how-to guide](../../../how-tos/hooks-merge-checks-guide) and the 
['Controlling when pull requests can be merged' tutorial](../../../tutorials-and-examples/controlling-when-pull-requests-can-be-merged).
