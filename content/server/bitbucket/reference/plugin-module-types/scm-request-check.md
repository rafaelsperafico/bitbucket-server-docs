---
title:  SCM Request Check Plugin Module
platform: server
product: bitbucketserver
category: reference
subcategory: modules
date: "2017-12-14"
---
#  SCM Request Check Plugin Module

## Introduction

Whenever an scm client (for example, the `git` binary) pushes commits to or pulls commits from Bitbucket Server, the request must
pass through a pipeline of SCM Request Checks before being allowed to interact with a Bitbucket Server repository. You can add to
this pipeline by defining your own SCM Request Check modules.

Example SCM request checks might include:

* A check that puts a repository into maintenance mode (all pushes and pulls are blocked) to fix a bad merge
* A check that prevents access to a repository that has been moved to another server, and sends the user a message with
the new location

[`ScmRequestCheck`](https://developer.atlassian.com/static/javadoc/bitbucket-server/latest/spi/reference/com/atlassian/bitbucket/scm/ScmRequestCheck.html)
implementations are passed an [`ScmRequest`](https://developer.atlassian.com/static/javadoc/bitbucket-server/latest/spi/reference/com/atlassian/bitbucket/scm/ScmRequest.html)
object which allows them to determine the context repository for the request and whether the request is a push or pull.
[`ScmRequest`](https://developer.atlassian.com/static/javadoc/bitbucket-server/latest/spi/reference/com/atlassian/bitbucket/scm/ScmRequest.html)
also allows the request check to send human-readable messages to the client.

## Configuration

The root element for an SCM Request Check plugin module is `<scm-request-check/>`. It allows the following attributes for
configuration:

### Attributes

<table>
    <tr>
        <th>Name</th>
        <th>Required</th>
        <th>Description</th>
        <th>Default</th>
    </tr>
    <tr>
        <td>key</td>
        <td>Yes</td>
        <td>The identifier of the plugin module. This key must be unique within the plugin where it is defined.</td>
        <td>N/A</td>
    </tr>
    <tr>
        <td>class</td>
        <td>Yes</td>
        <td>The fully qualified Java class name of SCM request check. This class must implement <a href="https://developer.atlassian.com/static/javadoc/bitbucket-server/latest/spi/reference/com/atlassian/bitbucket/scm/ScmRequestCheck.html"><tt>ScmRequestCheck</tt></a>.</td>
        <td>N/A</td>
    </tr>
    <tr>
        <td>weight</td>
        <td></td>
        <td>The (integer) weight of the plugin module. Request checks with a larger weight will be processed later.</td>
        <td>10</td>
    </tr>
</table>

### Example

Here is an excerpt from the `atlassian-plugin.xml` file that defines Bitbucket Server's bundled SCM request checks:

```xml
<atlassian-plugin key="\${project.groupId}.\${project.artifactId}" name="\${project.name}" plugins-version="2">
    <plugin-info>
        <description>\${project.description}</description>
        <version>\${project.version}</version>
        <vendor name="\${project.organization.name}" url="\${project.organization.url}" />
    </plugin-info>

    <!-- check that the context user has access to the repository, processed first -->
    <scm-request-check key="repository-authorisation-check"
                       weight="100"
                       class="com.atlassian.bitbucket.internal.scm.check.RepositoryAuthorisationCheck" />

    <!-- check that there aren't too many concurrent git hosting operations, processed second -->
    <scm-request-check key="throttle-check"
                       weight="200"
                       class="com.atlassian.bitbucket.internal.scm.check.ThrottleCheck" />
</atlassian-plugin>
```
