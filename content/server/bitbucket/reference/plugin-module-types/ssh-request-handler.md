---
title:  SSH Request Handler Plugin Module
platform: server
product: bitbucketserver
category: reference
subcategory: modules
date: "2017-12-14"
---
#  SSH Request Handler Plugin Module

## Introduction

As you probably know, Bitbucket Server supports serving SCM hosting requests (i.e. pushes and pulls) over both HTTP/S and SSH. What you may
_not know_   is that Bitbucket Server supports another simple SSH command - `whoami`. If you have SSH enabled on your server and have uploaded your public
key, try the following from the command line:

```sh
$ ssh -p 7999 your-bitbucket-server.example.com whoami
```

(Replace 7999 with the port that your Bitbucket Server server running Bitbucket Server)

You should get a simple response with your username:

```sh
tpettersen
```

Neat, huh? What's even cooler is the fact that these commands are _pluggable_. The `whoami` command is actually provided by a simple bundled
plugin in Bitbucket Server. You can implement your own SSH command support using the SSH Request Handler plugin module.

See the javadoc for [`SshScmRequestHandler`](https://developer.atlassian.com/static/javadoc/bitbucket-server/latest/spi/reference/com/atlassian/bitbucket/scm/ssh/SshScmRequestHandler.html) and [`SshScmRequest`](https://developer.atlassian.com/static/javadoc/bitbucket-server/latest/spi/reference/com/atlassian/bitbucket/scm/ssh/SshScmRequest.html) for more details on implementing support for a new SSH command.

## Configuration

The root element for an SSH Request Handler plugin module is `<ssh-request-handler/>`. It allows the following attributes for
configuration:

### Attributes

<table>
    <tr>
        <th>Name</th>
        <th>Required</th>
        <th>Description</th>
        <th>Default</th>
    </tr>
    <tr>
        <td>key</td>
        <td>Yes</td>
        <td>The identifier of the plugin module. This key must be unique within the plugin where it is defined.</td>
        <td>N/A</td>
    </tr>
    <tr>
        <td>class</td>
        <td>Yes</td>
        <td>
            The fully qualified Java class name of the SSH request handler. This class must implement
            <a href="https://developer.atlassian.com/static/javadoc/bitbucket-server/latest/spi/reference/com/atlassian/bitbucket/scm/ssh/SshScmRequestHandler.html"><tt>SshScmRequestHandler</tt></a>
            (don't worry about the 'Scm' in the name - non-SCM related commands are fine too).
        </td>
        <td>N/A</td>
    </tr>
</table>

### Example

Here is a simple `atlassian-plugin.xml` with a single SSH Request Handler module:

```xml
<atlassian-plugin name="My SSH Command Handler" key="example.plugin.mysshcommand" plugins-version="2">
    <plugin-info>
        <description>${project.description}</description>
        <version>${project.version}</version>
        <vendor name="${project.organization.name}" url="${project.organization.url}" />
    </plugin-info>

    <ssh-request-handler key="my-ssh-command" class="com.example.myplugin.MyCommandSshRequestHandler" />
</atlassian-plugin>
```
