---
title: User Erasure
platform: server
product: bitbucketserver
category: reference
subcategory: modules
date: "2018-11-13"
---
# User Erasure Plugin Module

## Introduction

Plugins can implement a user erasure handler to participate in the user erasure process for a user. User erasure is used to remove
personally identifiable user data, or replace it with an alias if the data can not be removed.

## Difference between user erasure and user deletion

User deletion removes a user from Bitbucket, while user erasure modifies a deleted user by updating their personally identifiable
information (i.e. replacing their username with a non-attributable alias).

When a user is deleted, data such as permissions, SSH keys and access tokens are deleted (once the `UserCleanupEvent` is published for
that user some time after the actual deletion). When a user is erased, their username is changed to a non-attributable alias, their
personal project key is renamed to this alias, and any mentions to their original username updated to this alias.

To erase a user, that user must be first deleted. The `UserCleanupEvent` is guaranteed to have run before a user is erased. If your plugin
needs to perform some kind of cleanup on _every_ deleted user (e.g. deleting user settings), you should listen to the `UserCleanupEvent` with
an event listener. However, if your plugin persists personally identifiable user data (e.g. displays a username for user in the UI), you can
implement a user erasure handler and update or remove that information.

## How to use the `UserErasureHandler`

The `UserErasureRequest` parameter in `UserErasureHandler#eraseUser(..)` supplies an `originalUsername` and an `updatedUser`. If your plugin
stores user data by username, you can use the `originalUsername` to look up the relevant data for the user being erased. If your plugin stores
user data by user ID, you can use `updatedUser.getId()` to look up the relevant data. Since the user is deleted it won't be accessible through
 the Bitbucket user APIs (e.g. `UserService` and `UserAdminService`). Once you have this data, you can either delete it if
appropriate, e.g. deleting avatars, or update it to use the new username for the user (`updatedUser.getUsername()`), e.g. user mentions.

## Configuration

The element for the user erasure plugin module is `<user-erasure-handler/>`. It allows the following configuration attributes:

#### Attributes

<table>
    <tr>
        <th>Name</th>
        <th>Required</th>
        <th>Description</th>
    </tr>
    <tr>
        <td>key</td>
        <td>Yes</td>
        <td>The identifier of the plugin module. This key must be unique within the plugin where it is defined.</td>
    </tr>
    <tr>
        <td>class</td>
        <td>Yes</td>
        <td>
            A user erasure handler definition containing a fully qualified class name.
            This class must implement <a href="https://docs.atlassian.com/bitbucket-server/javadoc/latest/spi/reference/com/atlassian/bitbucket/user/UserErasureHandler.html">UserErasureHandler</a>.
        </td>
    </tr>
    <tr>
        <td>weight</td>
        <td></td>
        <td>The (integer) weight of the plugin module. User erasure handlers with a higher weight will be processed later. Third party plugins must use a weight greater than 100</td>
    </tr>
</table>

#### Example

Here is an example for a custom user erasure handler:

```xml

<atlassian-plugin key="${project.groupId}.${project.artifactId}" name="${project.name}" plugins-version="2">
    <plugin-info>
        <description>${project.description}</description>
        <version>${project.version}</version>
        <vendor name="${project.organization.name}" url="${project.organization.url}" />
    </plugin-info>

    <user-erasure-handler
        class="com.example.bitbucket.MyUserErasureHandler"
        key="ExampleUserErasureHandler"
        weight="101"
    />

</atlassian-plugin>
```

#### Weights

Third party plugins <b>must</b> use a plugin weight greater than 100.
