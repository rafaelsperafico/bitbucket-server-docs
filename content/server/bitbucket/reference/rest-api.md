---
title:  REST API
platform: server
product: bitbucketserver
category: reference
subcategory: rest-reference
date: "2018-08-29"
---
#  REST APIs

The Bitbucket Server REST API is split up into multiple modules, each provided by a separate bundled plugin.

General information about using the REST APIs can be found at [Using the REST API](../../how-tos/command-line-rest)
and [Authenticating with the REST API](../../how-tos/example-basic-authentication).

## Core

The [Core REST API](https://developer.atlassian.com/static/rest/bitbucket-server/latest/bitbucket-rest.html) provides REST
resources for core functionality such as server administration, projects, repositories, pull requests and user
management.

## Audit

The [Audit REST API](https://developer.atlassian.com/static/rest/bitbucket-server/latest/bitbucket-audit-rest.html)
provides REST resources for querying the subset of audit events which are stored against a project or repository.
Full audit logs are only available via the audit log file.

## Branches

The [Branch REST API](https://developer.atlassian.com/static/rest/bitbucket-server/latest/bitbucket-branch-rest.html)
provides REST resources for managing repository branches and branch models.

## Builds

The [Build REST API](https://developer.atlassian.com/static/rest/bitbucket-server/latest/bitbucket-build-rest.html)
provides REST resources for updating and querying build information.

See [this how-to guide](../../how-tos/updating-build-status-for-commits) for more details.

## Code Insights

The [Code Insights REST API](https://developer.atlassian.com/static/rest/bitbucket-server/latest/bitbucket-code-insights-rest.html)
provides REST resources for creating reports and annotating a pull request diff.

See [this how-to guide](../../how-tos/code-insights) for more details or 
[this tutorial](../../tutorials-and-examples/code-insights-tutorial) for an example.

## Comment Likes

The [Comment Likes REST API](https://developer.atlassian.com/static/rest/bitbucket-server/latest/bitbucket-comment-likes-rest.html)
provides REST resources for adding, removing and querying [comment likes](https://confluence.atlassian.com/display/STASH/Stash+3.5+release+notes#Stash3.5releasenotes-Commentlikes).

## Default Reviewers

The [Default Reviewers REST API](https://developer.atlassian.com/static/rest/bitbucket-server/latest/bitbucket-default-reviewers-rest.html)
provides REST resources for managing repository default reviewer configurations.

## Git
The [Git REST API](https://developer.atlassian.com/static/rest/bitbucket-server/latest/bitbucket-git-rest.html) provides
remote APIs supporting selected git-specific operations in Bitbucket, already available through
[Java API](https://developer.atlassian.com/static/javadoc/bitbucket-server/latest/git-api/reference/com/atlassian/bitbucket/scm/git/GitScm.html).
Currently supporting management of repository tags.

## Jira

The [Jira REST API](https://developer.atlassian.com/static/rest/bitbucket-server/latest/bitbucket-jira-rest.html)
provides REST resources for querying Jira issues indexed against commits.

## Personal Access Tokens

The [Personal Access Tokens REST API](https://developer.atlassian.com/static/rest/bitbucket-server/latest/bitbucket-access-tokens-rest.html)
provides REST resources for creating, deleting, and retrieving personal access tokens, as well as modifying existing
token names and permissions.

## Ref Restriction

The [Ref Restriction REST API](https://developer.atlassian.com/static/rest/bitbucket-server/latest/bitbucket-ref-restriction-rest.html)
provides REST resources for managing repository ref restrictions.

## Repository Ref Synchronization

The [Repository Ref Synchronization REST API](https://developer.atlassian.com/static/rest/bitbucket-server/latest/bitbucket-repository-ref-sync-rest.html)
provides REST resources for managing ref synchronization for a repository.

## SSH

The [SSH REST API](https://developer.atlassian.com/static/rest/bitbucket-server/latest/bitbucket-ssh-rest.html) provides REST resources
for managing user SSH keys.
