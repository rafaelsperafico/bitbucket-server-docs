---
title: Branch Selector Field Soy template
platform: server
product: bitbucketserver
category: reference
subcategory: other
date: "2017-12-14"
---
# Branch Selector Field Soy template

## bitbucket.component.branchSelector.field

Renders a full AUI form field with label, hidden input, and branch selector. In order to read the value selected in the
branch selector you can get the value of the hidden input, which is automatically 
populated when users interact with the paired branch selector, or listen for the `bitbucket.component.branchSelector.change`
event.

Non repository-hook users of this field will need to add a plugin dependency 
on `com.atlassian.bitbucket.server.bitbucket-web-api:branch-selector-field` to ensure required components are loaded.

### Example usage

In your Soy template:

```html
{call bitbucket.component.branchSelector.field}
  {param id: 'test-selector' /}
  {param labelText: 'Branch' /}
{/call}
```

In your JavaScript:

```javascript
var branch = $('#test-selector').val();
```

Or alternatively listen for the `bitbucket.component.branchSelector.change` event. Note that this assumes the
web resource containing your JavaScript depends on `com.atlassian.bitbucket.server.bitbucket-web-api:events`:

```javascript
require([
    'bitbucket/util/events'
], function (events) {
    events.on('bitbucket.component.branchSelector.change', function (data) {
        console.log("The branch selector with id " + data.elementId + " was changed to  " + data.ref.displayId);
    });
});
```

### Parameters

<table>
<tr>
    <th>Name</th>
    <th>Required?</th>
    <th>Description</th>
</tr>
<tr>
    <td><code>id</code></td>
    <td>Yes</td>
    <td>The id of the input. Will default to <code>name</code>. At least one of <code>id</code> and <code>name</code> is required.</td>
</tr>
<tr>
    <td><code>name</code></td>
    <td>No</td>
    <td>The name of the input. Will default to <code>id</code>. At least one of <code>id</code> and <code>name</code> is required.</td>
</tr>
<tr>
    <td><code>labelText</code></td>
    <td>Yes</td>
    <td>The text to use as the label for this field</td>
</tr>
<tr>
    <td><code>isRequired</code></td>
    <td>No</td>
    <td>Whether this field is required</td>
</tr>
<tr>
    <td><code>descriptionText</code></td>
    <td>No</td>
    <td>Description string for the field</td>
</tr>
<tr>
    <td><code>errorTexts</code></td>
    <td>No</td>
    <td>A list of strings for any errors that need to be displayed.</td>
</tr>
<tr>
    <td><code>showTags</code></td>
    <td>No</td>
    <td>Whether to allow the user to select tags, as well as branches. False by default</td>
</tr>
<tr>
    <td><code>initialValue</code></td>
    <td>No</td>
    <td>a branch name (fully qualified with the <code>refs/heads/</code> prefix) that is initially selected (or null)</td>
</tr>
</table>

## bitbucket.component.branchSelector.input

Renders a hidden input and branch selector. The input will be automatically populated when users interact with the paired branch selector.

### Parameters

<table>
<tr>
    <th>Name</th>
    <th>Required?</th>
    <th>Description</th>
</tr>
<tr>
    <td><code>id</code></td>
    <td>Yes</td>
    <td>The id of the input. Will default to <code>name</code>. At least one of <code>id</code> and <code>name</code> is required.</td>
</tr>
<tr>
    <td><code>name</code></td>
    <td>No</td>
    <td>The name of the input. Will default to <code>id</code>. At least one of <code>id</code> and <code>name</code> is required.</td>
</tr>
<tr>
    <td><code>initialValue</code></td>
    <td>No</td>
    <td>a branch name (fully qualified with the <code>refs/heads/</code> prefix) that is initially selected (or null)</td>
</tr>
<tr>
    <td><code>showTags</code></td>
    <td>No</td>
    <td>Whether to allow the user to select tags, as well as branches. False by default</td>
</tr>
</table>
