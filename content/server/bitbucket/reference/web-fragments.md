---
title:  Web Fragments
platform: server
product: bitbucketserver
category: reference
subcategory: other
date: "2017-12-14"
---
#  Web Fragments

A web fragment is a link, a section of links or an area of HTML content (or
'panel') in a particular location of the Bitbucket Server web interface. A web fragment
could be a menu in Bitbucket Server's top navigation bar (which in itself may contain its
own links and link sections), buttons on the project listing or panels on the
commit page. Generally the content for web fragments are generated on the server,
however Bitbucket Server also supports [client web fragments.](../plugin-module-types/client-web-fragment)

## Plugin Modules involved in creating a Web Fragment

A web fragment is one of six kinds of plugin module:

  * A [Web Item](https://developer.atlassian.com/display/DOCS/Web+Item+Plugin+Module) module defines an individual link that is displayed at a particular location or section of the Bitbucket Server user interface. For example, a web item might define an individual item within a Bitbucket Server drop-down menu or a button on the issue operations bar.
  * A [Web Panel](https://developer.atlassian.com/display/DOCS/Web+Panel+Plugin+Module) module defines a section of HTML content displayed on a Bitbucket Server page.
  * A [Web Section](https://developer.atlassian.com/display/DOCS/Web+Section+Plugin+Module) module defines a collection of links that is displayed together at a particular location of the Bitbucket Server user interface. For example, a web section might define a group of items within a Bitbucket Server drop-down menu (separated by lines) or a group of buttons on the commit page.
  * A [Client Web Item](../plugin-module-types/client-web-item) client side version of Web Item.
  * A [Client Web Panel](../plugin-module-types/client-web-panel) client side version of Web Panel.
  * A [Client Web Section](../plugin-module-types/client-web-section) client side version of Web Section.

Web items or web sections are utilized in a number of different ways,
depending on the location of the web fragment you are creating. Note that  [client web fragments](../plugin-module-types/client-web-fragment) have some additional things to consider.

## Finding plugin points in the User Interface

You can view all available UI plugin points on a page by adding certain query parameters to the URL of any page in Bitbucket Server.

Simply add one or more of the following query parameters to the URL for any existing Bitbucket Server view:

<table>
  <thead>
    <tr><th>Query Parameter</th><th>Description</th></tr>
  </thead>
  <tbody>
    <tr><th>web.items</th><td>Display available <a href="https://developer.atlassian.com/display/PLUGINFRAMEWORK/Web+Item+Plugin+Module">Web Item</a> and <a href="../plugin-module-types/client-web-item">Client Web Item</a> locations</td></tr>
    <tr><th>web.panels</th><td>Display available <a href="https://developer.atlassian.com/display/PLUGINFRAMEWORK/Web+Panel+Plugin+Module">Web Panel</a> and <a href="../plugin-module-types/client-web-panel">Client Web Panel</a> locations</td></tr>
    <tr><th>web.sections</th><td>Display available <a href="https://developer.atlassian.com/display/PLUGINFRAMEWORK/Web+Section+Plugin+Module">Web Section</a> and <a href="../plugin-module-types/client-web-section">Client Web Section</a> locations</td></tr>
  </tbody>
</table>

Bitbucket Server will then render example web items in all available pluggable locations.

For example, to find all web panels on a repository browse page:

`https://<your Bitbucket Server instance>/projects?web.panels`

![Bitbucket Web Panels](/server/bitbucket/images/bitbucket-web.panels.png)

Available web panel locations are rendered in red and any relevant objects which are available in the context are listed.

You can add multiple query parameters to see different types of web fragments. For example:

`https://<your Bitbucket Server instance>/projects?web.items&web.panels&web.sections`

Would display all web item, web panel and web section locations on the repository browse page.

## Existing Web Fragments in Bitbucket Server

You can insert custom web fragments into existing ones in Bitbucket Server (e.g. web
sections which are 'built in' to Bitbucket Server). However, you cannot redefine Bitbucket Server's
existing web fragments.

We recommend downloading the [Bitbucket Server source
archive](http://www.atlassian.com/software/stash/StashSourceDownloads.jspa),
so you can access the appropriate source files that define Bitbucket Server's existing
web fragments. This will help you:

  * Alter the positions of your own web fragments
  * Ensure `key` values in your own `<web-items>` and `<web-sections>` remain unique.

{{% note %}}

You need to log in as a user with a commercial license to access the download page for the Bitbucket Server source archive.

{{% /note %}}

## Conditions

Conditions can be added to the web section, web item and web panel modules, to display them only when all the given
conditions are true.

Condition elements must contain a class attribute with the fully-qualified name of a Java class. The referenced class:

  * must implement `com.atlassian.plugin.web.Condition`, and
  * will be auto-wired by Spring before any condition checks are performed.

To add a condition to your web section, web item or web panel, add the `condition` element as follows:

```xml
<condition class="com.atlassian.bitbucket.web.conditions.IsLoggedInCondition"/>
```

Condition elements can take optional parameters. These parameters will be passed in to the condition's init() method
as a map of string key/value pairs after autowiring, but before any condition checks are performed. For example:

```xml
<condition class="com.atlassian.bitbucket.web.conditions.HasGlobalPermissionCondition">
    <param name="permission">ADMIN</param>
</condition>
```

To invert a condition, add the attribute `invert="true"` to the condition element. This is useful where you want to
show the section if a certain condition is not satisfied. Conditions elements are composed of a collection of
condition/conditions elements and a type attribute. The type attribute defines what logical operator is used to
evaluate its collection of condition elements. The type can be one of `AND` or `OR`.

For example: The following condition is true if the current user is a system administrator AND the mail server is
configured:

```xml
<conditions type="AND">
    <condition class="com.atlassian.bitbucket.web.conditions.IsMailServerConfiguredCondition"/>
    <condition class="com.atlassian.bitbucket.web.conditions.HasGlobalPermissionCondition">
        <param name="permission">SYS_ADMIN</param>
    </condition>
</conditions>
```

### Example

Here is an example atlassian-plugin.xml file containing a single web item:

```xml
<web-item key="repositories-menu" name="Repositories Global web item" weight="30" section="header.global.primary">
    <condition class="com.atlassian.bitbucket.web.conditions.IsLoggedInCondition"/>
    <label key="bitbucket.web.menu.repositories">Repositories</label>
    <link linkId="repositories-menu-trigger">/</link>
    <styleClass>recent-repositories</styleClass>
</web-item>
```

### Using a provided Bitbucket Server condition

Many convenient conditions are provided by Bitbucket Server. A full list can be found in our
[documentation](https://developer.atlassian.com/static/javadoc/bitbucket-server/latest/web-common/reference/com/atlassian/bitbucket/web/conditions/package-summary.html).
