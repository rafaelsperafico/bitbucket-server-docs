---
title:  Bitbucket Server web resource contexts
platform: server
product: bitbucketserver
category: reference
subcategory: other
date: "2017-12-14"
---
#  Bitbucket Server web resource contexts

If you are writing a plugin for Bitbucket Server, more than likely you will need to
inject custom resources such as CSS or JavaScript into pages which are core to
the application.

See [Web Resource Plugin Module](https://developer.atlassian.com/display/DOCS/Web+Resource+Plugin+Module) for
more information on how to use the web resource context.

## Available contexts

We do not recommend using the standard `atl.general` context unless you
_actually_ need to inject resources on _every page_ in Bitbucket Server. Please scope
your resources to the pages that need them.

<table>
    <tr>
        <th>Context</th>
        <th>Description</th>
        <th>Since</th>
    </tr>
    <tr>
        <td>atl.general</td>
        <td>All non-admin pages</td>
        <td>1.0</td>
    </tr>
    <tr>
        <td>atl.admin</td>
        <td>All admin pages</td>
        <td>1.0</td>
    </tr>
    <tr>
        <td>bitbucket.page.about</td>
        <td>About page</td>
        <td>4.0</td>
    </tr>
    <tr>
        <td>bitbucket.page.admin.authentication</td>
        <td>Admin authentication settings page</td>
        <td>4.0</td>
    </tr>
    <tr>
        <td>bitbucket.page.admin.avatars</td>
        <td>Admin avatars settings page</td>
        <td>4.0</td>
    </tr>
    <tr>
        <td>bitbucket.page.admin.editDbConfig</td>
        <td>Admin database edit configuration page</td>
        <td>4.0</td>
    </tr>
    <tr>
        <td>bitbucket.page.admin.globalPermissions</td>
        <td>Admin global permissions page</td>
        <td>4.0</td>
    </tr>
    <tr>
        <td>bitbucket.page.admin.groups.edit</td>
        <td>Admin group edit page</td>
        <td>4.0</td>
    </tr>
    <tr>
        <td>bitbucket.page.admin.groups.list</td>
        <td>Admin group listing page</td>
        <td>4.0</td>
    </tr>
    <tr>
        <td>bitbucket.page.admin.license</td>
        <td>Admin license page</td>
        <td>4.0</td>
    </tr>
    <tr>
        <td>bitbucket.page.admin.license.edit</td>
        <td>Admin edit license page</td>
        <td>4.0</td>
    </tr>
    <tr>
        <td>bitbucket.page.admin.logging</td>
        <td>Admin logging page</td>
        <td>4.0</td>
    </tr>
    <tr>
        <td>bitbucket.page.admin.mailServer</td>
        <td>Admin mail server settings page</td>
        <td>4.0</td>
    </tr>
    <tr>
        <td>bitbucket.page.admin.server</td>
        <td>Admin server settings page</td>
        <td>4.0</td>
    </tr>
    <tr>
        <td>bitbucket.page.admin.users.create</td>
        <td>Admin user creation page</td>
        <td>4.0</td>
    </tr>
    <tr>
        <td>bitbucket.page.admin.users.edit</td>
        <td>Admin user edit page</td>
        <td>4.0</td>
    </tr>
    <tr>
        <td>bitbucket.page.admin.users.list</td>
        <td>Admin user listing page</td>
        <td>4.0</td>
    </tr>
    <tr>
        <td>bitbucket.page.branch.creation</td>
        <td>Branch creation page</td>
        <td>4.0</td>
    </tr>
    <tr>
        <td>bitbucket.page.branch.creation.noaccess</td>
        <td>Branch creation page (no write access to any repo)</td>
        <td>4.0</td>
    </tr>
    <tr>
        <td>bitbucket.page.backup</td>
        <td>Backup page</td>
        <td>4.0</td>
    </tr>
    <tr>
        <td>bitbucket.page.compare</td>
        <td>Compare page</td>
        <td>4.0</td>
    </tr>
    <tr>
        <td>bitbucket.page.db</td>
        <td>Admin database settings page</td>
        <td>4.0</td>
    </tr>
    <tr>
        <td>bitbucket.page.errors</td>
        <td>Generic error page (401, 403, 404, 500, etc)</td>
        <td>4.0</td>
    </tr>
    <tr>
        <td>bitbucket.page.gettingstarted</td>
        <td>Welcome page for first-time Bitbucket Server users</td>
        <td>4.0</td>
    </tr>
    <tr>
        <td>bitbucket.page.globalRepositoryList</td>
        <td>Global repository list page</td>
        <td>4.0</td>
    </tr>
    <tr>
        <td>bitbucket.page.login</td>
        <td>Login page</td>
        <td>4.0</td>
    </tr>
    <tr>
        <td>bitbucket.page.logout</td>
        <td>Logout page</td>
        <td>4.0</td>
    </tr>
    <tr>
        <td>bitbucket.page.maintenance.lock</td>
        <td>Locked for maintenance page</td>
        <td>4.0</td>
    </tr>
    <tr>
        <td>bitbucket.page.migration</td>
        <td>Database migration page</td>
        <td>4.0</td>
    </tr>
    <tr>
        <td>bitbucket.page.project.auditLog</td>
        <td>Project audit log page</td>
        <td>4.0</td>
    </tr>
    <tr>
        <td>bitbucket.page.project.create</td>
        <td>Project creation page</td>
        <td>4.0</td>
    </tr>
    <tr>
        <td>bitbucket.page.project.generalSettings</td>
        <td>Project settings page</td>
        <td>4.0</td>
    </tr>
    <tr>
        <td>bitbucket.page.project.overview</td>
        <td>Project overview page (repository listing)</td>
        <td>4.0</td>
    </tr>
    <tr>
        <td>bitbucket.page.project.permissions</td>
        <td>Project permissions page</td>
        <td>4.0</td>
    </tr>
    <tr>
        <td>bitbucket.page.projectList</td>
        <td>Project listing page</td>
        <td>4.0</td>
    </tr>
    <tr>
        <td>bitbucket.page.pullRequest.list</td>
        <td>Pull request listing page</td>
        <td>4.0</td>
    </tr>
    <tr>
        <td>bitbucket.page.pullRequest.view</td>
        <td>Pull request view page</td>
        <td>4.0</td>
    </tr>
    <tr>
        <td>bitbucket.page.repository.auditLog</td>
        <td>Repository audit log page</td>
        <td>4.0</td>
    </tr>
    <tr>
        <td>bitbucket.page.repository.branches</td>
        <td>Repository branches page</td>
        <td>4.0</td>
    </tr>
    <tr>
        <td>bitbucket.page.repository.branchPermissions</td>
        <td>Repository branch permissions page</td>
        <td>4.0</td>
    </tr>
    <tr>
        <td>bitbucket.page.repository.commit</td>
        <td>Repository commit page</td>
        <td>4.0</td>
    </tr>
    <tr>
        <td>bitbucket.page.repository.commits</td>
        <td>Repository commit listing page</td>
        <td>4.0</td>
    </tr>
    <tr>
        <td>bitbucket.page.repository.create</td>
        <td>Create new repository page</td>
        <td>4.0</td>
    </tr>
    <tr>
        <td>bitbucket.page.repository.empty</td>
        <td>Repository is empty page</td>
        <td>4.0</td>
    </tr>
    <tr>
        <td>bitbucket.page.repository.filebrowser</td>
        <td>Repository file browser page</td>
        <td>4.0</td>
    </tr>
    <tr>
        <td>bitbucket.page.repository.fileContent</td>
        <td>Repository file content page (source and diff view)</td>
        <td>4.0</td>
    </tr>
    <tr>
        <td>bitbucket.page.repository.fork</td>
        <td>Create a fork of a repository</td>
        <td>4.0</td>
    </tr>
    <tr>
        <td>bitbucket.page.repository.generalSettings</td>
        <td>Repository details page (in the Repository's settings)</td>
        <td>4.0</td>
    </tr>
    <tr>
        <td>bitbucket.page.repository.noDefaultBranch</td>
        <td>Repository page when no default branch is set in the Git repository</td>
        <td>4.0</td>
    </tr>
    <tr>
        <td>bitbucket.page.repository.permissions</td>
        <td>Repository-level permissions page</td>
        <td>4.0</td>
    </tr>
    <tr>
        <td>bitbucket.page.repository.settings.hooks</td>
        <td>Repository hooks configuration page</td>
        <td>4.0</td>
    </tr>
    <tr>
        <td>bitbucket.page.repository.settings.pullRequests</td>
        <td>Repository pull-request settings</td>
        <td>4.0</td>
    </tr>
    <tr>
        <td>bitbucket.page.repository.settings.refSync</td>
        <td>Repository ref-sync settings page</td>
        <td>4.0</td>
    </tr>
    <tr>
        <td>bitbucket.page.resetUserPassword</td>
        <td>Reset user password page</td>
        <td>4.0</td>
    </tr>
    <tr>
        <td>bitbucket.page.setup.adminUser</td>
        <td>bitbucket.setup: configure admin user</td>
        <td>4.0</td>
    </tr>
    <tr>
        <td>bitbucket.page.setup.database</td>
        <td>bitbucket.setup: configure database</td>
        <td>4.0</td>
    </tr>
    <tr>
        <td>bitbucket.page.setup.jiraIntegration</td>
        <td>bitbucket.setup: configure Jira integration</td>
        <td>4.0</td>
    </tr>
    <tr>
        <td>bitbucket.page.setup.settings</td>
        <td>bitbucket.setup: configure settings</td>
        <td>4.0</td>
    </tr>
    <tr>
        <td>bitbucket.page.signup</td>
        <td>User signup page</td>
        <td>4.0</td>
    </tr>
    <tr>
        <td>bitbucket.page.ssh.key.add</td>
        <td>Add public SSH key</td>
        <td>4.0</td>
    </tr>
    <tr>
        <td>bitbucket.page.user.account.password</td>
        <td>User account password page.</td>
        <td>4.0</td>
    </tr>
    <tr>
        <td>bitbucket.page.user.account.settings</td>
        <td>User account details page.</td>
        <td>4.0</td>
    </tr>
    <tr>
        <td>bitbucket.page.user.notification.settings</td>
        <td>User account notification settings page.</td>
        <td>4.0</td>
    </tr>
    <tr>
        <td>bitbucket.page.user.repository.notification.settings</td>
        <td>User account watched repositories settings page.</td>
        <td>5.10</td>
    </tr>
    <tr>
        <td>bitbucket.page.user.profile</td>
        <td>Profile page when viewed by any user.</td>
        <td>4.0</td>
    </tr>
    <tr>
        <td>bitbucket.page.user.profile.self</td>
        <td>Profile page when viewed by the currently logged in user.</td>
        <td>4.0</td>
    </tr>
    <tr>
        <td>bitbucket.page.user.profile.others</td>
        <td>Profile page when viewed by a user other than the currently logged in one.</td>
        <td>4.0</td>
    </tr>
    <tr>
        <td>bitbucket.page.xsrfNotification</td>
        <td>XSRF token missing notification page</td>
        <td>4.0</td>
    </tr>
    <tr>
        <td>bitbucket.feature.files.fileHandlers</td>
        <td>File handlers to handle rendering the source of a file or the diff of a file changed</td>
        <td>4.0</td>
    </tr>
</table>
