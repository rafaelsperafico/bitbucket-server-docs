---
title:  Web UI APIs
platform: server
product: bitbucketserver
category: reference
subcategory: other
date: "2017-12-14"
---
#  Web UI APIs

There are a few APIs that allow you to integrate with Bitbucket Server's UI in stable and supported ways.

## Decorators

Decorators allow you to style your own Servlet HTML output to look and behave like Bitbucket pages.  There is more detail available on the [Plugin Decorators](../plugin-decorators) page,
or you can follow a tutorial for [decorating the user account](../../tutorials-and-examples/decorating-the-user-account) or
[decorating the user profile](../../tutorials-and-examples/decorating-the-user-profile).

## Web Fragments

Web fragments allow you to define content that should be shown on existing Bitbucket Server pages. There is more detail available on the
[Web Fragment](../web-fragments) and [Client Web Fragment](../plugin-module-types/client-web-fragment) pages, or you can follow a tutorial to [integrate your Client Web Panel into the Pull Request
Overview](../../tutorials-and-examples/pull-request-overview).

## Web Contexts

You can include your own UI assets (JavaScript, CSS, LESS, Soy, etc) onto any Bitbucket Server page by choosing an appropriate context for it. A list
of available contexts can be found on the [Web Resource Contexts](../web-resource-contexts) page.

## UI Components - JavaScript, Soy, and LESS

Bitbucket Server, like all Atlassian products, supports [AUI](https://docs.atlassian.com/aui/latest/) components. These allow you to build
UI components that look and behave like the rest of Bitbucket Server. Note that the stability of AUI components marked "experimental" is not guaranteed between minor versions of Bitbucket Server.

Bitbucket Server also exposes some of its own components for you to use.

- [JavaScript API](../js-api) - utilities you can use in your own JavaScript code.
- [Soy API](../soy-api) - currently a single template. We will expand this API in future releases.

*Note that any unmentioned JS, Soy, or LESS components are _not_ considered stable API and may change in any version. Do not use them if you require your plugin to work across upgrades of Bitbucket Server.*

### Web resources

Bitbucket Server exposes a number of web resources as part of its web API.  These resources are intended for you to use as a dependency when you 
require certain functionality provided by Bitbucket Server in web resource modules that you create in your app.  While there is the possibility 
that these resources have already been loaded by Bitbucket Server, it is best practice to depend on any resources you use explicitly in order to ensure 
your app continues to function if the dependencies loaded by Bitbucket Server change in future.

#### AMD

If you create your app's JavaScript using [AMD](../js-api#Asynchronous_Module_Definition_\(AMD\)), or are going to be consuming any AMD modules from 
[Bitbucket Server's JavaScript API](../js-api) or AUI, then you must depend on the AMD module to ensure that Bitbucket Server's AMD loader is present on the page.

##### Dependency

```xml
<dependency>com.atlassian.bitbucket.server.bitbucket-web-api:amd</dependency>
```

#### AUI

If you are using AUI in your app then you should depend on the `aui` module in the Bitbucket Server web API in order to ensure that dependencies AUI
requires from Bitbucket Server are correctly loaded before AUI.

Note that this dependency only includes the AUI core, any modules marked as "Experimental" in the AUI documentation will still need explicit dependencies.

By including the `aui` and `amd` dependencies AUI will be available in your JavaScript as an AMD module called `aui`.

##### Dependency

```xml
<dependency>com.atlassian.bitbucket.server.bitbucket-web-api:aui</dependency>
```

##### Usage in JavaScript

(Note this assumes you have a dependency on the AMD web resource as well as AUI)

```javascript
define('my-plugin/module', [
    'aui'
], function (
    AJS // In Bitbucket Server we refer to the AUI AMD module as `AJS` (Atlassian JS) for historical reasons.
        // You are not required to do this in your app.
) {
    AJS.$(document).onReady(function () {    
        AJS.messages.generic({
            title: 'Hello, world!',
            body: '<p>This is a great message.</p>'
        });
    });
});
```

#### Client-side Soy Templates

If you use your own client-side Soy templates in your app then you need to depend on the `client-soy` resource in order
to ensure that dependencies required by the JavaScript compiled Soy templates are loaded before you call your templates.

##### Dependency

```xml
<dependency>com.atlassian.bitbucket.server.bitbucket-web-api:client-soy</dependency>
```

#### jQuery

If your app uses [jQuery](https://jquery.com) then you should depend on the jQuery resource to ensure that it is loaded and available prior to your app's code executing. Currently
depending on jQuery will provide jQuery via AMD, however this is an implementation detail and so you should also ensure you include the AMD dependency if
your own code is using AMD.  The exposed module name is called `jquery`.

##### Dependency

```xml
<dependency>com.atlassian.bitbucket.server.bitbucket-web-api:jquery</dependency>
```

##### Usage in JavaScript

(Note this assumes you have a dependency on the AMD web resource as well as jQuery)

```javascript
define('my-plugin/module', [
    'jquery'
], function (
    $
) {
    $(document).onReady(function () {
        console.log('Hello, world!');
    }
});
```
