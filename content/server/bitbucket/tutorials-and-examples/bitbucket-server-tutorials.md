---
title: Bitbucket Server Plugin Tutorials
platform: server
product: bitbucketserver
category: devguide
subcategory: learning
date: "2017-12-14"
---
# Bitbucket Server Plugin Tutorials

This page contains a selection of tutorials for implementing selected plugin points from Bitbucket Server.

## Controlling when pull requests can be merged
[This tutorial](../controlling-when-pull-requests-can-be-merged) will take you through the steps required to write a plugin containing a
[Merge Request Check](../../reference/plugin-module-types/merge-check)
which is designed to prevent pull requests from being merged unless the user performing the merge is an administrator for the target repository.

After completing this tutorial you will have learned to do the following:

 * Generate a Bitbucket Server plugin using Atlassian SDK
 * Implement a repository merge check in Java
 * Declare a repository merge check in your plugin's atlassian-plugin.xml
 * Use various Bitbucket Server services and the repository merge check API
 * Internationalize the messages your plugin emits

[Check out the "Controlling when pull requests can be merged" tutorial](../controlling-when-pull-requests-can-be-merged).

## Decorating the user profile and account pages

Adding custom content to the public user profile, or a user's account management pages is easy! Follow the following tutorials:

* [User profile](../decorating-the-user-profile)
* [User account](../decorating-the-user-account)

## Showing related items on a pull request

[This tutorial](../pull-request-overview) will take you through the steps required to add custom content to the Overview tab
of a pull request.

After completing this tutorial you will have learned to do the following:

 * Find plug-in points in Bitbucket Server's UI for adding content
 * Define a [Client Web Panel](../../reference/plugin-module-types/client-web-panel)
 * Use custom CSS and JS in your panel with [Web Resource](https://developer.atlassian.com/display/DOCS/Web+Resource+Plugin+Module) dependencies

[Check out the "Pull Request Overview" tutorial](../pull-request-overview).
